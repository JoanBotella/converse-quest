# Converse Quest

A TypeScript engine for Interactive Fiction games, or in castillian, Aventuras Conversacionales.

## License

GNU GPL v3 or later <https://www.gnu.org/licenses/gpl-3.0.en.html>

## Author

Joan Botella Vinaches <https://joanbotella.com>
