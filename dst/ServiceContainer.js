"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServiceContainer = void 0;
var DemoCliAppRunner_1 = require("./demo/service/cliAppRunner/DemoCliAppRunner");
var DemoApp_1 = require("./demo/service/app/DemoApp");
var DemoCliAppRequestBuilder_1 = require("./demo/service/cliAppRequestBuilder/DemoCliAppRequestBuilder");
var DemoCliAppResponseDispatcher_1 = require("./demo/service/cliAppResponseDispatcher/DemoCliAppResponseDispatcher");
var DemoRouter_1 = require("./demo/service/router/DemoRouter");
var DemoCliStylizer_1 = require("./demo/service/cliStylizer/DemoCliStylizer");
var DemoCliQueryNormalizer_1 = require("./demo/service/cliQueryNormalizer/DemoCliQueryNormalizer");
var DemoCliPrompter_1 = require("./demo/service/cliPrompter/DemoCliPrompter");
var DemoCliQueryBuilder_1 = require("./demo/service/cliQueryBuilder/DemoCliQueryBuilder");
var DemoNodeRequestByAppRequestBuilder_1 = require("./demo/service/nodeRequestByAppRequestBuilder/DemoNodeRequestByAppRequestBuilder");
var DemoSystemNodeResponseBuilder_1 = require("./demo/service/systemNodeResponseBuilder/DemoSystemNodeResponseBuilder");
var DemoAppResponseByNodeResponseBuilder_1 = require("./demo/service/appResponseByNodeResponseBuilder/DemoAppResponseByNodeResponseBuilder");
var DemoNextNodeRequestBuilder_1 = require("./demo/service/nextNodeRequestBuilder/DemoNextNodeRequestBuilder");
var DemoNodeRequestProcessor_1 = require("./demo/service/nodeRequestProcessor/DemoNodeRequestProcessor");
var DemoStartingStatusBuilder_1 = require("./demo/service/startingStatusBuilder/DemoStartingStatusBuilder");
var ServiceContainer = /** @class */ (function () {
    function ServiceContainer() {
    }
    ServiceContainer.prototype.getCliAppRunner = function () {
        if (this.cliAppRunner === undefined) {
            this.cliAppRunner = new DemoCliAppRunner_1.DemoCliAppRunner(this.getStartingStatusBuilder(), this.getCliAppRequestBuilder(), this.getApp(), this.getCliAppResponseDispatcher());
        }
        return this.cliAppRunner;
    };
    ServiceContainer.prototype.getCliAppRequestBuilder = function () {
        if (this.cliAppRequestBuilder === undefined) {
            this.cliAppRequestBuilder = new DemoCliAppRequestBuilder_1.DemoCliAppRequestBuilder(this.getCliQueryBuilder());
        }
        return this.cliAppRequestBuilder;
    };
    ServiceContainer.prototype.getApp = function () {
        if (this.app === undefined) {
            this.app = new DemoApp_1.DemoApp(this.getNodeRequestByAppRequestBuilder(), this.getAppResponseByNodeResponseBuilder(), this.getNextNodeRequestBuilder(), this.getNodeRequestProcessor());
        }
        return this.app;
    };
    ServiceContainer.prototype.getCliAppResponseDispatcher = function () {
        if (this.cliAppResponseDispatcher === undefined) {
            this.cliAppResponseDispatcher = new DemoCliAppResponseDispatcher_1.DemoCliAppResponseDispatcher(this.getCliStylizer(), this.getCliPrompter());
        }
        return this.cliAppResponseDispatcher;
    };
    ServiceContainer.prototype.getRouter = function () {
        if (this.router === undefined) {
            this.router = new DemoRouter_1.DemoRouter();
        }
        return this.router;
    };
    ServiceContainer.prototype.getCliStylizer = function () {
        if (this.cliStylizer === undefined) {
            this.cliStylizer = new DemoCliStylizer_1.DemoCliStylizer();
        }
        return this.cliStylizer;
    };
    ServiceContainer.prototype.getCliQueryNormalizer = function () {
        if (this.cliQueryNormalizer === undefined) {
            this.cliQueryNormalizer = new DemoCliQueryNormalizer_1.DemoCliQueryNormalizer();
        }
        return this.cliQueryNormalizer;
    };
    ServiceContainer.prototype.getCliPrompter = function () {
        if (this.cliPrompter === undefined) {
            this.cliPrompter = new DemoCliPrompter_1.DemoCliPrompter(this.getCliStylizer());
        }
        return this.cliPrompter;
    };
    ServiceContainer.prototype.getCliQueryBuilder = function () {
        if (this.cliQueryBuilder === undefined) {
            this.cliQueryBuilder = new DemoCliQueryBuilder_1.DemoCliQueryBuilder(this.getCliPrompter(), this.getCliQueryNormalizer());
        }
        return this.cliQueryBuilder;
    };
    ServiceContainer.prototype.getNodeRequestByAppRequestBuilder = function () {
        if (this.nodeRequestByAppRequestBuilder === undefined) {
            this.nodeRequestByAppRequestBuilder = new DemoNodeRequestByAppRequestBuilder_1.DemoNodeRequestByAppRequestBuilder();
        }
        return this.nodeRequestByAppRequestBuilder;
    };
    ServiceContainer.prototype.getSystemNodeResponseBuilder = function () {
        if (this.systemNodeResponseBuilder === undefined) {
            this.systemNodeResponseBuilder = new DemoSystemNodeResponseBuilder_1.DemoSystemNodeResponseBuilder();
        }
        return this.systemNodeResponseBuilder;
    };
    ServiceContainer.prototype.getAppResponseByNodeResponseBuilder = function () {
        if (this.appResponseByNodeResponseBuilder === undefined) {
            this.appResponseByNodeResponseBuilder = new DemoAppResponseByNodeResponseBuilder_1.DemoAppResponseByNodeResponseBuilder();
        }
        return this.appResponseByNodeResponseBuilder;
    };
    ServiceContainer.prototype.getNextNodeRequestBuilder = function () {
        if (this.nextNodeRequestBuilder === undefined) {
            this.nextNodeRequestBuilder = new DemoNextNodeRequestBuilder_1.DemoNextNodeRequestBuilder();
        }
        return this.nextNodeRequestBuilder;
    };
    ServiceContainer.prototype.getNodeRequestProcessor = function () {
        if (this.nodeRequestProcessor === undefined) {
            this.nodeRequestProcessor = new DemoNodeRequestProcessor_1.DemoNodeRequestProcessor(this.getSystemNodeResponseBuilder(), this.getRouter());
        }
        return this.nodeRequestProcessor;
    };
    ServiceContainer.prototype.getStartingStatusBuilder = function () {
        if (this.startingStatusBuilder === undefined) {
            this.startingStatusBuilder = new DemoStartingStatusBuilder_1.DemoStartingStatusBuilder();
        }
        return this.startingStatusBuilder;
    };
    return ServiceContainer;
}());
exports.ServiceContainer = ServiceContainer;
