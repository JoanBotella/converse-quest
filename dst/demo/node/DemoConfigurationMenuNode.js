"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DemoConfigurationMenuNode = void 0;
var DemoNodeAbs_1 = require("../library/node/DemoNodeAbs");
var DemoSlide_1 = require("../library/slide/DemoSlide");
var DemoLanguageMenuNode_1 = require("./DemoLanguageMenuNode");
var DemoConfigurationMenuNode = /** @class */ (function (_super) {
    __extends(DemoConfigurationMenuNode, _super);
    function DemoConfigurationMenuNode() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DemoConfigurationMenuNode.prototype.tryToSetupNodeResponse = function () {
        var nodeRequest = this.getNodeRequest(), status = nodeRequest.getStatus(), configuration = status.getConfiguration(), variables = status.getVariables(), translation = this.getTranslation();
        if (!this.hasNodeRequestQuery()) {
            var slide = new DemoSlide_1.DemoSlide();
            slide.setTitle(translation.node_configurationMenu_title());
            slide.setText(translation.node_configurationMenu_text_look());
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, [
                slide,
            ]);
            this.setNodeResponse(nodeResponse);
            return;
        }
        if (this.queryMatches(translation.node_configurationMenu_query_1())) {
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, []);
            nodeResponse = this.getNodeBrowser().push(nodeResponse, DemoLanguageMenuNode_1.DemoLanguageMenuNode.ID);
            this.setNodeResponse(nodeResponse);
        }
        else if (this.queryMatches(translation.node_configurationMenu_query_2())) {
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, []);
            nodeResponse = this.getNodeBrowser().pop(nodeResponse);
            this.setNodeResponse(nodeResponse);
        }
    };
    DemoConfigurationMenuNode.ID = 'configurationMenu';
    return DemoConfigurationMenuNode;
}(DemoNodeAbs_1.DemoNodeAbs));
exports.DemoConfigurationMenuNode = DemoConfigurationMenuNode;
