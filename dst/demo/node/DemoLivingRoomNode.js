"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DemoLivingRoomNode = void 0;
var DemoNodeAbs_1 = require("../library/node/DemoNodeAbs");
var DemoSlide_1 = require("../library/slide/DemoSlide");
var DemoCorridorNode_1 = require("./DemoCorridorNode");
var DemoTalkJoanNode_1 = require("./DemoTalkJoanNode");
var DemoLivingRoomNode = /** @class */ (function (_super) {
    __extends(DemoLivingRoomNode, _super);
    function DemoLivingRoomNode() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DemoLivingRoomNode.prototype.tryToSetupNodeResponse = function () {
        var nodeRequest = this.getNodeRequest(), status = nodeRequest.getStatus(), configuration = status.getConfiguration(), variables = status.getVariables(), translation = this.getTranslation();
        if (!this.hasNodeRequestQuery()) {
            var slide = new DemoSlide_1.DemoSlide();
            slide.setTitle(translation.node_livingRoom_title());
            slide.setText(translation.node_livingRoom_text_look());
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, [
                slide,
            ]);
            this.setNodeResponse(nodeResponse);
            return;
        }
        if (this.queryMatches(translation.node_livingRoom_query_look())) {
            var slide = new DemoSlide_1.DemoSlide();
            slide.setText(translation.node_livingRoom_text_look());
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, [
                slide,
            ]);
            this.setNodeResponse(nodeResponse);
        }
        else if (this.queryMatches(translation.node_livingRoom_query_lookJoan())) {
            var slide = new DemoSlide_1.DemoSlide();
            slide.setText(translation.node_livingRoom_text_lookJoan());
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, [
                slide,
            ]);
            this.setNodeResponse(nodeResponse);
        }
        else if (this.queryMatches(translation.node_livingRoom_query_talkJoan())) {
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, []);
            nodeResponse = this.getNodeBrowser().push(nodeResponse, DemoTalkJoanNode_1.DemoTalkJoanNode.ID);
            this.setNodeResponse(nodeResponse);
        }
        else if (this.queryMatches(translation.node_livingRoom_query_goCorridor())) {
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, []);
            nodeResponse = this.getNodeBrowser().change(nodeResponse, DemoCorridorNode_1.DemoCorridorNode.ID);
            this.setNodeResponse(nodeResponse);
        }
    };
    DemoLivingRoomNode.ID = 'livingRoom';
    return DemoLivingRoomNode;
}(DemoNodeAbs_1.DemoNodeAbs));
exports.DemoLivingRoomNode = DemoLivingRoomNode;
