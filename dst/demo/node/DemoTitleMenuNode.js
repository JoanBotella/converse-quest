"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DemoTitleMenuNode = void 0;
var DemoNodeAbs_1 = require("../library/node/DemoNodeAbs");
var DemoSlide_1 = require("../library/slide/DemoSlide");
var DemoBedroomNode_1 = require("./DemoBedroomNode");
var DemoConfigurationMenuNode_1 = require("./DemoConfigurationMenuNode");
var DemoTitleMenuNode = /** @class */ (function (_super) {
    __extends(DemoTitleMenuNode, _super);
    function DemoTitleMenuNode() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DemoTitleMenuNode.prototype.tryToSetupNodeResponse = function () {
        var nodeRequest = this.getNodeRequest(), status = nodeRequest.getStatus(), configuration = status.getConfiguration(), variables = status.getVariables(), translation = this.getTranslation();
        if (!this.hasNodeRequestQuery()) {
            var slide = new DemoSlide_1.DemoSlide();
            slide.setTitle(translation.node_titleMenu_title());
            slide.setText(translation.node_titleMenu_text_look());
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, [
                slide,
            ]);
            this.setNodeResponse(nodeResponse);
            return;
        }
        if (this.queryMatches(translation.node_titleMenu_query_1())) {
            var slide = new DemoSlide_1.DemoSlide();
            slide.setText(translation.node_titleMenu_text_1());
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, [
                slide,
            ]);
            nodeResponse = this.getNodeBrowser().change(nodeResponse, DemoBedroomNode_1.DemoBedroomNode.ID);
            this.setNodeResponse(nodeResponse);
        }
        else if (this.queryMatches(translation.node_titleMenu_query_2())) {
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, []);
            nodeResponse = this.getNodeBrowser().push(nodeResponse, DemoConfigurationMenuNode_1.DemoConfigurationMenuNode.ID);
            this.setNodeResponse(nodeResponse);
        }
        else if (this.queryMatches(translation.node_titleMenu_query_3())) {
            var slide = new DemoSlide_1.DemoSlide();
            slide.setText(translation.node_titleMenu_text_3());
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, [
                slide,
            ]);
            nodeResponse.setEnd(true);
            this.setNodeResponse(nodeResponse);
        }
    };
    DemoTitleMenuNode.ID = 'titleMenu';
    return DemoTitleMenuNode;
}(DemoNodeAbs_1.DemoNodeAbs));
exports.DemoTitleMenuNode = DemoTitleMenuNode;
