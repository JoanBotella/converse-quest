"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DemoCorridorNode = void 0;
var DemoNodeAbs_1 = require("../library/node/DemoNodeAbs");
var DemoBedroomNode_1 = require("./DemoBedroomNode");
var DemoSlide_1 = require("../library/slide/DemoSlide");
var DemoLivingRoomNode_1 = require("./DemoLivingRoomNode");
var DemoCorridorNode = /** @class */ (function (_super) {
    __extends(DemoCorridorNode, _super);
    function DemoCorridorNode() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DemoCorridorNode.prototype.tryToSetupNodeResponse = function () {
        var nodeRequest = this.getNodeRequest(), status = nodeRequest.getStatus(), configuration = status.getConfiguration(), variables = status.getVariables(), translation = this.getTranslation();
        if (!this.hasNodeRequestQuery()) {
            var slide = new DemoSlide_1.DemoSlide();
            slide.setTitle(translation.node_corridor_title());
            slide.setText(translation.node_corridor_text_look());
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, [
                slide,
            ]);
            this.setNodeResponse(nodeResponse);
            return;
        }
        if (this.queryMatches(translation.node_corridor_query_look())) {
            var slide = new DemoSlide_1.DemoSlide();
            slide.setText(translation.node_corridor_text_look());
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, [
                slide,
            ]);
            this.setNodeResponse(nodeResponse);
        }
        else if (this.queryMatches(translation.node_corridor_query_goBedroom())) {
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, []);
            nodeResponse = this.getNodeBrowser().change(nodeResponse, DemoBedroomNode_1.DemoBedroomNode.ID);
            this.setNodeResponse(nodeResponse);
        }
        else if (this.queryMatches(translation.node_corridor_query_goLivingRoom())) {
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, []);
            nodeResponse = this.getNodeBrowser().change(nodeResponse, DemoLivingRoomNode_1.DemoLivingRoomNode.ID);
            this.setNodeResponse(nodeResponse);
        }
    };
    DemoCorridorNode.ID = 'corridor';
    return DemoCorridorNode;
}(DemoNodeAbs_1.DemoNodeAbs));
exports.DemoCorridorNode = DemoCorridorNode;
