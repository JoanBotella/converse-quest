"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DemoTalkJoanNode = void 0;
var DemoNodeAbs_1 = require("../library/node/DemoNodeAbs");
var DemoSlide_1 = require("../library/slide/DemoSlide");
var DemoTalkJoanNode = /** @class */ (function (_super) {
    __extends(DemoTalkJoanNode, _super);
    function DemoTalkJoanNode() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DemoTalkJoanNode.prototype.tryToSetupNodeResponse = function () {
        var nodeRequest = this.getNodeRequest(), status = nodeRequest.getStatus(), configuration = status.getConfiguration(), variables = status.getVariables(), translation = this.getTranslation();
        if (!this.hasNodeRequestQuery()) {
            var slide = new DemoSlide_1.DemoSlide();
            slide.setText(translation.node_talkJoan_text_look());
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, [
                slide,
            ]);
            this.setNodeResponse(nodeResponse);
            return;
        }
        if (this.queryMatches(translation.node_talkJoan_query_1())) {
            var slide = new DemoSlide_1.DemoSlide();
            slide.setText(translation.node_talkJoan_text_1());
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, [
                slide,
            ]);
            nodeResponse = this.getNodeBrowser().pop(nodeResponse);
            this.setNodeResponse(nodeResponse);
        }
    };
    DemoTalkJoanNode.ID = 'talkJoan';
    return DemoTalkJoanNode;
}(DemoNodeAbs_1.DemoNodeAbs));
exports.DemoTalkJoanNode = DemoTalkJoanNode;
