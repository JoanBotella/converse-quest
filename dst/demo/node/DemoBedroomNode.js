"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DemoBedroomNode = void 0;
var DemoNodeAbs_1 = require("../library/node/DemoNodeAbs");
var DemoSlide_1 = require("../library/slide/DemoSlide");
var DemoCorridorNode_1 = require("./DemoCorridorNode");
var DemoBedroomNode = /** @class */ (function (_super) {
    __extends(DemoBedroomNode, _super);
    function DemoBedroomNode() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DemoBedroomNode.prototype.tryToSetupNodeResponse = function () {
        var nodeRequest = this.getNodeRequest(), status = nodeRequest.getStatus(), configuration = status.getConfiguration(), variables = status.getVariables(), translation = this.getTranslation();
        if (!this.hasNodeRequestQuery()) {
            var slide = new DemoSlide_1.DemoSlide();
            slide.setTitle(translation.node_bedroom_title());
            slide.setText(this.buildLook(variables));
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, [
                slide,
            ]);
            this.setNodeResponse(nodeResponse);
            return;
        }
        if (this.queryMatches(translation.node_bedroom_query_look())) {
            var slide = new DemoSlide_1.DemoSlide();
            slide.setText(this.buildLook(variables));
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, [
                slide,
            ]);
            this.setNodeResponse(nodeResponse);
        }
        else if (this.queryMatches(translation.node_bedroom_query_lookKeys())) {
            var slide = new DemoSlide_1.DemoSlide();
            slide.setText(translation.node_bedroom_text_lookKeys());
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, [
                slide,
            ]);
            this.setNodeResponse(nodeResponse);
        }
        else if (this.queryMatches(translation.node_bedroom_query_takeKeys())) {
            variables.setHasKeys(true);
            var slide = new DemoSlide_1.DemoSlide();
            slide.setText(translation.node_bedroom_text_takeKeys());
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, [
                slide,
            ]);
            this.setNodeResponse(nodeResponse);
        }
        else if (this.queryMatches(translation.node_bedroom_query_goCorridor())) {
            var nodeResponse = this.buildNodeResponse(configuration, variables, status, []);
            nodeResponse = this.getNodeBrowser().change(nodeResponse, DemoCorridorNode_1.DemoCorridorNode.ID);
            this.setNodeResponse(nodeResponse);
        }
    };
    DemoBedroomNode.prototype.buildLook = function (variables) {
        var translation = this.getTranslation(), text = translation.node_bedroom_text_look_1();
        if (!variables.getHasKeys()) {
            text += translation.node_bedroom_text_look_2();
        }
        text += translation.node_bedroom_text_look_3();
        return text;
    };
    DemoBedroomNode.ID = 'bedroom';
    return DemoBedroomNode;
}(DemoNodeAbs_1.DemoNodeAbs));
exports.DemoBedroomNode = DemoBedroomNode;
