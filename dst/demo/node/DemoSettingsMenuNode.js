"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DemoSettingsMenuNode = void 0;
var DemoNodeAbs_1 = require("../library/node/DemoNodeAbs");
var DemoSlide_1 = require("../library/slide/DemoSlide");
var DemoSettingsMenuNode = /** @class */ (function (_super) {
    __extends(DemoSettingsMenuNode, _super);
    function DemoSettingsMenuNode() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DemoSettingsMenuNode.prototype.nodeRun = function (status, variables) {
        if (!this.hasNodeRequestQuery()) {
            var slide = new DemoSlide_1.DemoSlide();
            slide.setTitle('Settings Menu');
            slide.setText('<p>Choose an option:</p><p><span class="verb">1</span>) Back</p>');
            var nodeResponse = this.buildNodeResponse(status, variables, [
                slide,
            ]);
            this.setNodeResponse(nodeResponse);
            return;
        }
        var query = this.getNodeRequestQueryAfterHas();
        if (query == '1') {
            var nodeResponse = this.buildNodeResponse(status, variables, []);
            nodeResponse = this.getNodeBrowser().pop(nodeResponse);
            this.setNodeResponse(nodeResponse);
        }
    };
    DemoSettingsMenuNode.CODE = 'settings_menu';
    return DemoSettingsMenuNode;
}(DemoNodeAbs_1.DemoNodeAbs));
exports.DemoSettingsMenuNode = DemoSettingsMenuNode;
