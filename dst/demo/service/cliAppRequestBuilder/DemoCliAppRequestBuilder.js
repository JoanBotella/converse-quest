"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DemoCliAppRequestBuilder = void 0;
var CoreCliAppRequestBuilderAbs_1 = require("../../../converseQuest/core/library/cliAppRequestBuilder/CoreCliAppRequestBuilderAbs");
var DemoAppRequest_1 = require("../../library/appRequest/DemoAppRequest");
var DemoCliAppRequestBuilder = /** @class */ (function (_super) {
    __extends(DemoCliAppRequestBuilder, _super);
    function DemoCliAppRequestBuilder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DemoCliAppRequestBuilder.prototype.instantiate = function (status) {
        return new DemoAppRequest_1.DemoAppRequest(status);
    };
    return DemoCliAppRequestBuilder;
}(CoreCliAppRequestBuilderAbs_1.CoreCliAppRequestBuilderAbs));
exports.DemoCliAppRequestBuilder = DemoCliAppRequestBuilder;
