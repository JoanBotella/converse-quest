"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DemoSystemNodeResponseBuilder = void 0;
var CoreSystemNodeResponseBuilderAbs_1 = require("../../../converseQuest/core/library/systemNodeResponseBuilder/CoreSystemNodeResponseBuilderAbs");
var DemoNodeResponse_1 = require("../../library/nodeResponse/DemoNodeResponse");
var DemoSlide_1 = require("../../library/slide/DemoSlide");
var DemoConfigurationMenuNode_1 = require("../../node/DemoConfigurationMenuNode");
var DemoSystemNodeResponseBuilder = /** @class */ (function (_super) {
    __extends(DemoSystemNodeResponseBuilder, _super);
    function DemoSystemNodeResponseBuilder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DemoSystemNodeResponseBuilder.prototype.buildNodeResponse = function () {
        var nodeRequest = this.getNodeRequest(), status = nodeRequest.getStatus(), configuration = status.getConfiguration(), variables = status.getVariables(), translation = this.getTranslation();
        if (!this.hasNodeRequestQuery()) {
            return new DemoNodeResponse_1.DemoNodeResponse(status);
        }
        if (this.queryMatches(translation.systemNodeResponseBuilder_query_exit())) {
            var nodeResponse = new DemoNodeResponse_1.DemoNodeResponse(status);
            var slide = new DemoSlide_1.DemoSlide();
            slide.setText(translation.systemNodeResponseBuilder_text_exit());
            nodeResponse.appendSlide(slide);
            nodeResponse.setEnd(true);
            return nodeResponse;
        }
        if (this.queryMatches(translation.systemNodeResponseBuilder_query_inventory())) {
            var nodeResponse = new DemoNodeResponse_1.DemoNodeResponse(status);
            var slide = new DemoSlide_1.DemoSlide();
            slide.setText(this.buildInventoryText());
            nodeResponse.appendSlide(slide);
            return nodeResponse;
        }
        if (this.queryMatches(translation.systemNodeResponseBuilder_query_configuration())) {
            var nodeResponse = new DemoNodeResponse_1.DemoNodeResponse(status);
            nodeResponse = this.getNodeBrowser().push(nodeResponse, DemoConfigurationMenuNode_1.DemoConfigurationMenuNode.ID);
            return nodeResponse;
        }
        if (variables.getHasKeys()
            && this.queryMatches(translation.systemNodeResponseBuilder_query_lookKeys())) {
            var nodeResponse = new DemoNodeResponse_1.DemoNodeResponse(status);
            var slide = new DemoSlide_1.DemoSlide();
            slide.setText(translation.systemNodeResponseBuilder_text_lookKeys());
            nodeResponse.appendSlide(slide);
            return nodeResponse;
        }
        return this.buildNotUnderstoodNodeResponse();
    };
    DemoSystemNodeResponseBuilder.prototype.buildInventoryText = function () {
        var nodeRequest = this.getNodeRequest(), status = nodeRequest.getStatus(), configuration = status.getConfiguration(), variables = status.getVariables(), translation = this.getTranslation(), text = '';
        if (variables.getHasKeys()) {
            text += translation.systemNodeResponseBuilder_text_inventory_keys();
        }
        if (text == '') {
            text += translation.systemNodeResponseBuilder_text_inventory_empty();
        }
        return text;
    };
    DemoSystemNodeResponseBuilder.prototype.buildNotUnderstoodNodeResponse = function () {
        var nodeResponse = new DemoNodeResponse_1.DemoNodeResponse(this.getNodeRequest().getStatus());
        var slide = new DemoSlide_1.DemoSlide();
        slide.setText(this.getTranslation().systemNodeResponseBuilder_text_notUnderstood());
        nodeResponse.appendSlide(slide);
        return nodeResponse;
    };
    return DemoSystemNodeResponseBuilder;
}(CoreSystemNodeResponseBuilderAbs_1.CoreSystemNodeResponseBuilderAbs));
exports.DemoSystemNodeResponseBuilder = DemoSystemNodeResponseBuilder;
