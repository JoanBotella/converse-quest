"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DemoNextNodeRequestBuilder = void 0;
var CoreNextNodeRequestBuilder_1 = require("../../../converseQuest/core/library/nextNodeRequestBuilder/CoreNextNodeRequestBuilder");
var DemoNodeRequest_1 = require("../../library/nodeRequest/DemoNodeRequest");
var DemoNextNodeRequestBuilder = /** @class */ (function (_super) {
    __extends(DemoNextNodeRequestBuilder, _super);
    function DemoNextNodeRequestBuilder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DemoNextNodeRequestBuilder.prototype.build = function (nodeResponse) {
        return new DemoNodeRequest_1.DemoNodeRequest(nodeResponse.getStatus());
    };
    return DemoNextNodeRequestBuilder;
}(CoreNextNodeRequestBuilder_1.CoreNextNodeRequestBuilderAbs));
exports.DemoNextNodeRequestBuilder = DemoNextNodeRequestBuilder;
