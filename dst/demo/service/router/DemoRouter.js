"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DemoRouter = void 0;
var CoreRouterAbs_1 = require("../../../converseQuest/core/library/router/CoreRouterAbs");
var DemoTitleMenuNode_1 = require("../../node/DemoTitleMenuNode");
var DemoBedroomNode_1 = require("../../node/DemoBedroomNode");
var DemoCorridorNode_1 = require("../../node/DemoCorridorNode");
var DemoLivingRoomNode_1 = require("../../node/DemoLivingRoomNode");
var DemoTalkJoanNode_1 = require("../../node/DemoTalkJoanNode");
var DemoConfigurationMenuNode_1 = require("../../node/DemoConfigurationMenuNode");
var DemoLanguageMenuNode_1 = require("../../node/DemoLanguageMenuNode");
var DemoRouter = /** @class */ (function (_super) {
    __extends(DemoRouter, _super);
    function DemoRouter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DemoRouter.prototype.route = function (nodeId) {
        var nodeBuilder = this.getNodeBuilder();
        switch (nodeId) {
            case DemoBedroomNode_1.DemoBedroomNode.ID: return nodeBuilder.buildBedroomNode();
            case DemoConfigurationMenuNode_1.DemoConfigurationMenuNode.ID: return nodeBuilder.buildSettingsMenuNode();
            case DemoCorridorNode_1.DemoCorridorNode.ID: return nodeBuilder.buildCorridorNode();
            case DemoLanguageMenuNode_1.DemoLanguageMenuNode.ID: return nodeBuilder.buildLanguageMenuNode();
            case DemoLivingRoomNode_1.DemoLivingRoomNode.ID: return nodeBuilder.buildLivingRoomNode();
            case DemoTalkJoanNode_1.DemoTalkJoanNode.ID: return nodeBuilder.buildTalkJoanNode();
            case DemoTitleMenuNode_1.DemoTitleMenuNode.ID: return nodeBuilder.buildTitleMenuNode();
            default:
                throw new Error('The node id "' + nodeId + '" is unroutable.');
        }
    };
    return DemoRouter;
}(CoreRouterAbs_1.CoreRouterAbs));
exports.DemoRouter = DemoRouter;
