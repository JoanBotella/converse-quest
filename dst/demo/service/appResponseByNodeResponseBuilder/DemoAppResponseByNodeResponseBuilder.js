"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DemoAppResponseByNodeResponseBuilder = void 0;
var CoreAppResponseByNodeResponseBuilderAbs_1 = require("../../../converseQuest/core/library/appResponseByNodeResponseBuilder/CoreAppResponseByNodeResponseBuilderAbs");
var DemoAppResponse_1 = require("../../library/appResponse/DemoAppResponse");
var DemoAppResponseByNodeResponseBuilder = /** @class */ (function (_super) {
    __extends(DemoAppResponseByNodeResponseBuilder, _super);
    function DemoAppResponseByNodeResponseBuilder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DemoAppResponseByNodeResponseBuilder.prototype.instantiate = function (status) {
        return new DemoAppResponse_1.DemoAppResponse(status);
    };
    return DemoAppResponseByNodeResponseBuilder;
}(CoreAppResponseByNodeResponseBuilderAbs_1.CoreAppResponseByNodeResponseBuilderAbs));
exports.DemoAppResponseByNodeResponseBuilder = DemoAppResponseByNodeResponseBuilder;
