"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DemoNodeBuilder = void 0;
var DemoBedroomNode_1 = require("../../node/DemoBedroomNode");
var DemoCorridorNode_1 = require("../../node/DemoCorridorNode");
var DemoLivingRoomNode_1 = require("../../node/DemoLivingRoomNode");
var DemoTalkJoanNode_1 = require("../../node/DemoTalkJoanNode");
var DemoTitleMenuNode_1 = require("../../node/DemoTitleMenuNode");
var CoreNodeBuilderAbs_1 = require("../../../converseQuest/core/library/nodeBuilder/CoreNodeBuilderAbs");
var DemoConfigurationMenuNode_1 = require("../../node/DemoConfigurationMenuNode");
var DemoLanguageMenuNode_1 = require("../../node/DemoLanguageMenuNode");
var DemoNodeBuilder = /** @class */ (function (_super) {
    __extends(DemoNodeBuilder, _super);
    function DemoNodeBuilder(serviceContainer) {
        var _this = _super.call(this) || this;
        _this.serviceContainer = serviceContainer;
        return _this;
    }
    DemoNodeBuilder.prototype.buildBedroomNode = function () {
        return new DemoBedroomNode_1.DemoBedroomNode(this.serviceContainer.getNodeBrowser(), this.serviceContainer.getTranslationContainer());
    };
    DemoNodeBuilder.prototype.buildCorridorNode = function () {
        return new DemoCorridorNode_1.DemoCorridorNode(this.serviceContainer.getNodeBrowser(), this.serviceContainer.getTranslationContainer());
    };
    DemoNodeBuilder.prototype.buildLivingRoomNode = function () {
        return new DemoLivingRoomNode_1.DemoLivingRoomNode(this.serviceContainer.getNodeBrowser(), this.serviceContainer.getTranslationContainer());
    };
    DemoNodeBuilder.prototype.buildTalkJoanNode = function () {
        return new DemoTalkJoanNode_1.DemoTalkJoanNode(this.serviceContainer.getNodeBrowser(), this.serviceContainer.getTranslationContainer());
    };
    DemoNodeBuilder.prototype.buildTitleMenuNode = function () {
        return new DemoTitleMenuNode_1.DemoTitleMenuNode(this.serviceContainer.getNodeBrowser(), this.serviceContainer.getTranslationContainer());
    };
    DemoNodeBuilder.prototype.buildSettingsMenuNode = function () {
        return new DemoConfigurationMenuNode_1.DemoConfigurationMenuNode(this.serviceContainer.getNodeBrowser(), this.serviceContainer.getTranslationContainer());
    };
    DemoNodeBuilder.prototype.buildLanguageMenuNode = function () {
        return new DemoLanguageMenuNode_1.DemoLanguageMenuNode(this.serviceContainer.getNodeBrowser(), this.serviceContainer.getTranslationContainer());
    };
    return DemoNodeBuilder;
}(CoreNodeBuilderAbs_1.CoreNodeBuilderAbs));
exports.DemoNodeBuilder = DemoNodeBuilder;
