"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DemoTranslationContainer = void 0;
var CoreTranslationContainerAbs_1 = require("../../../converseQuest/core/library/translationContainer/CoreTranslationContainerAbs");
var DemoEnTranslation_1 = require("../../library/translation/DemoEnTranslation");
var DemoEsTranslation_1 = require("../../library/translation/DemoEsTranslation");
var DemoTranslationContainer = /** @class */ (function (_super) {
    __extends(DemoTranslationContainer, _super);
    function DemoTranslationContainer() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DemoTranslationContainer.prototype.buildTranslation = function (languageCode) {
        switch (languageCode) {
            case DemoEnTranslation_1.DemoEnTranslation.CODE: return new DemoEnTranslation_1.DemoEnTranslation();
            case DemoEsTranslation_1.DemoEsTranslation.CODE: return new DemoEsTranslation_1.DemoEsTranslation();
        }
        throw new Error('There is no translation for the language code "' + languageCode + '"');
    };
    return DemoTranslationContainer;
}(CoreTranslationContainerAbs_1.CoreTranslationContainerAbs));
exports.DemoTranslationContainer = DemoTranslationContainer;
