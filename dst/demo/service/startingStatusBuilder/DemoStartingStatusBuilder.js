"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DemoStartingStatusBuilder = void 0;
var CoreStartingStatusBuilderAbs_1 = require("../../../converseQuest/core/library/startingStatusBuilder/CoreStartingStatusBuilderAbs");
var DemoStatus_1 = require("../../library/status/DemoStatus");
var DemoTitleMenuNode_1 = require("../../node/DemoTitleMenuNode");
var DemoConfiguration_1 = require("../../library/configuration/DemoConfiguration");
var DemoVariables_1 = require("../../library/variables/DemoVariables");
var DemoEnTranslation_1 = require("../../library/translation/DemoEnTranslation");
var DemoStartingStatusBuilder = /** @class */ (function (_super) {
    __extends(DemoStartingStatusBuilder, _super);
    function DemoStartingStatusBuilder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DemoStartingStatusBuilder.prototype.build = function () {
        return new DemoStatus_1.DemoStatus(DemoTitleMenuNode_1.DemoTitleMenuNode.ID, new DemoConfiguration_1.DemoConfiguration(DemoEnTranslation_1.DemoEnTranslation.CODE), new DemoVariables_1.DemoVariables());
    };
    return DemoStartingStatusBuilder;
}(CoreStartingStatusBuilderAbs_1.CoreStartingStatusBuilderAbs));
exports.DemoStartingStatusBuilder = DemoStartingStatusBuilder;
