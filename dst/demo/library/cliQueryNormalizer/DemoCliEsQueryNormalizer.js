"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DemoCliEsQueryNormalizer = void 0;
var CoreCliQueryNormalizerAbs_1 = require("../../../converseQuest/core/library/cliQueryNormalizer/CoreCliQueryNormalizerAbs");
var DemoCliEsQueryNormalizer = /** @class */ (function (_super) {
    __extends(DemoCliEsQueryNormalizer, _super);
    function DemoCliEsQueryNormalizer() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DemoCliEsQueryNormalizer.prototype.normalize = function (query) {
        query = query.toLowerCase();
        query = query.replace(/á/g, 'a');
        query = query.replace(/é/g, 'e');
        query = query.replace(/í/g, 'i');
        query = query.replace(/ó/g, 'o');
        query = query.replace(/ú/g, 'u');
        query = query.replace(/ (el|la|los|las|hacia|a|al|en|con) /g, ' ');
        return query;
    };
    return DemoCliEsQueryNormalizer;
}(CoreCliQueryNormalizerAbs_1.CoreCliQueryNormalizerAbs));
exports.DemoCliEsQueryNormalizer = DemoCliEsQueryNormalizer;
