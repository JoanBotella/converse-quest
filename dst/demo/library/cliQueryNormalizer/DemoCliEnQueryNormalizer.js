"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DemoCliEnQueryNormalizer = void 0;
var CoreCliQueryNormalizerAbs_1 = require("../../../converseQuest/core/library/cliQueryNormalizer/CoreCliQueryNormalizerAbs");
var DemoCliEnQueryNormalizer = /** @class */ (function (_super) {
    __extends(DemoCliEnQueryNormalizer, _super);
    function DemoCliEnQueryNormalizer() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DemoCliEnQueryNormalizer.prototype.normalize = function (query) {
        query = query.toLowerCase();
        query = query.replace(/ (a|the|at|to|through|up) /g, ' ');
        return query;
    };
    return DemoCliEnQueryNormalizer;
}(CoreCliQueryNormalizerAbs_1.CoreCliQueryNormalizerAbs));
exports.DemoCliEnQueryNormalizer = DemoCliEnQueryNormalizer;
