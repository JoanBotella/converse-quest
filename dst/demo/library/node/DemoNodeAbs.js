"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DemoNodeAbs = void 0;
var CoreNodeAbs_1 = require("../../../converseQuest/core/library/node/CoreNodeAbs");
var DemoNodeResponse_1 = require("../nodeResponse/DemoNodeResponse");
var DemoNodeAbs = /** @class */ (function (_super) {
    __extends(DemoNodeAbs, _super);
    function DemoNodeAbs() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DemoNodeAbs.prototype.buildNodeResponse = function (configuration, variables, status, slides) {
        status.setConfiguration(configuration);
        status.setVariables(variables);
        var nodeResponse = new DemoNodeResponse_1.DemoNodeResponse(status);
        nodeResponse.setSlides(slides);
        return nodeResponse;
    };
    return DemoNodeAbs;
}(CoreNodeAbs_1.CoreNodeAbs));
exports.DemoNodeAbs = DemoNodeAbs;
