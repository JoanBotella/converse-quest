"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DemoEnTranslation = void 0;
var CoreTranslationAbs_1 = require("../../../converseQuest/core/library/translation/CoreTranslationAbs");
var DemoEnTranslation = /** @class */ (function (_super) {
    __extends(DemoEnTranslation, _super);
    function DemoEnTranslation() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DemoEnTranslation.prototype.node_bedroom_query_goCorridor = function () { return 'go corridor'; };
    DemoEnTranslation.prototype.node_bedroom_query_look = function () { return 'look'; };
    DemoEnTranslation.prototype.node_bedroom_query_lookKeys = function () { return 'look keys'; };
    DemoEnTranslation.prototype.node_bedroom_query_takeKeys = function () { return 'take keys'; };
    DemoEnTranslation.prototype.node_bedroom_text_lookKeys = function () { return '<p>A lot of <span class="item">keys</span> joined by a keychain. I have there the key of the house and the one for the motorbike.</p>'; };
    DemoEnTranslation.prototype.node_bedroom_text_look_1 = function () { return '<p>This is my bedroom.'; };
    DemoEnTranslation.prototype.node_bedroom_text_look_2 = function () { return ' My <span class="item">keys</span> are on the desktop.'; };
    DemoEnTranslation.prototype.node_bedroom_text_look_3 = function () { return '</p><p>I can exit to the <span class="exit">corridor</span>.</p>'; };
    DemoEnTranslation.prototype.node_bedroom_text_takeKeys = function () { return '<p>Taken.</p>'; };
    DemoEnTranslation.prototype.node_bedroom_title = function () { return 'Bedroom'; };
    DemoEnTranslation.prototype.node_configurationMenu_query_1 = function () { return '1'; };
    DemoEnTranslation.prototype.node_configurationMenu_query_2 = function () { return '2'; };
    DemoEnTranslation.prototype.node_configurationMenu_text_look = function () { return '<p>Choose an option:</p><p><span class="verb">1</span>) Language</p><p><span class="verb">2</span>) Back</p>'; };
    DemoEnTranslation.prototype.node_configurationMenu_title = function () { return 'Configuration Menu'; };
    DemoEnTranslation.prototype.node_corridor_query_goBedroom = function () { return 'go bedroom'; };
    DemoEnTranslation.prototype.node_corridor_query_goLivingRoom = function () { return 'go living room'; };
    DemoEnTranslation.prototype.node_corridor_query_look = function () { return 'look'; };
    DemoEnTranslation.prototype.node_corridor_text_look = function () { return '<p>This is the corridor.</p><p>I can go back to my <span class="exit">bedroom</span> or enter the <span class="exit">living room</span>.</p>'; };
    DemoEnTranslation.prototype.node_corridor_title = function () { return 'Corridor'; };
    DemoEnTranslation.prototype.node_languageMenu_query_1 = function () { return '1'; };
    DemoEnTranslation.prototype.node_languageMenu_query_2 = function () { return '2'; };
    DemoEnTranslation.prototype.node_languageMenu_text_look = function () { return '<p>Choose an option:</p><p><span class="verb">1</span>) English</p><p><span class="verb">2</span>) Español</p>'; };
    DemoEnTranslation.prototype.node_languageMenu_title = function () { return 'Language Menu'; };
    DemoEnTranslation.prototype.node_livingRoom_query_goCorridor = function () { return 'go corridor'; };
    DemoEnTranslation.prototype.node_livingRoom_query_look = function () { return 'look'; };
    DemoEnTranslation.prototype.node_livingRoom_query_lookJoan = function () { return 'look joan'; };
    DemoEnTranslation.prototype.node_livingRoom_query_talkJoan = function () { return 'talk joan'; };
    DemoEnTranslation.prototype.node_livingRoom_text_look = function () { return '<p>This is the living room. My friend <span class="character">Joan</span> is on the sofa.</p><p>I can go back to the <span class="exit">corridor</span>.</p>'; };
    DemoEnTranslation.prototype.node_livingRoom_text_lookJoan = function () { return '<p>He\'s reading something.</p>'; };
    DemoEnTranslation.prototype.node_livingRoom_title = function () { return 'Living Room'; };
    DemoEnTranslation.prototype.node_talkJoan_query_1 = function () { return '1'; };
    DemoEnTranslation.prototype.node_talkJoan_text_1 = function () { return '<p class="talking">Ok</p>'; };
    DemoEnTranslation.prototype.node_talkJoan_text_look = function () { return '<p class="talking">Hello buddy. Need something?</p><p><span class="verb">1</span>) No, thanks.</p>'; };
    DemoEnTranslation.prototype.node_titleMenu_query_1 = function () { return '1'; };
    DemoEnTranslation.prototype.node_titleMenu_query_2 = function () { return '2'; };
    DemoEnTranslation.prototype.node_titleMenu_query_3 = function () { return '3'; };
    DemoEnTranslation.prototype.node_titleMenu_text_1 = function () { return '<p>Get ready!</p>'; };
    DemoEnTranslation.prototype.node_titleMenu_text_3 = function () { return '<p>Leaving already?</p>'; };
    DemoEnTranslation.prototype.node_titleMenu_text_look = function () { return '<p>Choose an option:</p><p><span class="verb">1</span>) Start game</p><p><span class="verb">2</span>) Configuration</p><p><span class="verb">3</span>) Quit</p>'; };
    DemoEnTranslation.prototype.node_titleMenu_title = function () { return 'Title Menu'; };
    DemoEnTranslation.prototype.systemNodeResponseBuilder_query_configuration = function () { return 'c,configuration'; };
    DemoEnTranslation.prototype.systemNodeResponseBuilder_query_exit = function () { return 'quit,exit'; };
    DemoEnTranslation.prototype.systemNodeResponseBuilder_query_inventory = function () { return 'i,inventory'; };
    DemoEnTranslation.prototype.systemNodeResponseBuilder_query_lookKeys = function () { return 'look keys'; };
    DemoEnTranslation.prototype.systemNodeResponseBuilder_text_exit = function () { return '<p>Bye!</p>'; };
    DemoEnTranslation.prototype.systemNodeResponseBuilder_text_inventory_empty = function () { return '<p>I\'m empty-handed.</p>'; };
    DemoEnTranslation.prototype.systemNodeResponseBuilder_text_inventory_keys = function () { return '<p>I have my <span class="item">keys</span> with me.</p>'; };
    DemoEnTranslation.prototype.systemNodeResponseBuilder_text_lookKeys = function () { return '<p>A lot of keys joined by a keychain. I have there the key of the house and the one for the motorbike.</p>'; };
    DemoEnTranslation.prototype.systemNodeResponseBuilder_text_notUnderstood = function () { return '<p>What?</p>'; };
    DemoEnTranslation.CODE = 'en';
    return DemoEnTranslation;
}(CoreTranslationAbs_1.CoreTranslationAbs));
exports.DemoEnTranslation = DemoEnTranslation;
