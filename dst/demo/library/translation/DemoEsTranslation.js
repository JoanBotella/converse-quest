"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DemoEsTranslation = void 0;
var CoreTranslationAbs_1 = require("../../../converseQuest/core/library/translation/CoreTranslationAbs");
var DemoEsTranslation = /** @class */ (function (_super) {
    __extends(DemoEsTranslation, _super);
    function DemoEsTranslation() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DemoEsTranslation.prototype.node_bedroom_query_goCorridor = function () { return 'ir pasillo'; };
    DemoEsTranslation.prototype.node_bedroom_query_look = function () { return 'mirar'; };
    DemoEsTranslation.prototype.node_bedroom_query_lookKeys = function () { return 'mirar llaves'; };
    DemoEsTranslation.prototype.node_bedroom_query_takeKeys = function () { return 'coger llaves'; };
    DemoEsTranslation.prototype.node_bedroom_text_lookKeys = function () { return '<p>Muchas <span class="item">llaves</span> en un llavero. Tengo ahí la llave de la casa y la de la moto.</p>'; };
    DemoEsTranslation.prototype.node_bedroom_text_look_1 = function () { return '<p>Ésta es mi habitación.'; };
    DemoEsTranslation.prototype.node_bedroom_text_look_2 = function () { return ' Mis <span class="item">llaves</span> están sobre el escritorio.'; };
    DemoEsTranslation.prototype.node_bedroom_text_look_3 = function () { return '</p><p>Puedo salir al <span class="exit">pasillo</span>.</p>'; };
    DemoEsTranslation.prototype.node_bedroom_text_takeKeys = function () { return '<p>Cogidas.</p>'; };
    DemoEsTranslation.prototype.node_bedroom_title = function () { return 'Dormitorio'; };
    DemoEsTranslation.prototype.node_configurationMenu_query_1 = function () { return '1'; };
    DemoEsTranslation.prototype.node_configurationMenu_query_2 = function () { return '2'; };
    DemoEsTranslation.prototype.node_configurationMenu_text_look = function () { return '<p>Elige una opción:</p><p><span class="verb">1</span>) Idioma</p><p><span class="verb">2</span>) Atrás</p>'; };
    DemoEsTranslation.prototype.node_configurationMenu_title = function () { return 'Menú de Configuración'; };
    DemoEsTranslation.prototype.node_corridor_query_goBedroom = function () { return 'ir dormitorio'; };
    DemoEsTranslation.prototype.node_corridor_query_goLivingRoom = function () { return 'ir salon'; };
    DemoEsTranslation.prototype.node_corridor_query_look = function () { return 'mirar'; };
    DemoEsTranslation.prototype.node_corridor_text_look = function () { return '<p>Éste es el pasillo.</p><p>Puedo volver a mi <span class="exit">dormitorio</span> o entrar en el <span class="exit">salón</span>.</p>'; };
    DemoEsTranslation.prototype.node_corridor_title = function () { return 'Pasillo'; };
    DemoEsTranslation.prototype.node_languageMenu_query_1 = function () { return '1'; };
    DemoEsTranslation.prototype.node_languageMenu_query_2 = function () { return '2'; };
    DemoEsTranslation.prototype.node_languageMenu_text_look = function () { return '<p>Elige una opción:</p><p><span class="verb">1</span>) English</p><p><span class="verb">2</span>) Español</p>'; };
    DemoEsTranslation.prototype.node_languageMenu_title = function () { return 'Menú de Idioma'; };
    DemoEsTranslation.prototype.node_livingRoom_query_goCorridor = function () { return 'ir pasillo'; };
    DemoEsTranslation.prototype.node_livingRoom_query_look = function () { return 'mirar'; };
    DemoEsTranslation.prototype.node_livingRoom_query_lookJoan = function () { return 'mirar joan'; };
    DemoEsTranslation.prototype.node_livingRoom_query_talkJoan = function () { return 'hablar joan'; };
    DemoEsTranslation.prototype.node_livingRoom_text_look = function () { return '<p>Éste es el salón. Mi amigo <span class="character">Joan</span> está en el sofá.</p><p>Puedo volver al <span class="exit">pasillo</span>.</p>'; };
    DemoEsTranslation.prototype.node_livingRoom_text_lookJoan = function () { return '<p>Está leyendo algo.</p>'; };
    DemoEsTranslation.prototype.node_livingRoom_title = function () { return 'Salón'; };
    DemoEsTranslation.prototype.node_talkJoan_query_1 = function () { return '1'; };
    DemoEsTranslation.prototype.node_talkJoan_text_1 = function () { return '<p class="talking">Ok</p>'; };
    DemoEsTranslation.prototype.node_talkJoan_text_look = function () { return '<p class="talking">Hola compi. ¿Necesitas algo?</p><p><span class="verb">1</span>) No, gracias.</p>'; };
    DemoEsTranslation.prototype.node_titleMenu_query_1 = function () { return '1'; };
    DemoEsTranslation.prototype.node_titleMenu_query_2 = function () { return '2'; };
    DemoEsTranslation.prototype.node_titleMenu_query_3 = function () { return '3'; };
    DemoEsTranslation.prototype.node_titleMenu_text_1 = function () { return '<p>¡Prepárate!</p>'; };
    DemoEsTranslation.prototype.node_titleMenu_text_3 = function () { return '<p>¿Ya te vas?</p>'; };
    DemoEsTranslation.prototype.node_titleMenu_text_look = function () { return '<p>Elige una opción:</p><p><span class="verb">1</span>) Empezar el juego</p><p><span class="verb">2</span>) Configuración</p><p><span class="verb">3</span>) Salir</p>'; };
    DemoEsTranslation.prototype.node_titleMenu_title = function () { return 'Menú de Título'; };
    DemoEsTranslation.prototype.systemNodeResponseBuilder_query_configuration = function () { return 'c,configuracion'; };
    DemoEsTranslation.prototype.systemNodeResponseBuilder_query_exit = function () { return 'salir'; };
    DemoEsTranslation.prototype.systemNodeResponseBuilder_query_inventory = function () { return 'i,inventario'; };
    DemoEsTranslation.prototype.systemNodeResponseBuilder_query_lookKeys = function () { return 'mirar llaves'; };
    DemoEsTranslation.prototype.systemNodeResponseBuilder_text_exit = function () { return '<p>¡Adiós!</p>'; };
    DemoEsTranslation.prototype.systemNodeResponseBuilder_text_inventory_empty = function () { return '<p>Tengo las manos vacías.</p>'; };
    DemoEsTranslation.prototype.systemNodeResponseBuilder_text_inventory_keys = function () { return '<p>Tengo mis <span class="item">llaves</span> conmigo.</p>'; };
    DemoEsTranslation.prototype.systemNodeResponseBuilder_text_lookKeys = function () { return '<p>Muchas llaves unidas por un llavero. Ahí tengo la llave de la casa y de la moto.</p>'; };
    DemoEsTranslation.prototype.systemNodeResponseBuilder_text_notUnderstood = function () { return '<p>¿Qué?</p>'; };
    DemoEsTranslation.CODE = 'es';
    return DemoEsTranslation;
}(CoreTranslationAbs_1.CoreTranslationAbs));
exports.DemoEsTranslation = DemoEsTranslation;
