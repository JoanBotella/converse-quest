"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DemoServiceContainer = void 0;
var DemoCliAppRunner_1 = require("../../service/cliAppRunner/DemoCliAppRunner");
var DemoApp_1 = require("../../service/app/DemoApp");
var DemoCliAppRequestBuilder_1 = require("../../service/cliAppRequestBuilder/DemoCliAppRequestBuilder");
var DemoCliAppResponseDispatcher_1 = require("../../service/cliAppResponseDispatcher/DemoCliAppResponseDispatcher");
var DemoRouter_1 = require("../../service/router/DemoRouter");
var DemoCliPrompter_1 = require("../../service/cliPrompter/DemoCliPrompter");
var DemoCliQueryBuilder_1 = require("../../service/cliQueryBuilder/DemoCliQueryBuilder");
var DemoNodeRequestByAppRequestBuilder_1 = require("../../service/nodeRequestByAppRequestBuilder/DemoNodeRequestByAppRequestBuilder");
var DemoSystemNodeResponseBuilder_1 = require("../../service/systemNodeResponseBuilder/DemoSystemNodeResponseBuilder");
var DemoAppResponseByNodeResponseBuilder_1 = require("../../service/appResponseByNodeResponseBuilder/DemoAppResponseByNodeResponseBuilder");
var DemoNextNodeRequestBuilder_1 = require("../../service/nextNodeRequestBuilder/DemoNextNodeRequestBuilder");
var DemoNodeRequestProcessor_1 = require("../../service/nodeRequestProcessor/DemoNodeRequestProcessor");
var DemoStartingStatusBuilder_1 = require("../../service/startingStatusBuilder/DemoStartingStatusBuilder");
var DemoNodeBuilder_1 = require("../../service/nodeBuilder/DemoNodeBuilder");
var CoreCliStylizer_1 = require("../../../converseQuest/core/service/cliStylizer/CoreCliStylizer");
var CoreServiceContainerAbs_1 = require("../../../converseQuest/core/library/serviceContainer/CoreServiceContainerAbs");
var DemoNodeBrowser_1 = require("../../service/nodeBrowser/DemoNodeBrowser");
var DemoTranslationContainer_1 = require("../../service/translationContainer/DemoTranslationContainer");
var DemoCliQueryNormalizerContainer_1 = require("../../service/cliQueryNormalizerContainer/DemoCliQueryNormalizerContainer");
var DemoServiceContainer = /** @class */ (function (_super) {
    __extends(DemoServiceContainer, _super);
    function DemoServiceContainer() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DemoServiceContainer.prototype.buildCliAppRunner = function () {
        return new DemoCliAppRunner_1.DemoCliAppRunner(this.getStartingStatusBuilder(), this.getCliAppRequestBuilder(), this.getApp(), this.getCliAppResponseDispatcher());
    };
    DemoServiceContainer.prototype.buildCliAppRequestBuilder = function () {
        return new DemoCliAppRequestBuilder_1.DemoCliAppRequestBuilder(this.getCliQueryBuilder());
    };
    DemoServiceContainer.prototype.buildApp = function () {
        return new DemoApp_1.DemoApp(this.getNodeRequestByAppRequestBuilder(), this.getAppResponseByNodeResponseBuilder(), this.getNextNodeRequestBuilder(), this.getNodeRequestProcessor());
    };
    DemoServiceContainer.prototype.buildCliAppResponseDispatcher = function () {
        return new DemoCliAppResponseDispatcher_1.DemoCliAppResponseDispatcher(this.getCliStylizer(), this.getCliPrompter(), this.getTranslationContainer());
    };
    DemoServiceContainer.prototype.buildRouter = function () {
        return new DemoRouter_1.DemoRouter(this.getNodeBuilder());
    };
    DemoServiceContainer.prototype.buildCliStylizer = function () {
        return new CoreCliStylizer_1.CoreCliStylizer();
    };
    DemoServiceContainer.prototype.buildCliQueryNormalizerContainer = function () {
        return new DemoCliQueryNormalizerContainer_1.DemoCliQueryNormalizerContainer();
    };
    DemoServiceContainer.prototype.buildCliPrompter = function () {
        return new DemoCliPrompter_1.DemoCliPrompter(this.getCliStylizer());
    };
    DemoServiceContainer.prototype.buildCliQueryBuilder = function () {
        return new DemoCliQueryBuilder_1.DemoCliQueryBuilder(this.getCliPrompter(), this.getCliQueryNormalizerContainer(), this.getTranslationContainer());
    };
    DemoServiceContainer.prototype.buildNodeRequestByAppRequestBuilder = function () {
        return new DemoNodeRequestByAppRequestBuilder_1.DemoNodeRequestByAppRequestBuilder();
    };
    DemoServiceContainer.prototype.buildSystemNodeResponseBuilder = function () {
        return new DemoSystemNodeResponseBuilder_1.DemoSystemNodeResponseBuilder(this.getNodeBrowser(), this.getTranslationContainer());
    };
    DemoServiceContainer.prototype.buildAppResponseByNodeResponseBuilder = function () {
        return new DemoAppResponseByNodeResponseBuilder_1.DemoAppResponseByNodeResponseBuilder();
    };
    DemoServiceContainer.prototype.buildNextNodeRequestBuilder = function () {
        return new DemoNextNodeRequestBuilder_1.DemoNextNodeRequestBuilder();
    };
    DemoServiceContainer.prototype.buildNodeRequestProcessor = function () {
        return new DemoNodeRequestProcessor_1.DemoNodeRequestProcessor(this.getSystemNodeResponseBuilder(), this.getRouter());
    };
    DemoServiceContainer.prototype.buildStartingStatusBuilder = function () {
        return new DemoStartingStatusBuilder_1.DemoStartingStatusBuilder();
    };
    DemoServiceContainer.prototype.buildNodeBuilder = function () {
        return new DemoNodeBuilder_1.DemoNodeBuilder(this);
    };
    DemoServiceContainer.prototype.buildNodeBrowser = function () {
        return new DemoNodeBrowser_1.DemoNodeBrowser();
    };
    DemoServiceContainer.prototype.buildTranslationContainer = function () {
        return new DemoTranslationContainer_1.DemoTranslationContainer();
    };
    return DemoServiceContainer;
}(CoreServiceContainerAbs_1.CoreServiceContainerAbs));
exports.DemoServiceContainer = DemoServiceContainer;
