"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CliAppRunner = void 0;
var DemoAppRequest_1 = require("../../../../demo/library/appRequest/DemoAppRequest");
var CliAppRunner = /** @class */ (function () {
    function CliAppRunner(app) {
        this.app = app;
    }
    CliAppRunner.prototype.run = function () {
        this.app.run(new DemoAppRequest_1.DemoAppRequest());
    };
    return CliAppRunner;
}());
exports.CliAppRunner = CliAppRunner;
