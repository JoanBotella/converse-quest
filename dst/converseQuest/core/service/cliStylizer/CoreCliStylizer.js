"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreCliStylizer = void 0;
var CoreCliStylizer = /** @class */ (function () {
    function CoreCliStylizer() {
    }
    CoreCliStylizer.prototype.stylizeText = function (text) {
        text = text.replace(/<br \/>/g, '\n');
        text = text.replace(/<p>/g, '\n');
        text = text.replace(/<p class="talking">/g, '\n' + CoreCliStylizer.STYLE_ITALIC);
        text = text.replace(/<\/p>/g, '\n' + CoreCliStylizer.STYLE_RESET);
        text = text.replace(/<span class="character">/g, CoreCliStylizer.FG_GREEN);
        text = text.replace(/<span class="verb">/g, CoreCliStylizer.FG_YELLOW);
        text = text.replace(/<span class="item">/g, CoreCliStylizer.FG_BLUE);
        text = text.replace(/<span class="exit">/g, CoreCliStylizer.FG_RED);
        text = text.replace(/<\/span>/g, CoreCliStylizer.STYLE_RESET);
        return text;
    };
    CoreCliStylizer.prototype.stylizeTitle = function (text) {
        return '\n' + CoreCliStylizer.STYLE_BRIGHT + text + CoreCliStylizer.STYLE_RESET;
    };
    CoreCliStylizer.prototype.stylizeSlidePrompt = function (text) {
        return CoreCliStylizer.HI_FG_BLACK + text + CoreCliStylizer.STYLE_RESET;
    };
    CoreCliStylizer.prototype.stylizeQueryPrompt = function (text) {
        return CoreCliStylizer.HI_FG_BLACK + text + CoreCliStylizer.STYLE_RESET;
    };
    // https://gist.github.com/iamnewton/8754917
    // Regular colors
    CoreCliStylizer.FG_BLACK = '\x1b[0;30m';
    CoreCliStylizer.FG_RED = '\x1b[0;31m';
    CoreCliStylizer.FG_GREEN = '\x1b[0;32m';
    CoreCliStylizer.FG_YELLOW = '\x1b[0;33m';
    CoreCliStylizer.FG_BLUE = '\x1b[0;34m';
    CoreCliStylizer.FG_MAGENTA = '\x1b[0;35m';
    CoreCliStylizer.FG_CYAN = '\x1b[0;36m';
    CoreCliStylizer.FG_WHITE = '\x1b[0;37m';
    // Bold
    CoreCliStylizer.BOLD_BLACK = '\x1b[1;30m';
    CoreCliStylizer.BOLD_RED = '\x1b[1;31m';
    CoreCliStylizer.BOLD_GREEN = '\x1b[1;32m';
    CoreCliStylizer.BOLD_YELLOW = '\x1b[1;33m';
    CoreCliStylizer.BOLD_BLUE = '\x1b[1;34m';
    CoreCliStylizer.BOLD_MAGENTA = '\x1b[1;35m';
    CoreCliStylizer.BOLD_CYAN = '\x1b[1;36m';
    CoreCliStylizer.BOLD_WHITE = '\x1b[1;37m';
    // Underline
    CoreCliStylizer.UNDERLINE_BLACK = '\x1b[4;30m';
    CoreCliStylizer.UNDERLINE_RED = '\x1b[4;31m';
    CoreCliStylizer.UNDERLINE_GREEN = '\x1b[4;32m';
    CoreCliStylizer.UNDERLINE_YELLOW = '\x1b[4;33m';
    CoreCliStylizer.UNDERLINE_BLUE = '\x1b[4;34m';
    CoreCliStylizer.UNDERLINE_MAGENTA = '\x1b[4;35m';
    CoreCliStylizer.UNDERLINE_CYAN = '\x1b[4;36m';
    CoreCliStylizer.UNDERLINE_WHITE = '\x1b[4;37m';
    // Background
    CoreCliStylizer.BG_BLACK = '\x1b[40m';
    CoreCliStylizer.BG_RED = '\x1b[41m';
    CoreCliStylizer.BG_GREEN = '\x1b[42m';
    CoreCliStylizer.BG_YELLOW = '\x1b[43m';
    CoreCliStylizer.BG_BLUE = '\x1b[44m';
    CoreCliStylizer.BG_MAGENTA = '\x1b[45m';
    CoreCliStylizer.BG_CYAN = '\x1b[46m';
    CoreCliStylizer.BG_WHITE = '\x1b[47m';
    // High intensity
    CoreCliStylizer.HI_FG_BLACK = '\x1b[0;90m';
    CoreCliStylizer.HI_FG_RED = '\x1b[0;91m';
    CoreCliStylizer.HI_FG_GREEN = '\x1b[0;92m';
    CoreCliStylizer.HI_FG_YELLOW = '\x1b[0;93m';
    CoreCliStylizer.HI_FG_BLUE = '\x1b[0;94m';
    CoreCliStylizer.HI_FG_MAGENTA = '\x1b[0;95m';
    CoreCliStylizer.HI_FG_CYAN = '\x1b[0;96m';
    CoreCliStylizer.HI_FG_WHITE = '\x1b[0;97m';
    // Bold high intensity
    CoreCliStylizer.HI_BOLD_BLACK = '\x1b[1;90m';
    CoreCliStylizer.HI_BOLD_RED = '\x1b[1;91m';
    CoreCliStylizer.HI_BOLD_GREEN = '\x1b[1;92m';
    CoreCliStylizer.HI_BOLD_YELLOW = '\x1b[1;93m';
    CoreCliStylizer.HI_BOLD_BLUE = '\x1b[1;94m';
    CoreCliStylizer.HI_BOLD_MAGENTA = '\x1b[1;95m';
    CoreCliStylizer.HI_BOLD_CYAN = '\x1b[1;96m';
    CoreCliStylizer.HI_BOLD_WHITE = '\x1b[1;97m';
    // Bold high intensity background
    CoreCliStylizer.HI_BG_BLACK = '\x1b[0,100m';
    CoreCliStylizer.HI_BG_RED = '\x1b[0,101m';
    CoreCliStylizer.HI_BG_GREEN = '\x1b[0,102m';
    CoreCliStylizer.HI_BG_YELLOW = '\x1b[0,103m';
    CoreCliStylizer.HI_BG_BLUE = '\x1b[0,104m';
    CoreCliStylizer.HI_BG_MAGENTA = '\x1b[0,105m';
    CoreCliStylizer.HI_BG_CYAN = '\x1b[0,106m';
    CoreCliStylizer.HI_BG_WHITE = '\x1b[0,107m';
    // Styles
    CoreCliStylizer.STYLE_RESET = '\x1b[0m';
    CoreCliStylizer.STYLE_BRIGHT = '\x1b[1m';
    CoreCliStylizer.STYLE_DIM = '\x1b[2m';
    CoreCliStylizer.STYLE_ITALIC = '\x1b[3m';
    CoreCliStylizer.STYLE_UNDERSCORE = '\x1b[4m';
    CoreCliStylizer.STYLE_BLINK = '\x1b[5m';
    CoreCliStylizer.STYLE_REVERSE = '\x1b[7m';
    CoreCliStylizer.STYLE_HIDDEN = '\x1b[8m';
    CoreCliStylizer.STYLE_STRIKETHROUG = '\x1b[9m';
    return CoreCliStylizer;
}());
exports.CoreCliStylizer = CoreCliStylizer;
