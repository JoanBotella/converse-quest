"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreCliAppRunner = void 0;
var CoreCliAppRunner = /** @class */ (function () {
    function CoreCliAppRunner(cliAppRequestBuilder, app) {
        this.cliAppRequestBuilder = cliAppRequestBuilder;
        this.app = app;
    }
    CoreCliAppRunner.prototype.run = function () {
        var appRequest = this.cliAppRequestBuilder.build();
        this.app.run(appRequest);
    };
    return CoreCliAppRunner;
}());
exports.CoreCliAppRunner = CoreCliAppRunner;
