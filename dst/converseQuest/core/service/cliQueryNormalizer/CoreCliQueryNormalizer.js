"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreCliQueryNormalizer = void 0;
var CoreCliQueryNormalizer = /** @class */ (function () {
    function CoreCliQueryNormalizer() {
    }
    CoreCliQueryNormalizer.prototype.normalize = function (query) {
        query = query.toLowerCase();
        query = query.replace(/^look at/, 'look');
        query = query.replace(/^watch to/, 'watch');
        query = query.replace(/^pick up/, 'take');
        query = query.replace(/^go to/, 'go');
        query = query.replace(/^go through/, 'go');
        query = query.replace(/ a /g, ' ');
        query = query.replace(/ the /g, ' ');
        return query;
    };
    return CoreCliQueryNormalizer;
}());
exports.CoreCliQueryNormalizer = CoreCliQueryNormalizer;
