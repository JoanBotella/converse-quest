"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreAppAbs = void 0;
var CoreAppAbs = /** @class */ (function () {
    function CoreAppAbs(nodeRequestByAppRequestBuilder, appResponseByNodeResponseBuilder, nextNodeRequestBuilder, nodeRequestProcessor) {
        this.nodeRequestByAppRequestBuilder = nodeRequestByAppRequestBuilder;
        this.appResponseByNodeResponseBuilder = appResponseByNodeResponseBuilder;
        this.nextNodeRequestBuilder = nextNodeRequestBuilder;
        this.nodeRequestProcessor = nodeRequestProcessor;
    }
    CoreAppAbs.prototype.run = function (appRequest) {
        var nodeRequest = this.nodeRequestByAppRequestBuilder.build(appRequest);
        var slidesAccumulator = [];
        var nodeResponse;
        while (true) {
            nodeResponse = this.nodeRequestProcessor.process(nodeRequest);
            slidesAccumulator = slidesAccumulator.concat(nodeResponse.getSlides());
            if (!nodeResponse.getProcessAgain()) {
                break;
            }
            nodeRequest = this.nextNodeRequestBuilder.build(nodeResponse);
        }
        var appResponse = this.appResponseByNodeResponseBuilder.build(nodeResponse);
        appResponse.setSlides(slidesAccumulator);
        return appResponse;
    };
    return CoreAppAbs;
}());
exports.CoreAppAbs = CoreAppAbs;
