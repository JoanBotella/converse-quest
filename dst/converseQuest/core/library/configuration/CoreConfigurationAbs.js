"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreConfigurationAbs = void 0;
var CoreConfigurationAbs = /** @class */ (function () {
    function CoreConfigurationAbs(languageCode) {
        this.languageCode = languageCode;
    }
    CoreConfigurationAbs.prototype.getLanguageCode = function () {
        return this.languageCode;
    };
    CoreConfigurationAbs.prototype.setLanguageCode = function (languageCode) {
        this.languageCode = languageCode;
    };
    return CoreConfigurationAbs;
}());
exports.CoreConfigurationAbs = CoreConfigurationAbs;
