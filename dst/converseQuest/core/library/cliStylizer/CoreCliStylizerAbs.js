"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreCliStylizerAbs = void 0;
var CoreCliStylizerAbs = /** @class */ (function () {
    function CoreCliStylizerAbs() {
    }
    CoreCliStylizerAbs.prototype.stylizeText = function (text) {
        text = text.replace(/<br \/>/g, '\n');
        text = text.replace(/<p>/g, '\n');
        text = text.replace(/<p class="talking">/g, '\n' + CoreCliStylizerAbs.STYLE_ITALIC);
        text = text.replace(/<\/p>/g, '\n' + CoreCliStylizerAbs.STYLE_RESET);
        text = text.replace(/<span class="character">/g, CoreCliStylizerAbs.FG_GREEN);
        text = text.replace(/<span class="verb">/g, CoreCliStylizerAbs.FG_YELLOW);
        text = text.replace(/<span class="item">/g, CoreCliStylizerAbs.FG_BLUE);
        text = text.replace(/<span class="exit">/g, CoreCliStylizerAbs.FG_RED);
        text = text.replace(/<\/span>/g, CoreCliStylizerAbs.STYLE_RESET);
        return text;
    };
    CoreCliStylizerAbs.prototype.stylizeTitle = function (text) {
        return '\n' + CoreCliStylizerAbs.STYLE_BRIGHT + text + CoreCliStylizerAbs.STYLE_RESET;
    };
    CoreCliStylizerAbs.prototype.stylizeSlidePrompt = function (text) {
        return CoreCliStylizerAbs.HI_FG_BLACK + text + CoreCliStylizerAbs.STYLE_RESET;
    };
    CoreCliStylizerAbs.prototype.stylizeQueryPrompt = function (text) {
        return CoreCliStylizerAbs.HI_FG_BLACK + text + CoreCliStylizerAbs.STYLE_RESET;
    };
    // https://gist.github.com/iamnewton/8754917
    // Regular colors
    CoreCliStylizerAbs.FG_BLACK = '\x1b[0;30m';
    CoreCliStylizerAbs.FG_RED = '\x1b[0;31m';
    CoreCliStylizerAbs.FG_GREEN = '\x1b[0;32m';
    CoreCliStylizerAbs.FG_YELLOW = '\x1b[0;33m';
    CoreCliStylizerAbs.FG_BLUE = '\x1b[0;34m';
    CoreCliStylizerAbs.FG_MAGENTA = '\x1b[0;35m';
    CoreCliStylizerAbs.FG_CYAN = '\x1b[0;36m';
    CoreCliStylizerAbs.FG_WHITE = '\x1b[0;37m';
    // Bold
    CoreCliStylizerAbs.BOLD_BLACK = '\x1b[1;30m';
    CoreCliStylizerAbs.BOLD_RED = '\x1b[1;31m';
    CoreCliStylizerAbs.BOLD_GREEN = '\x1b[1;32m';
    CoreCliStylizerAbs.BOLD_YELLOW = '\x1b[1;33m';
    CoreCliStylizerAbs.BOLD_BLUE = '\x1b[1;34m';
    CoreCliStylizerAbs.BOLD_MAGENTA = '\x1b[1;35m';
    CoreCliStylizerAbs.BOLD_CYAN = '\x1b[1;36m';
    CoreCliStylizerAbs.BOLD_WHITE = '\x1b[1;37m';
    // Underline
    CoreCliStylizerAbs.UNDERLINE_BLACK = '\x1b[4;30m';
    CoreCliStylizerAbs.UNDERLINE_RED = '\x1b[4;31m';
    CoreCliStylizerAbs.UNDERLINE_GREEN = '\x1b[4;32m';
    CoreCliStylizerAbs.UNDERLINE_YELLOW = '\x1b[4;33m';
    CoreCliStylizerAbs.UNDERLINE_BLUE = '\x1b[4;34m';
    CoreCliStylizerAbs.UNDERLINE_MAGENTA = '\x1b[4;35m';
    CoreCliStylizerAbs.UNDERLINE_CYAN = '\x1b[4;36m';
    CoreCliStylizerAbs.UNDERLINE_WHITE = '\x1b[4;37m';
    // Background
    CoreCliStylizerAbs.BG_BLACK = '\x1b[40m';
    CoreCliStylizerAbs.BG_RED = '\x1b[41m';
    CoreCliStylizerAbs.BG_GREEN = '\x1b[42m';
    CoreCliStylizerAbs.BG_YELLOW = '\x1b[43m';
    CoreCliStylizerAbs.BG_BLUE = '\x1b[44m';
    CoreCliStylizerAbs.BG_MAGENTA = '\x1b[45m';
    CoreCliStylizerAbs.BG_CYAN = '\x1b[46m';
    CoreCliStylizerAbs.BG_WHITE = '\x1b[47m';
    // High intensity
    CoreCliStylizerAbs.HI_FG_BLACK = '\x1b[0;90m';
    CoreCliStylizerAbs.HI_FG_RED = '\x1b[0;91m';
    CoreCliStylizerAbs.HI_FG_GREEN = '\x1b[0;92m';
    CoreCliStylizerAbs.HI_FG_YELLOW = '\x1b[0;93m';
    CoreCliStylizerAbs.HI_FG_BLUE = '\x1b[0;94m';
    CoreCliStylizerAbs.HI_FG_MAGENTA = '\x1b[0;95m';
    CoreCliStylizerAbs.HI_FG_CYAN = '\x1b[0;96m';
    CoreCliStylizerAbs.HI_FG_WHITE = '\x1b[0;97m';
    // Bold high intensity
    CoreCliStylizerAbs.HI_BOLD_BLACK = '\x1b[1;90m';
    CoreCliStylizerAbs.HI_BOLD_RED = '\x1b[1;91m';
    CoreCliStylizerAbs.HI_BOLD_GREEN = '\x1b[1;92m';
    CoreCliStylizerAbs.HI_BOLD_YELLOW = '\x1b[1;93m';
    CoreCliStylizerAbs.HI_BOLD_BLUE = '\x1b[1;94m';
    CoreCliStylizerAbs.HI_BOLD_MAGENTA = '\x1b[1;95m';
    CoreCliStylizerAbs.HI_BOLD_CYAN = '\x1b[1;96m';
    CoreCliStylizerAbs.HI_BOLD_WHITE = '\x1b[1;97m';
    // Bold high intensity background
    CoreCliStylizerAbs.HI_BG_BLACK = '\x1b[0,100m';
    CoreCliStylizerAbs.HI_BG_RED = '\x1b[0,101m';
    CoreCliStylizerAbs.HI_BG_GREEN = '\x1b[0,102m';
    CoreCliStylizerAbs.HI_BG_YELLOW = '\x1b[0,103m';
    CoreCliStylizerAbs.HI_BG_BLUE = '\x1b[0,104m';
    CoreCliStylizerAbs.HI_BG_MAGENTA = '\x1b[0,105m';
    CoreCliStylizerAbs.HI_BG_CYAN = '\x1b[0,106m';
    CoreCliStylizerAbs.HI_BG_WHITE = '\x1b[0,107m';
    // Styles
    CoreCliStylizerAbs.STYLE_RESET = '\x1b[0m';
    CoreCliStylizerAbs.STYLE_BRIGHT = '\x1b[1m';
    CoreCliStylizerAbs.STYLE_DIM = '\x1b[2m';
    CoreCliStylizerAbs.STYLE_ITALIC = '\x1b[3m';
    CoreCliStylizerAbs.STYLE_UNDERSCORE = '\x1b[4m';
    CoreCliStylizerAbs.STYLE_BLINK = '\x1b[5m';
    CoreCliStylizerAbs.STYLE_REVERSE = '\x1b[7m';
    CoreCliStylizerAbs.STYLE_HIDDEN = '\x1b[8m';
    CoreCliStylizerAbs.STYLE_STRIKETHROUG = '\x1b[9m';
    return CoreCliStylizerAbs;
}());
exports.CoreCliStylizerAbs = CoreCliStylizerAbs;
