"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreServiceContainerAbs = void 0;
var CoreServiceContainerAbs = /** @class */ (function () {
    function CoreServiceContainerAbs() {
    }
    CoreServiceContainerAbs.prototype.getCliAppRunner = function () {
        if (this.cliAppRunner === undefined) {
            this.cliAppRunner = this.buildCliAppRunner();
        }
        return this.cliAppRunner;
    };
    CoreServiceContainerAbs.prototype.getCliAppRequestBuilder = function () {
        if (this.cliAppRequestBuilder === undefined) {
            this.cliAppRequestBuilder = this.buildCliAppRequestBuilder();
        }
        return this.cliAppRequestBuilder;
    };
    CoreServiceContainerAbs.prototype.getApp = function () {
        if (this.app === undefined) {
            this.app = this.buildApp();
        }
        return this.app;
    };
    CoreServiceContainerAbs.prototype.getCliAppResponseDispatcher = function () {
        if (this.cliAppResponseDispatcher === undefined) {
            this.cliAppResponseDispatcher = this.buildCliAppResponseDispatcher();
        }
        return this.cliAppResponseDispatcher;
    };
    CoreServiceContainerAbs.prototype.getRouter = function () {
        if (this.router === undefined) {
            this.router = this.buildRouter();
        }
        return this.router;
    };
    CoreServiceContainerAbs.prototype.getCliStylizer = function () {
        if (this.cliStylizer === undefined) {
            this.cliStylizer = this.buildCliStylizer();
        }
        return this.cliStylizer;
    };
    CoreServiceContainerAbs.prototype.getCliQueryNormalizerContainer = function () {
        if (this.cliQueryNormalizerContainer === undefined) {
            this.cliQueryNormalizerContainer = this.buildCliQueryNormalizerContainer();
        }
        return this.cliQueryNormalizerContainer;
    };
    CoreServiceContainerAbs.prototype.getCliPrompter = function () {
        if (this.cliPrompter === undefined) {
            this.cliPrompter = this.buildCliPrompter();
        }
        return this.cliPrompter;
    };
    CoreServiceContainerAbs.prototype.getCliQueryBuilder = function () {
        if (this.cliQueryBuilder === undefined) {
            this.cliQueryBuilder = this.buildCliQueryBuilder();
        }
        return this.cliQueryBuilder;
    };
    CoreServiceContainerAbs.prototype.getNodeRequestByAppRequestBuilder = function () {
        if (this.nodeRequestByAppRequestBuilder === undefined) {
            this.nodeRequestByAppRequestBuilder = this.buildNodeRequestByAppRequestBuilder();
        }
        return this.nodeRequestByAppRequestBuilder;
    };
    CoreServiceContainerAbs.prototype.getSystemNodeResponseBuilder = function () {
        if (this.systemNodeResponseBuilder === undefined) {
            this.systemNodeResponseBuilder = this.buildSystemNodeResponseBuilder();
        }
        return this.systemNodeResponseBuilder;
    };
    CoreServiceContainerAbs.prototype.getAppResponseByNodeResponseBuilder = function () {
        if (this.appResponseByNodeResponseBuilder === undefined) {
            this.appResponseByNodeResponseBuilder = this.buildAppResponseByNodeResponseBuilder();
        }
        return this.appResponseByNodeResponseBuilder;
    };
    CoreServiceContainerAbs.prototype.getNextNodeRequestBuilder = function () {
        if (this.nextNodeRequestBuilder === undefined) {
            this.nextNodeRequestBuilder = this.buildNextNodeRequestBuilder();
        }
        return this.nextNodeRequestBuilder;
    };
    CoreServiceContainerAbs.prototype.getNodeRequestProcessor = function () {
        if (this.nodeRequestProcessor === undefined) {
            this.nodeRequestProcessor = this.buildNodeRequestProcessor();
        }
        return this.nodeRequestProcessor;
    };
    CoreServiceContainerAbs.prototype.getStartingStatusBuilder = function () {
        if (this.startingStatusBuilder === undefined) {
            this.startingStatusBuilder = this.buildStartingStatusBuilder();
        }
        return this.startingStatusBuilder;
    };
    CoreServiceContainerAbs.prototype.getNodeBuilder = function () {
        if (this.nodeBuilder === undefined) {
            this.nodeBuilder = this.buildNodeBuilder();
        }
        return this.nodeBuilder;
    };
    CoreServiceContainerAbs.prototype.getNodeBrowser = function () {
        if (this.nodeBrowser === undefined) {
            this.nodeBrowser = this.buildNodeBrowser();
        }
        return this.nodeBrowser;
    };
    CoreServiceContainerAbs.prototype.getTranslationContainer = function () {
        if (this.translationContainer === undefined) {
            this.translationContainer = this.buildTranslationContainer();
        }
        return this.translationContainer;
    };
    return CoreServiceContainerAbs;
}());
exports.CoreServiceContainerAbs = CoreServiceContainerAbs;
