"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreNodeBrowserAbs = void 0;
var CoreNodeBrowserAbs = /** @class */ (function () {
    function CoreNodeBrowserAbs() {
    }
    CoreNodeBrowserAbs.prototype.change = function (nodeResponse, nodeId) {
        var status = nodeResponse.getStatus(), breadcrumbs = status.getBreadcrumbs();
        breadcrumbs[breadcrumbs.length - 1] = nodeId;
        status.setBreadcrumbs(breadcrumbs);
        nodeResponse.setStatus(status);
        nodeResponse.setProcessAgain(true);
        return nodeResponse;
    };
    CoreNodeBrowserAbs.prototype.push = function (nodeResponse, nodeId) {
        var status = nodeResponse.getStatus(), breadcrumbs = status.getBreadcrumbs();
        breadcrumbs.push(nodeId);
        status.setBreadcrumbs(breadcrumbs);
        nodeResponse.setStatus(status);
        nodeResponse.setProcessAgain(true);
        return nodeResponse;
    };
    CoreNodeBrowserAbs.prototype.pop = function (nodeResponse) {
        var status = nodeResponse.getStatus(), breadcrumbs = status.getBreadcrumbs();
        if (breadcrumbs.length <= 1) {
            throw new Error('The breadcrumbs cannot be popped to emptyness.');
        }
        breadcrumbs.pop();
        status.setBreadcrumbs(breadcrumbs);
        nodeResponse.setStatus(status);
        nodeResponse.setProcessAgain(true);
        return nodeResponse;
    };
    return CoreNodeBrowserAbs;
}());
exports.CoreNodeBrowserAbs = CoreNodeBrowserAbs;
