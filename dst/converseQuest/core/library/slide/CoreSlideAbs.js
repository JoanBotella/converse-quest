"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreSlideAbs = void 0;
var CoreSlideAbs = /** @class */ (function () {
    function CoreSlideAbs() {
        this.title = undefined;
        this.text = undefined;
    }
    CoreSlideAbs.prototype.hasTitle = function () {
        return this.title !== undefined;
    };
    CoreSlideAbs.prototype.getTitleAfterHas = function () {
        return this.title;
    };
    CoreSlideAbs.prototype.setTitle = function (title) {
        this.title = title;
    };
    CoreSlideAbs.prototype.unsetTitle = function () {
        this.title = undefined;
    };
    CoreSlideAbs.prototype.hasText = function () {
        return this.text !== undefined;
    };
    CoreSlideAbs.prototype.getTextAfterHas = function () {
        return this.text;
    };
    CoreSlideAbs.prototype.setText = function (text) {
        this.text = text;
    };
    CoreSlideAbs.prototype.unsetText = function () {
        this.text = undefined;
    };
    return CoreSlideAbs;
}());
exports.CoreSlideAbs = CoreSlideAbs;
