"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreAppRequestAbs = void 0;
var CoreAppRequestAbs = /** @class */ (function () {
    function CoreAppRequestAbs(status) {
        this.query = undefined;
        this.status = status;
    }
    CoreAppRequestAbs.prototype.hasQuery = function () {
        return this.query !== undefined;
    };
    CoreAppRequestAbs.prototype.getQueryAfterHas = function () {
        return this.query;
    };
    CoreAppRequestAbs.prototype.setQuery = function (query) {
        this.query = query;
    };
    CoreAppRequestAbs.prototype.unsetQuery = function () {
        this.query = undefined;
    };
    CoreAppRequestAbs.prototype.getStatus = function () {
        return this.status;
    };
    CoreAppRequestAbs.prototype.setStatus = function (status) {
        this.status = status;
    };
    return CoreAppRequestAbs;
}());
exports.CoreAppRequestAbs = CoreAppRequestAbs;
