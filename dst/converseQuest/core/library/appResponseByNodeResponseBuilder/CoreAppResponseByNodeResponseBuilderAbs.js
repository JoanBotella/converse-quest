"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreAppResponseByNodeResponseBuilderAbs = void 0;
var CoreAppResponseByNodeResponseBuilderAbs = /** @class */ (function () {
    function CoreAppResponseByNodeResponseBuilderAbs() {
    }
    CoreAppResponseByNodeResponseBuilderAbs.prototype.build = function (nodeResponse) {
        var appResponse = this.instantiate(nodeResponse.getStatus());
        appResponse.setSlides(nodeResponse.getSlides());
        appResponse.setEnd(nodeResponse.getEnd());
        return appResponse;
    };
    return CoreAppResponseByNodeResponseBuilderAbs;
}());
exports.CoreAppResponseByNodeResponseBuilderAbs = CoreAppResponseByNodeResponseBuilderAbs;
