"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreAppResponseAbs = void 0;
var CoreAppResponseAbs = /** @class */ (function () {
    function CoreAppResponseAbs(status) {
        this.status = status;
        this.slides = [];
        this.end = false;
    }
    CoreAppResponseAbs.prototype.getStatus = function () {
        return this.status;
    };
    CoreAppResponseAbs.prototype.setStatus = function (status) {
        this.status = status;
    };
    CoreAppResponseAbs.prototype.appendSlide = function (slide) {
        this.slides.push(slide);
    };
    CoreAppResponseAbs.prototype.getSlides = function () {
        return this.slides;
    };
    CoreAppResponseAbs.prototype.setSlides = function (slides) {
        this.slides = slides;
    };
    CoreAppResponseAbs.prototype.setEnd = function (end) {
        this.end = end;
    };
    CoreAppResponseAbs.prototype.getEnd = function () {
        return this.end;
    };
    return CoreAppResponseAbs;
}());
exports.CoreAppResponseAbs = CoreAppResponseAbs;
