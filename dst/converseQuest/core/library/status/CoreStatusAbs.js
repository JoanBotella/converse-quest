"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreStatusAbs = void 0;
var CoreStatusAbs = /** @class */ (function () {
    function CoreStatusAbs(nodeId, configuration, variables) {
        this.breadcrumbs = [nodeId];
        this.configuration = configuration;
        this.variables = variables;
    }
    CoreStatusAbs.prototype.getBreadcrumbs = function () {
        return this.breadcrumbs;
    };
    CoreStatusAbs.prototype.setBreadcrumbs = function (breadcrumbs) {
        this.breadcrumbs = breadcrumbs;
    };
    CoreStatusAbs.prototype.getConfiguration = function () {
        return this.configuration;
    };
    CoreStatusAbs.prototype.setConfiguration = function (configuration) {
        this.configuration = configuration;
    };
    CoreStatusAbs.prototype.getVariables = function () {
        return this.variables;
    };
    CoreStatusAbs.prototype.setVariables = function (variables) {
        this.variables = variables;
    };
    return CoreStatusAbs;
}());
exports.CoreStatusAbs = CoreStatusAbs;
