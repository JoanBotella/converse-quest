"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreNodeRequestByAppRequestBuilderAbs = void 0;
var CoreNodeRequestByAppRequestBuilderAbs = /** @class */ (function () {
    function CoreNodeRequestByAppRequestBuilderAbs() {
    }
    CoreNodeRequestByAppRequestBuilderAbs.prototype.build = function (appRequest) {
        var nodeRequest = this.instantiateNodeRequest(appRequest.getStatus());
        if (appRequest.hasQuery()) {
            nodeRequest.setQuery(appRequest.getQueryAfterHas());
        }
        return nodeRequest;
    };
    return CoreNodeRequestByAppRequestBuilderAbs;
}());
exports.CoreNodeRequestByAppRequestBuilderAbs = CoreNodeRequestByAppRequestBuilderAbs;
