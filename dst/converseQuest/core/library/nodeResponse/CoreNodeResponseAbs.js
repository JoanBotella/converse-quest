"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreNodeResponseAbs = void 0;
var CoreNodeResponseAbs = /** @class */ (function () {
    function CoreNodeResponseAbs(status) {
        this.status = status;
        this.slides = [];
        this.end = false;
        this.processAgain = false;
    }
    CoreNodeResponseAbs.prototype.getStatus = function () {
        return this.status;
    };
    CoreNodeResponseAbs.prototype.setStatus = function (status) {
        this.status = status;
    };
    CoreNodeResponseAbs.prototype.appendSlide = function (slide) {
        this.slides.push(slide);
    };
    CoreNodeResponseAbs.prototype.getSlides = function () {
        return this.slides;
    };
    CoreNodeResponseAbs.prototype.setSlides = function (slides) {
        this.slides = slides;
    };
    CoreNodeResponseAbs.prototype.setEnd = function (end) {
        this.end = end;
    };
    CoreNodeResponseAbs.prototype.getEnd = function () {
        return this.end;
    };
    CoreNodeResponseAbs.prototype.setProcessAgain = function (processAgain) {
        this.processAgain = processAgain;
    };
    CoreNodeResponseAbs.prototype.getProcessAgain = function () {
        return this.processAgain;
    };
    return CoreNodeResponseAbs;
}());
exports.CoreNodeResponseAbs = CoreNodeResponseAbs;
