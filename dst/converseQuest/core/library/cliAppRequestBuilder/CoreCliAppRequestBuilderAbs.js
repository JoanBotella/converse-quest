"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreCliAppRequestBuilderAbs = void 0;
var CoreCliAppRequestBuilderAbs = /** @class */ (function () {
    function CoreCliAppRequestBuilderAbs(queryBuilder) {
        this.queryBuilder = queryBuilder;
        this.appRequest = undefined;
    }
    CoreCliAppRequestBuilderAbs.prototype.tryToBuildWithoutQuery = function (status) {
        this.appRequest = this.instantiate(status);
    };
    CoreCliAppRequestBuilderAbs.prototype.tryToBuildWithQuery = function (status) {
        this.tryToBuildWithoutQuery(status);
        this.queryBuilder.tryToBuild(status.getConfiguration().getLanguageCode());
        if (this.queryBuilder.hasQuery()) {
            this.appRequest.setQuery(this.queryBuilder.getQueryAfterHas());
        }
    };
    CoreCliAppRequestBuilderAbs.prototype.hasAppRequest = function () {
        return this.appRequest !== undefined;
    };
    CoreCliAppRequestBuilderAbs.prototype.getAppRequestAfterHas = function () {
        return this.appRequest;
    };
    return CoreCliAppRequestBuilderAbs;
}());
exports.CoreCliAppRequestBuilderAbs = CoreCliAppRequestBuilderAbs;
