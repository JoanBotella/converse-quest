"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreCliAppRunnerAbs = void 0;
var CoreCliAppRunnerAbs = /** @class */ (function () {
    function CoreCliAppRunnerAbs(startingStatusBuilder, cliAppRequestBuilder, app, cliAppResponseDispatcher) {
        this.startingStatusBuilder = startingStatusBuilder;
        this.cliAppRequestBuilder = cliAppRequestBuilder;
        this.app = app;
        this.cliAppResponseDispatcher = cliAppResponseDispatcher;
        this.firstStep = true;
        this.end = false;
    }
    CoreCliAppRunnerAbs.prototype.run = function () {
        this.firstStep = true;
        var status = this.startingStatusBuilder.build();
        while (!this.end) {
            status = this.step(status);
        }
    };
    CoreCliAppRunnerAbs.prototype.step = function (status) {
        var appRequest = this.buildAppRequest(status);
        var appResponse = this.app.run(appRequest);
        this.cliAppResponseDispatcher.dispatch(appResponse);
        this.end = appResponse.getEnd();
        return appResponse.getStatus();
    };
    CoreCliAppRunnerAbs.prototype.buildAppRequest = function (status) {
        if (this.firstStep) {
            this.cliAppRequestBuilder.tryToBuildWithoutQuery(status);
            this.firstStep = false;
        }
        else {
            this.cliAppRequestBuilder.tryToBuildWithQuery(status);
        }
        if (this.cliAppRequestBuilder.hasAppRequest()) {
            return this.cliAppRequestBuilder.getAppRequestAfterHas();
        }
        throw new Error('AppRequest could not be built!');
    };
    return CoreCliAppRunnerAbs;
}());
exports.CoreCliAppRunnerAbs = CoreCliAppRunnerAbs;
