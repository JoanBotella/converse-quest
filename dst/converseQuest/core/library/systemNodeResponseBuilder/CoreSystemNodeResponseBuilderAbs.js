"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreSystemNodeResponseBuilderAbs = void 0;
var CoreSystemNodeResponseBuilderAbs = /** @class */ (function () {
    function CoreSystemNodeResponseBuilderAbs(nodeBrowser, translationContainer) {
        this.nodeBrowser = nodeBrowser;
        this.translationContainer = translationContainer;
    }
    CoreSystemNodeResponseBuilderAbs.prototype.build = function (nodeRequest) {
        this.nodeRequest = nodeRequest;
        return this.buildNodeResponse();
    };
    CoreSystemNodeResponseBuilderAbs.prototype.hasNodeRequestQuery = function () {
        return this.getNodeRequest().hasQuery();
    };
    CoreSystemNodeResponseBuilderAbs.prototype.getNodeRequestQueryAfterHas = function () {
        return this.getNodeRequest().getQueryAfterHas();
    };
    CoreSystemNodeResponseBuilderAbs.prototype.getNodeRequest = function () {
        return this.nodeRequest;
    };
    CoreSystemNodeResponseBuilderAbs.prototype.getNodeBrowser = function () {
        return this.nodeBrowser;
    };
    CoreSystemNodeResponseBuilderAbs.prototype.getTranslation = function () {
        return this.translationContainer.get(this
            .getNodeRequest()
            .getStatus()
            .getConfiguration()
            .getLanguageCode());
    };
    CoreSystemNodeResponseBuilderAbs.prototype.queryMatches = function (list) {
        var items = list.split(',');
        return items.indexOf(this.getNodeRequestQueryAfterHas()) != -1;
    };
    return CoreSystemNodeResponseBuilderAbs;
}());
exports.CoreSystemNodeResponseBuilderAbs = CoreSystemNodeResponseBuilderAbs;
