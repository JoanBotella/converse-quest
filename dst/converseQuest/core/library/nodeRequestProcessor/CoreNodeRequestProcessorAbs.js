"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreNodeRequestProcessorAbs = void 0;
var CoreNodeRequestProcessorAbs = /** @class */ (function () {
    function CoreNodeRequestProcessorAbs(systemNodeResponseBuilder, router) {
        this.systemNodeResponseBuilder = systemNodeResponseBuilder;
        this.router = router;
    }
    CoreNodeRequestProcessorAbs.prototype.process = function (nodeRequest) {
        var nodeId = this.getNodeIdFromBreadcrumbs(nodeRequest), node = this.router.route(nodeId);
        return this.buildNodeResponse(node, nodeRequest);
    };
    CoreNodeRequestProcessorAbs.prototype.getNodeIdFromBreadcrumbs = function (nodeRequest) {
        var breadcrumbs = nodeRequest.getStatus().getBreadcrumbs();
        if (breadcrumbs.length) {
            return breadcrumbs[breadcrumbs.length - 1];
        }
        throw new Error('Any node could be processed because breadcrumbs are empty.');
    };
    CoreNodeRequestProcessorAbs.prototype.buildNodeResponse = function (node, nodeRequest) {
        node.run(nodeRequest);
        if (node.hasNodeResponse()) {
            return node.getNodeResponseAfterHas();
        }
        return this.systemNodeResponseBuilder.build(nodeRequest);
    };
    return CoreNodeRequestProcessorAbs;
}());
exports.CoreNodeRequestProcessorAbs = CoreNodeRequestProcessorAbs;
