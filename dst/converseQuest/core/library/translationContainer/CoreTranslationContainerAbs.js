"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreTranslationContainerAbs = void 0;
var CoreTranslationContainerAbs = /** @class */ (function () {
    function CoreTranslationContainerAbs() {
        this.translations = new Map();
    }
    CoreTranslationContainerAbs.prototype.get = function (languageCode) {
        if (!this.translations.has(languageCode)) {
            this.translations.set(languageCode, this.buildTranslation(languageCode));
        }
        return this.translations.get(languageCode);
    };
    return CoreTranslationContainerAbs;
}());
exports.CoreTranslationContainerAbs = CoreTranslationContainerAbs;
