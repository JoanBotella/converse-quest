"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreCliQueryBuilderAbs = void 0;
var CoreCliQueryBuilderAbs = /** @class */ (function () {
    function CoreCliQueryBuilderAbs(cliPrompter, cliQueryNormalizerContainer, translationContainer) {
        this.cliPrompter = cliPrompter;
        this.cliQueryNormalizerContainer = cliQueryNormalizerContainer;
        this.translationContainer = translationContainer;
        this.query = undefined;
    }
    CoreCliQueryBuilderAbs.prototype.tryToBuild = function (languageCode) {
        this.query = undefined;
        var answer = this.prompt(languageCode);
        if (answer != '') {
            var cliQueryNormalizer = this.cliQueryNormalizerContainer.get(languageCode);
            this.query = cliQueryNormalizer.normalize(answer);
        }
    };
    CoreCliQueryBuilderAbs.prototype.prompt = function (languageCode) {
        var translation = this.translationContainer.get(languageCode);
        return this.cliPrompter.prompt(translation.cliQueryBuilder_prompt());
    };
    CoreCliQueryBuilderAbs.prototype.hasQuery = function () {
        return this.query !== undefined;
    };
    CoreCliQueryBuilderAbs.prototype.getQueryAfterHas = function () {
        return this.query;
    };
    return CoreCliQueryBuilderAbs;
}());
exports.CoreCliQueryBuilderAbs = CoreCliQueryBuilderAbs;
