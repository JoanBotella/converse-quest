"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreCliQueryNormalizerContainerAbs = void 0;
var CoreCliQueryNormalizerContainerAbs = /** @class */ (function () {
    function CoreCliQueryNormalizerContainerAbs() {
        this.cliQueryNormalizers = new Map();
    }
    CoreCliQueryNormalizerContainerAbs.prototype.get = function (languageCode) {
        if (!this.cliQueryNormalizers.has(languageCode)) {
            this.cliQueryNormalizers.set(languageCode, this.buildCliQueryNormalizer(languageCode));
        }
        return this.cliQueryNormalizers.get(languageCode);
    };
    return CoreCliQueryNormalizerContainerAbs;
}());
exports.CoreCliQueryNormalizerContainerAbs = CoreCliQueryNormalizerContainerAbs;
