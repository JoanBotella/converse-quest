"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreCliAppResponseDispatcherAbs = void 0;
var CoreCliAppResponseDispatcherAbs = /** @class */ (function () {
    function CoreCliAppResponseDispatcherAbs(cliStylizer, cliPrompter, translationContainer) {
        this.cliStylizer = cliStylizer;
        this.cliPrompter = cliPrompter;
        this.translationContainer = translationContainer;
    }
    CoreCliAppResponseDispatcherAbs.prototype.dispatch = function (appResponse) {
        var slides = appResponse.getSlides(), prompt = this.buildPrompt(appResponse);
        for (var index = 0; index < slides.length; index++) {
            if (index > 0) {
                this.cliPrompter.prompt(this.cliStylizer.stylizeSlidePrompt(prompt));
            }
            this.dispatchSlide(slides[index]);
        }
    };
    CoreCliAppResponseDispatcherAbs.prototype.buildPrompt = function (appResponse) {
        var languageCode = appResponse.getStatus().getConfiguration().getLanguageCode(), translation = this.translationContainer.get(languageCode);
        return translation.cliAppResponseDispatcher_prompt();
    };
    CoreCliAppResponseDispatcherAbs.prototype.dispatchSlide = function (slide) {
        if (slide.hasTitle()) {
            console.log(this.cliStylizer.stylizeTitle(slide.getTitleAfterHas()));
        }
        if (slide.hasText()) {
            console.log(this.cliStylizer.stylizeText(slide.getTextAfterHas()));
        }
    };
    return CoreCliAppResponseDispatcherAbs;
}());
exports.CoreCliAppResponseDispatcherAbs = CoreCliAppResponseDispatcherAbs;
