"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreNodeAbs = void 0;
var CoreNodeAbs = /** @class */ (function () {
    function CoreNodeAbs(nodeBrowser, translationContainer) {
        this.nodeBrowser = nodeBrowser;
        this.translationContainer = translationContainer;
        this.nodeRequest = undefined;
        this.nodeResponse = undefined;
    }
    CoreNodeAbs.prototype.run = function (nodeRequest) {
        this.unsetNodeResponse();
        this.nodeRequest = nodeRequest;
        this.tryToSetupNodeResponse();
    };
    CoreNodeAbs.prototype.hasNodeResponse = function () {
        return this.nodeResponse !== undefined;
    };
    CoreNodeAbs.prototype.getNodeResponseAfterHas = function () {
        return this.nodeResponse;
    };
    CoreNodeAbs.prototype.setNodeResponse = function (nodeResponse) {
        this.nodeResponse = nodeResponse;
    };
    CoreNodeAbs.prototype.unsetNodeResponse = function () {
        this.nodeResponse = undefined;
    };
    CoreNodeAbs.prototype.getNodeRequest = function () {
        return this.nodeRequest;
    };
    CoreNodeAbs.prototype.hasNodeRequestQuery = function () {
        return this.getNodeRequest().hasQuery();
    };
    CoreNodeAbs.prototype.getNodeRequestQueryAfterHas = function () {
        return this.getNodeRequest().getQueryAfterHas();
    };
    CoreNodeAbs.prototype.getNodeBrowser = function () {
        return this.nodeBrowser;
    };
    CoreNodeAbs.prototype.getTranslation = function () {
        return this.translationContainer.get(this
            .getNodeRequest()
            .getStatus()
            .getConfiguration()
            .getLanguageCode());
    };
    CoreNodeAbs.prototype.queryMatches = function (list) {
        var items = list.split(',');
        return items.indexOf(this.getNodeRequestQueryAfterHas()) != -1;
    };
    return CoreNodeAbs;
}());
exports.CoreNodeAbs = CoreNodeAbs;
