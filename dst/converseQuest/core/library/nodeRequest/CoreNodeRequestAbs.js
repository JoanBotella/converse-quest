"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreNodeRequestAbs = void 0;
var CoreNodeRequestAbs = /** @class */ (function () {
    function CoreNodeRequestAbs(status) {
        this.query = undefined;
        this.status = status;
    }
    CoreNodeRequestAbs.prototype.hasQuery = function () {
        return this.query !== undefined;
    };
    CoreNodeRequestAbs.prototype.getQueryAfterHas = function () {
        return this.query;
    };
    CoreNodeRequestAbs.prototype.setQuery = function (query) {
        this.query = query;
    };
    CoreNodeRequestAbs.prototype.unsetQuery = function () {
        this.query = undefined;
    };
    CoreNodeRequestAbs.prototype.getStatus = function () {
        return this.status;
    };
    CoreNodeRequestAbs.prototype.setStatus = function (status) {
        this.status = status;
    };
    return CoreNodeRequestAbs;
}());
exports.CoreNodeRequestAbs = CoreNodeRequestAbs;
