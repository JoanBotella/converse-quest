"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreTranslationAbs = void 0;
var CoreTranslationAbs = /** @class */ (function () {
    function CoreTranslationAbs() {
    }
    CoreTranslationAbs.prototype.cliQueryBuilder_prompt = function () { return '> '; };
    CoreTranslationAbs.prototype.cliAppResponseDispatcher_prompt = function () { return '...'; };
    return CoreTranslationAbs;
}());
exports.CoreTranslationAbs = CoreTranslationAbs;
