"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreCliPrompterAbs = void 0;
var CoreCliPrompterAbs = /** @class */ (function () {
    function CoreCliPrompterAbs(stylizer) {
        this.reader = require('readline-sync');
        this.stylizer = stylizer;
    }
    CoreCliPrompterAbs.prototype.prompt = function (text) {
        return this.reader.question(this.stylizer.stylizeQueryPrompt(text));
    };
    return CoreCliPrompterAbs;
}());
exports.CoreCliPrompterAbs = CoreCliPrompterAbs;
