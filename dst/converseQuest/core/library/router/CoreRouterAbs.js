"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreRouterAbs = void 0;
var CoreRouterAbs = /** @class */ (function () {
    function CoreRouterAbs(nodeBuilder) {
        this.nodeBuilder = nodeBuilder;
    }
    CoreRouterAbs.prototype.getNodeBuilder = function () {
        return this.nodeBuilder;
    };
    return CoreRouterAbs;
}());
exports.CoreRouterAbs = CoreRouterAbs;
