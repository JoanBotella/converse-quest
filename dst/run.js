"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DemoServiceContainer_1 = require("./demo/library/serviceContainer/DemoServiceContainer");
var serviceContainer = new DemoServiceContainer_1.DemoServiceContainer();
var cliAppRunner = serviceContainer.getCliAppRunner();
cliAppRunner.run();
