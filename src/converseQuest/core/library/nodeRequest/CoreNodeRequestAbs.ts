import { CoreNodeRequestItf } from "./CoreNodeRequestItf";
import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";

export
	abstract class CoreNodeRequestAbs<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>
	>
	implements CoreNodeRequestItf<
		TConfiguration,
		TVariables,
		TStatus
	>
{

	private query:string|undefined;
	private status:TStatus;

	constructor(
		status:TStatus
	)
	{
		this.query = undefined;
		this.status = status;
	}

	public hasQuery():boolean
	{
		return this.query !== undefined;
	}

	public getQueryAfterHas():string
	{
		return this.query as string;
	}

	public setQuery(query:string):void
	{
		this.query = query;
	}

	public unsetQuery():void
	{
		this.query = undefined;
	}

	public getStatus():TStatus
	{
		return this.status;
	}

	public setStatus(status:TStatus):void
	{
		this.status = status;
	}

}