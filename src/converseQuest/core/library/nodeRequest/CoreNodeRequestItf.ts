import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";

export
	interface CoreNodeRequestItf<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>
	>
{

	hasQuery: () => boolean;

	getQueryAfterHas: () => string;

	setQuery: (query:string) => void;

	unsetQuery: () => void;

	getStatus: () => TStatus;

	setStatus: (status:TStatus) => void;

}