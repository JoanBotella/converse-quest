import { CoreAppItf } from "./CoreAppItf";
import { CoreAppRequestItf } from "../appRequest/CoreAppRequestItf";
import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreAppResponseItf } from "../appResponse/CoreAppResponseItf";
import { CoreSlideItf } from "../slide/CoreSlideItf";
import { CoreNodeRequestItf } from "../nodeRequest/CoreNodeRequestItf";
import { CoreNodeResponseItf } from "../nodeResponse/CoreNodeResponseItf";
import { CoreNodeRequestByAppRequestBuilderItf } from "../nodeRequestByAppRequestBuilder/CoreNodeRequestByAppRequestBuilderItf";
import { CoreAppResponseByNodeResponseBuilderItf } from "../appResponseByNodeResponseBuilder/CoreAppResponseByNodeResponseBuilderItf";
import { CoreNextNodeRequestBuilderItf } from "../nextNodeRequestBuilder/CoreNextNodeRequestBuilderItf";
import { CoreNodeRequestProcessorItf } from "../nodeRequestProcessor/CoreNodeRequestProcessorItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";

export
	abstract class CoreAppAbs<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>,
		TAppRequest extends CoreAppRequestItf<
			TConfiguration,
			TVariables,
			TStatus
		>,
		TSlide extends CoreSlideItf,
		TAppResponse extends CoreAppResponseItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide
		>,
		TNodeRequest extends CoreNodeRequestItf<
			TConfiguration,
			TVariables,
			TStatus
		>,
		TNodeResponse extends CoreNodeResponseItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide
		>,
		TNodeRequestByAppRequestBuilder extends CoreNodeRequestByAppRequestBuilderItf<
			TConfiguration,
			TVariables,
			TStatus,
			TNodeRequest,
			TAppRequest
		>,
		TAppResponseByNodeResponseBuilder extends CoreAppResponseByNodeResponseBuilderItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide,
			TAppResponse,
			TNodeResponse
		>,
		TNextNodeRequestBuilder extends CoreNextNodeRequestBuilderItf<
			TConfiguration,
			TVariables,
			TStatus,
			TNodeRequest,
			TSlide,
			TNodeResponse
		>,
		TNodeRequestProcessor extends CoreNodeRequestProcessorItf<
			TConfiguration,
			TVariables,
			TStatus,
			TNodeRequest,
			TSlide,
			TNodeResponse
		>
	>
	implements CoreAppItf<
		TConfiguration,
		TVariables,
		TStatus,
		TAppRequest,
		TSlide,
		TAppResponse
	>
{

	private nodeRequestByAppRequestBuilder:TNodeRequestByAppRequestBuilder;
	private appResponseByNodeResponseBuilder:TAppResponseByNodeResponseBuilder;
	private nextNodeRequestBuilder:TNextNodeRequestBuilder;
	private nodeRequestProcessor:TNodeRequestProcessor;

	constructor(
		nodeRequestByAppRequestBuilder:TNodeRequestByAppRequestBuilder,
		appResponseByNodeResponseBuilder:TAppResponseByNodeResponseBuilder,
		nextNodeRequestBuilder:TNextNodeRequestBuilder,
		nodeRequestProcessor:TNodeRequestProcessor
	)
	{
		this.nodeRequestByAppRequestBuilder = nodeRequestByAppRequestBuilder;
		this.appResponseByNodeResponseBuilder = appResponseByNodeResponseBuilder;
		this.nextNodeRequestBuilder = nextNodeRequestBuilder;
		this.nodeRequestProcessor = nodeRequestProcessor;
	}

	public run(appRequest:TAppRequest):TAppResponse
	{
		let nodeRequest:TNodeRequest = this.nodeRequestByAppRequestBuilder.build(appRequest);

		let slidesAccumulator:TSlide[] = [];
		let nodeResponse:TNodeResponse;

		while (true)
		{
			nodeResponse = this.nodeRequestProcessor.process(nodeRequest);

			slidesAccumulator = slidesAccumulator.concat(
				nodeResponse.getSlides()
			);

			if (
				!nodeResponse.getProcessAgain()
			)
			{
				break;
			}

			nodeRequest = this.nextNodeRequestBuilder.build(nodeResponse);
		}

		let appResponse:TAppResponse = this.appResponseByNodeResponseBuilder.build(nodeResponse);
		appResponse.setSlides(slidesAccumulator);
		return appResponse;
	}

}