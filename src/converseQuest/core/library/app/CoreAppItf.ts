import { CoreAppRequestItf } from "../appRequest/CoreAppRequestItf";
import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreAppResponseItf } from "../appResponse/CoreAppResponseItf";
import { CoreSlideItf } from "../slide/CoreSlideItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";

export
	interface CoreAppItf<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>,
		TAppRequest extends CoreAppRequestItf<
			TConfiguration,
			TVariables,
			TStatus
		>,
		TSlide extends CoreSlideItf,
		TAppResponse extends CoreAppResponseItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide
		>
	>
{

	run: (appRequest:TAppRequest) => TAppResponse;

}