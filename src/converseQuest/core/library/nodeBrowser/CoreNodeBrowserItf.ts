import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreSlideItf } from "../slide/CoreSlideItf";
import { CoreNodeResponseItf } from "../nodeResponse/CoreNodeResponseItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";

export
	interface CoreNodeBrowserItf<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>,
		TSlide extends CoreSlideItf,
		TNodeResponse extends CoreNodeResponseItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide
		>
	>
{

	change: (nodeResponse:TNodeResponse, nodeId:string) => TNodeResponse;

	push: (nodeResponse:TNodeResponse, nodeId:string) => TNodeResponse;

	pop: (nodeResponse:TNodeResponse) => TNodeResponse;

}