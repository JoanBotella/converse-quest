import { CoreNodeBrowserItf } from "./CoreNodeBrowserItf";
import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreNodeResponseItf } from "../nodeResponse/CoreNodeResponseItf";
import { CoreSlideItf } from "../slide/CoreSlideItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";

export
	abstract class CoreNodeBrowserAbs<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>,
		TSlide extends CoreSlideItf,
		TNodeResponse extends CoreNodeResponseItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide
		>
	>
	implements CoreNodeBrowserItf<
		TConfiguration,
		TVariables,
		TStatus,
		TSlide,
		TNodeResponse
	>
{

	public change(nodeResponse:TNodeResponse, nodeId:string):TNodeResponse
	{
		let
			status:TStatus = nodeResponse.getStatus(),
			breadcrumbs:string[] = status.getBreadcrumbs()
		;

		breadcrumbs[breadcrumbs.length - 1] = nodeId;

		status.setBreadcrumbs(breadcrumbs);
		nodeResponse.setStatus(status);

		nodeResponse.setProcessAgain(true);

		return nodeResponse;
	}

	public push(nodeResponse:TNodeResponse, nodeId:string):TNodeResponse
	{
		let
			status:TStatus = nodeResponse.getStatus(),
			breadcrumbs:string[] = status.getBreadcrumbs()
		;

		breadcrumbs.push(nodeId);

		status.setBreadcrumbs(breadcrumbs);
		nodeResponse.setStatus(status);

		nodeResponse.setProcessAgain(true);

		return nodeResponse;
	}

	public pop(nodeResponse:TNodeResponse):TNodeResponse
	{
		let
			status:TStatus = nodeResponse.getStatus(),
			breadcrumbs:string[] = status.getBreadcrumbs()
		;

		if (breadcrumbs.length <= 1)
		{
			throw new Error('The breadcrumbs cannot be popped to emptyness.');
		}

		breadcrumbs.pop();

		status.setBreadcrumbs(breadcrumbs);
		nodeResponse.setStatus(status);

		nodeResponse.setProcessAgain(true);

		return nodeResponse;
	}

}