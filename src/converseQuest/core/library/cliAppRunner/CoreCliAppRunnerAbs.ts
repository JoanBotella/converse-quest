import { CoreCliAppRunnerItf } from "./CoreCliAppRunnerItf";
import { CoreAppItf } from "../app/CoreAppItf";
import { CoreAppRequestItf } from "../appRequest/CoreAppRequestItf";
import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreAppResponseItf } from "../appResponse/CoreAppResponseItf";
import { CoreSlideItf } from "../slide/CoreSlideItf";
import { CoreCliAppResponseDispatcherItf } from "../cliAppResponseDispatcher/CoreCliAppResponseDispatcherItf";
import { CoreCliAppRequestBuilderItf } from "../cliAppRequestBuilder/CoreCliAppRequestBuilderItf";
import { CoreStartingStatusBuilderItf } from "../startingStatusBuilder/CoreStartingStatusBuilderItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";

export
	abstract class CoreCliAppRunnerAbs<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>,
		TAppRequest extends CoreAppRequestItf<
			TConfiguration,
			TVariables,
			TStatus
		>,
		TSlide extends CoreSlideItf,
		TAppResponse extends CoreAppResponseItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide
		>,
		TStartingStatusBuilder extends CoreStartingStatusBuilderItf<
			TConfiguration,
			TVariables,
			TStatus
		>,
		TCliAppRequestBuilder extends CoreCliAppRequestBuilderItf<
			TConfiguration,
			TVariables,
			TStatus,
			TAppRequest
		>,
		TApp extends CoreAppItf<
			TConfiguration,
			TVariables,
			TStatus,
			TAppRequest,
			TSlide,
			TAppResponse
		>,
		TCliAppResponseDispatcher extends CoreCliAppResponseDispatcherItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide,
			TAppResponse
		>
	>
	implements CoreCliAppRunnerItf
{

	private startingStatusBuilder:TStartingStatusBuilder;
	private cliAppRequestBuilder:TCliAppRequestBuilder;
	private app:TApp;
	private cliAppResponseDispatcher:TCliAppResponseDispatcher;

	private firstStep:boolean;
	private end:boolean;

	constructor(
		startingStatusBuilder:TStartingStatusBuilder,
		cliAppRequestBuilder:TCliAppRequestBuilder,
		app:TApp,
		cliAppResponseDispatcher:TCliAppResponseDispatcher
	)
	{
		this.startingStatusBuilder = startingStatusBuilder;
		this.cliAppRequestBuilder = cliAppRequestBuilder;
		this.app = app;
		this.cliAppResponseDispatcher = cliAppResponseDispatcher;

		this.firstStep = true;
		this.end = false;
	}

	public run():void
	{
		this.firstStep = true;

		let status:TStatus = this.startingStatusBuilder.build();

		while (!this.end)
		{
			status = this.step(status);
		}
	}

		private step(status:TStatus):TStatus
		{
			let appRequest:TAppRequest = this.buildAppRequest(status);

			let appResponse:TAppResponse = this.app.run(appRequest);

			this.cliAppResponseDispatcher.dispatch(appResponse);

			this.end = appResponse.getEnd();

			return appResponse.getStatus();
		}

			private buildAppRequest(status:TStatus):TAppRequest
			{
				if (this.firstStep)
				{
					this.cliAppRequestBuilder.tryToBuildWithoutQuery(status);
					this.firstStep = false;
				}
				else
				{
					this.cliAppRequestBuilder.tryToBuildWithQuery(status);
				}

				if (this.cliAppRequestBuilder.hasAppRequest())
				{
					return this.cliAppRequestBuilder.getAppRequestAfterHas();
				}

				throw new Error('AppRequest could not be built!');
			}

}