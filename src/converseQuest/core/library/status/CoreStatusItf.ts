import { CoreVariablesItf } from "../variables/CoreVariablesItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";

export
	interface CoreStatusItf<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf
	>
{

	getBreadcrumbs: () => string[];

	setBreadcrumbs: (breadcrumbs:string[]) => void;

	getConfiguration: () => TConfiguration;

	setConfiguration: (configuration:TConfiguration) => void;

	getVariables: () => TVariables;

	setVariables: (variables:TVariables) => void;

}