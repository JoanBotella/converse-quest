import { CoreStatusItf } from "./CoreStatusItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";

export
	abstract class CoreStatusAbs<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf
	>
	implements CoreStatusItf<
		TConfiguration,
		TVariables
	>
{

	private breadcrumbs:string[];
	private configuration:TConfiguration;
	private variables:TVariables;

	constructor(
		nodeId:string,
		configuration:TConfiguration,
		variables:TVariables
	)
	{
		this.breadcrumbs = [nodeId];
		this.configuration = configuration;
		this.variables = variables;
	}

	public getBreadcrumbs():string[]
	{
		return this.breadcrumbs;
	}

	public setBreadcrumbs(breadcrumbs:string[]):void
	{
		this.breadcrumbs = breadcrumbs;
	}

	public getConfiguration():TConfiguration
	{
		return this.configuration;
	}

	public setConfiguration(configuration:TConfiguration):void
	{
		this.configuration = configuration;
	}

	public getVariables():TVariables
	{
		return this.variables;
	}

	public setVariables(variables:TVariables):void
	{
		this.variables = variables;
	}

}