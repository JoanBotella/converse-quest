import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreNodeRequestItf } from "../nodeRequest/CoreNodeRequestItf";
import { CoreNextNodeRequestBuilderItf } from "./CoreNextNodeRequestBuilderItf";
import { CoreSlideItf } from "../slide/CoreSlideItf";
import { CoreNodeResponseItf } from "../nodeResponse/CoreNodeResponseItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";

export
	abstract class CoreNextNodeRequestBuilderAbs<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>,
		TNodeRequest extends CoreNodeRequestItf<
			TConfiguration,
			TVariables,
			TStatus
		>,
		TSlide extends CoreSlideItf,
		TNodeResponse extends CoreNodeResponseItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide
		>
	>
	implements CoreNextNodeRequestBuilderItf<
		TConfiguration,
		TVariables,
		TStatus,
		TNodeRequest,
		TSlide,
		TNodeResponse
	>
{

	public abstract build(nodeResponse:TNodeResponse):TNodeRequest;

}