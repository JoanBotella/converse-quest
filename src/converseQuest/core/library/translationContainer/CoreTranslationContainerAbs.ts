import { CoreTranslationContainerItf } from "./CoreTranslationContainerItf";
import { CoreTranslationItf } from "../translation/CoreTranslationItf";

export
	abstract class CoreTranslationContainerAbs<
		TTranslation extends CoreTranslationItf
	>
	implements CoreTranslationContainerItf<
		TTranslation
	>
{

	private translations:Map<string, TTranslation>;

	constructor(
	)
	{
		this.translations = new Map<string, TTranslation>();
	}

	public get(languageCode:string):TTranslation
	{
		if (!this.translations.has(languageCode))
		{
			this.translations.set(
				languageCode,
				this.buildTranslation(languageCode)
			);
		}
		return this.translations.get(languageCode) as TTranslation;
	}

		protected abstract buildTranslation(languageCode:string):TTranslation;

}