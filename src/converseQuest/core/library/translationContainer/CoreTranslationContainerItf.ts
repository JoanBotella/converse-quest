import { CoreTranslationItf } from "../translation/CoreTranslationItf";

export
	interface CoreTranslationContainerItf<
		TTranslation extends CoreTranslationItf
	>
{

	get: (languageCode:string) => TTranslation

}