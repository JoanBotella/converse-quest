import { CoreNodeItf } from "../node/CoreNodeItf";
import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreNodeRequestItf } from "../nodeRequest/CoreNodeRequestItf";
import { CoreSlideItf } from "../slide/CoreSlideItf";
import { CoreNodeResponseItf } from "../nodeResponse/CoreNodeResponseItf";
import { CoreRouterItf } from "./CoreRouterItf";
import { CoreNodeBuilderItf } from "../nodeBuilder/CoreNodeBuilderItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";

export
	abstract class CoreRouterAbs<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>,
		TNodeRequest extends CoreNodeRequestItf<
			TConfiguration,
			TVariables,
			TStatus
		>,
		TSlide extends CoreSlideItf,
		TNodeResponse extends CoreNodeResponseItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide
		>,
		TNode extends CoreNodeItf<
			TConfiguration,
			TVariables,
			TStatus,
			TNodeRequest,
			TSlide,
			TNodeResponse
		>,
		TNodeBuilder extends CoreNodeBuilderItf
	>
	implements CoreRouterItf<
		TConfiguration,
		TVariables,
		TStatus,
		TNodeRequest,
		TSlide,
		TNodeResponse,
		TNode
	>
{

	private nodeBuilder:TNodeBuilder;

	constructor(
		nodeBuilder:TNodeBuilder
	)
	{
		this.nodeBuilder = nodeBuilder;
	}

	public abstract route(nodeId:string):TNode;

	protected getNodeBuilder():TNodeBuilder
	{
		return this.nodeBuilder;
	}

}