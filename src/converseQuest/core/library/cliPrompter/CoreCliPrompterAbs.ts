import { CoreCliPrompterItf } from "./CoreCliPrompterItf";
import { CoreCliStylizerItf } from "../../service/cliStylizer/CoreCliStylizerItf";

export
	abstract class CoreCliPrompterAbs<
		TCliStylizer extends CoreCliStylizerItf
	>
	implements CoreCliPrompterItf
{

	private reader:any;
	private stylizer:TCliStylizer;

	constructor(
		stylizer:TCliStylizer
	)
	{
		this.reader = require('readline-sync');
		this.stylizer = stylizer;
	}

	public prompt(text:string):string
	{
		return this.reader.question(
			this.stylizer.stylizeQueryPrompt(text)
		);
	}

}
