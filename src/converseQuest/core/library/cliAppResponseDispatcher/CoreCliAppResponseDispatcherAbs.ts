import { CoreCliAppResponseDispatcherItf } from "./CoreCliAppResponseDispatcherItf";
import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreSlideItf } from "../slide/CoreSlideItf";
import { CoreAppResponseItf } from "../appResponse/CoreAppResponseItf";
import { CoreCliStylizerItf } from "../../service/cliStylizer/CoreCliStylizerItf";
import { CoreCliPrompterItf } from "../cliPrompter/CoreCliPrompterItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";
import { CoreTranslationItf } from "../translation/CoreTranslationItf";
import { CoreTranslationContainerItf } from "../translationContainer/CoreTranslationContainerItf";

export
	abstract class CoreCliAppResponseDispatcherAbs<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>,
		TSlide extends CoreSlideItf,
		TAppResponse extends CoreAppResponseItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide
		>,
		TCliStylizer extends CoreCliStylizerItf,
		TCliPrompter extends CoreCliPrompterItf,
		TTranslation extends CoreTranslationItf,
		TTranslationContainer extends CoreTranslationContainerItf<
			TTranslation
		>
	>
	implements CoreCliAppResponseDispatcherItf<
		TConfiguration,
		TVariables,
		TStatus,
		TSlide,
		TAppResponse
	>
{

	private cliStylizer:TCliStylizer;
	private cliPrompter:TCliPrompter;
	private translationContainer:TTranslationContainer;

	constructor(
		cliStylizer:TCliStylizer,
		cliPrompter:TCliPrompter,
		translationContainer:TTranslationContainer
	)
	{
		this.cliStylizer = cliStylizer;
		this.cliPrompter = cliPrompter;
		this.translationContainer = translationContainer;
	}

	public dispatch(appResponse:TAppResponse):void
	{
		let
			slides = appResponse.getSlides(),
			prompt:string = this.buildPrompt(appResponse)
		;

		for (let index = 0; index < slides.length; index++)
		{
			if (index > 0)
			{
				this.cliPrompter.prompt(
					this.cliStylizer.stylizeSlidePrompt(prompt)
				);
			}

			this.dispatchSlide(slides[index]);
		}
	}

		private buildPrompt(appResponse:TAppResponse):string
		{
			let
				languageCode:string = appResponse.getStatus().getConfiguration().getLanguageCode(),
				translation:TTranslation = this.translationContainer.get(languageCode)
			;
			return translation.cliAppResponseDispatcher_prompt();
		}

		private dispatchSlide(slide:TSlide):void
		{
			if (slide.hasTitle())
			{
				console.log(
					this.cliStylizer.stylizeTitle(
						slide.getTitleAfterHas()
					)
				);
			}

			if (slide.hasText())
			{
				console.log(
					this.cliStylizer.stylizeText(
						slide.getTextAfterHas()
					)
				);
			}
		}

}