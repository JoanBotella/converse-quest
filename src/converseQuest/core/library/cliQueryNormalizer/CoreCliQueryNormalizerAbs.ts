import { CoreCliQueryNormalizerItf } from "./CoreCliQueryNormalizerItf";

export
	abstract class CoreCliQueryNormalizerAbs
	implements CoreCliQueryNormalizerItf
{

	public abstract normalize(text:string):string;

}