
export
	interface CoreCliQueryNormalizerItf
{

	normalize: (text:string) => string;

}