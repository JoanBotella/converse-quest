import { CoreCliAppRequestBuilderItf } from "./CoreCliAppRequestBuilderItf";
import { CoreAppRequestItf } from "../appRequest/CoreAppRequestItf";
import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreCliQueryBuilderItf } from "../cliQueryBuilder/CoreCliQueryBuilderItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";

export
	abstract class CoreCliAppRequestBuilderAbs<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>,
		TAppRequest extends CoreAppRequestItf<
			TConfiguration,
			TVariables,
			TStatus
		>,
		TCliQueryBuilder extends CoreCliQueryBuilderItf
	>
	implements CoreCliAppRequestBuilderItf<
		TConfiguration,
		TVariables,
		TStatus,
		TAppRequest
	>
{

	private queryBuilder:TCliQueryBuilder;
	private appRequest:TAppRequest|undefined;

	constructor(
		queryBuilder:TCliQueryBuilder
	)
	{
		this.queryBuilder = queryBuilder;

		this.appRequest = undefined;
	}

	public tryToBuildWithoutQuery(status:TStatus):void
	{
		this.appRequest = this.instantiate(status);
	}

		protected abstract instantiate(status:TStatus):TAppRequest;

	public tryToBuildWithQuery(status:TStatus):void
	{
		this.tryToBuildWithoutQuery(status);

		this.queryBuilder.tryToBuild(
			status.getConfiguration().getLanguageCode()
		);

		if (this.queryBuilder.hasQuery())
		{
			(this.appRequest as TAppRequest).setQuery(
				this.queryBuilder.getQueryAfterHas()
			);
		}
	}

	public hasAppRequest():boolean
	{
		return this.appRequest !== undefined;
	}

	public getAppRequestAfterHas():TAppRequest
	{
		return this.appRequest as TAppRequest;
	}

}