import { CoreAppRequestItf } from "../appRequest/CoreAppRequestItf";
import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";

export
	interface CoreCliAppRequestBuilderItf<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>,
		TAppRequest extends CoreAppRequestItf<
			TConfiguration,
			TVariables,
			TStatus
		>
	>
{

	tryToBuildWithoutQuery: (status:TStatus) => void;

	tryToBuildWithQuery: (status:TStatus) => void;

	hasAppRequest: () => boolean;

	getAppRequestAfterHas: () => TAppRequest;

}