import { CoreStatusItf } from '../status/CoreStatusItf'
import { CoreNodeResponseItf } from '../nodeResponse/CoreNodeResponseItf'
import { CoreNodeRequestItf } from '../nodeRequest/CoreNodeRequestItf'
import { CoreSlideItf } from '../slide/CoreSlideItf'
import { CoreConfigurationItf } from '../configuration/CoreConfigurationItf'
import { CoreVariablesItf } from '../variables/CoreVariablesItf'

export
	interface CoreNodeItf<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>,
		TNodeRequest extends CoreNodeRequestItf<
			TConfiguration,
			TVariables,
			TStatus
		>,
		TSlide extends CoreSlideItf,
		TNodeResponse extends CoreNodeResponseItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide
		>
	>
{

	run: (nodeRequest:TNodeRequest) => void;

	hasNodeResponse: () => boolean;

	getNodeResponseAfterHas: () => TNodeResponse;

}