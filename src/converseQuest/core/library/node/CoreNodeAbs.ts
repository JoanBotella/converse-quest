import { CoreNodeItf } from './CoreNodeItf';
import { CoreStatusItf } from '../status/CoreStatusItf';
import { CoreNodeRequestItf } from '../nodeRequest/CoreNodeRequestItf';
import { CoreSlideItf } from '../slide/CoreSlideItf';
import { CoreNodeResponseItf } from '../nodeResponse/CoreNodeResponseItf';
import { CoreNodeBrowserItf } from '../nodeBrowser/CoreNodeBrowserItf';
import { CoreVariablesItf } from '../variables/CoreVariablesItf';
import { CoreConfigurationItf } from '../configuration/CoreConfigurationItf';
import { CoreTranslationContainerItf } from '../translationContainer/CoreTranslationContainerItf';
import { CoreTranslationItf } from '../translation/CoreTranslationItf';

export
	abstract class CoreNodeAbs<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>,
		TNodeRequest extends CoreNodeRequestItf<
			TConfiguration,
			TVariables,
			TStatus
		>,
		TSlide extends CoreSlideItf,
		TNodeResponse extends CoreNodeResponseItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide
		>,
		TNodeBrowser extends CoreNodeBrowserItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide,
			TNodeResponse
		>,
		TTranslation extends CoreTranslationItf,
		TTranslatorContainer extends CoreTranslationContainerItf<
			TTranslation
		>
	>
	implements CoreNodeItf<
		TConfiguration,
		TVariables,
		TStatus,
		TNodeRequest,
		TSlide,
		TNodeResponse
	>
{

	private nodeBrowser:TNodeBrowser;
	private translationContainer:TTranslatorContainer;

	private nodeRequest:TNodeRequest|undefined;
	private nodeResponse:TNodeResponse|undefined;

	constructor(
		nodeBrowser:TNodeBrowser,
		translationContainer:TTranslatorContainer
	)
	{
		this.nodeBrowser = nodeBrowser;
		this.translationContainer = translationContainer;

		this.nodeRequest = undefined;
		this.nodeResponse = undefined;
	}

	public run(nodeRequest:TNodeRequest):void
	{
		this.unsetNodeResponse();

		this.nodeRequest = nodeRequest;

		this.tryToSetupNodeResponse();
	}

		protected abstract tryToSetupNodeResponse():void;

	public hasNodeResponse():boolean
	{
		return this.nodeResponse !== undefined;
	}

	public getNodeResponseAfterHas():TNodeResponse
	{
		return this.nodeResponse as TNodeResponse;
	}

	protected setNodeResponse(nodeResponse:TNodeResponse):void
	{
		this.nodeResponse = nodeResponse;
	}

	protected unsetNodeResponse():void
	{
		this.nodeResponse = undefined;
	}

	protected getNodeRequest():TNodeRequest
	{
		return this.nodeRequest as TNodeRequest;
	}

	protected hasNodeRequestQuery():boolean
	{
		return this.getNodeRequest().hasQuery();
	}

	protected getNodeRequestQueryAfterHas():string
	{
		return this.getNodeRequest().getQueryAfterHas();
	}

	protected getNodeBrowser():TNodeBrowser
	{
		return this.nodeBrowser;
	}

	protected getTranslation():TTranslation
	{
		return this.translationContainer.get(
			this
				.getNodeRequest()
				.getStatus()
				.getConfiguration()
				.getLanguageCode()
		);
	}

	protected queryMatches(list:string):boolean
	{
		let items:string[] = list.split(',');
		return items.indexOf(this.getNodeRequestQueryAfterHas()) != -1;
	}

}