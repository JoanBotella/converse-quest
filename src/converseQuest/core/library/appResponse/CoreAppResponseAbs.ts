import { CoreAppResponseItf } from "./CoreAppResponseItf";
import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreSlideItf } from "../slide/CoreSlideItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";

export
	abstract class CoreAppResponseAbs<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>,
		TSlide extends CoreSlideItf
	>
	implements CoreAppResponseItf<
		TConfiguration,
		TVariables,
		TStatus,
		TSlide
	>
{

	private status:TStatus;
	private slides:TSlide[];
	private end:boolean;

	constructor(
		status:TStatus
	)
	{
		this.status = status;
		this.slides = [];
		this.end = false;
	}

	public getStatus():TStatus
	{
		return this.status;
	}

	public setStatus(status:TStatus):void
	{
		this.status = status;
	}

	public appendSlide(slide:TSlide):void
	{
		this.slides.push(slide);
	}

	public getSlides():TSlide[]
	{
		return this.slides;
	}

	public setSlides(slides:TSlide[]):void
	{
		this.slides = slides;
	}

	public setEnd(end:boolean):void
	{
		this.end = end;
	}

	public getEnd():boolean
	{
		return this.end;
	}

}