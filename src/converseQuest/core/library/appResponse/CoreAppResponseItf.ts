import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreSlideItf } from "../slide/CoreSlideItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";

export
	interface CoreAppResponseItf<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>,
		TSlide extends CoreSlideItf
	>
{

	getStatus: () => TStatus;

	setStatus: (status:TStatus) => void;

	appendSlide: (slide:TSlide) => void;

	getSlides: () => TSlide[];

	setSlides(slides:TSlide[]):void;

	setEnd: (end:boolean) => void;

	getEnd: () => boolean;

}