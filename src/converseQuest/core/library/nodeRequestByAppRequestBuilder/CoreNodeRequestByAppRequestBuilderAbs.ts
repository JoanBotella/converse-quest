import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreNodeRequestItf } from "../nodeRequest/CoreNodeRequestItf";
import { CoreAppRequestItf } from "../appRequest/CoreAppRequestItf";
import { CoreNodeRequestByAppRequestBuilderItf } from "./CoreNodeRequestByAppRequestBuilderItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";

export
	abstract class CoreNodeRequestByAppRequestBuilderAbs<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>,
		TNodeRequest extends CoreNodeRequestItf<
			TConfiguration,
			TVariables,
			TStatus
		>,
		TAppRequest extends CoreAppRequestItf<
			TConfiguration,
			TVariables,
			TStatus
		>
	>
	implements CoreNodeRequestByAppRequestBuilderItf<
		TConfiguration,
		TVariables,
		TStatus,
		TNodeRequest,
		TAppRequest
	>
{

	public build(appRequest:TAppRequest):TNodeRequest
	{
		let nodeRequest:TNodeRequest = this.instantiateNodeRequest(
			appRequest.getStatus()
		);

		if (appRequest.hasQuery())
		{
			nodeRequest.setQuery(
				appRequest.getQueryAfterHas()
			);
		}

		return nodeRequest;
	}

		protected abstract instantiateNodeRequest(status:TStatus):TNodeRequest;

}