import { CoreSystemNodeResponseBuilderItf } from "./CoreSystemNodeResponseBuilderItf";
import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreSlideItf } from "../slide/CoreSlideItf";
import { CoreNodeResponseItf } from "../nodeResponse/CoreNodeResponseItf";
import { CoreNodeRequestItf } from "../nodeRequest/CoreNodeRequestItf";
import { CoreNodeBrowserItf } from "../nodeBrowser/CoreNodeBrowserItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";
import { CoreTranslationItf } from "../translation/CoreTranslationItf";
import { CoreTranslationContainerItf } from "../translationContainer/CoreTranslationContainerItf";

export
	abstract class CoreSystemNodeResponseBuilderAbs<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>,
		TNodeRequest extends CoreNodeRequestItf<
			TConfiguration,
			TVariables,
			TStatus
		>,
		TSlide extends CoreSlideItf,
		TNodeResponse extends CoreNodeResponseItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide
		>,
		TNodeBrowser extends CoreNodeBrowserItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide,
			TNodeResponse
		>,
		TTranslation extends CoreTranslationItf,
		TTranslationContainer extends CoreTranslationContainerItf<
			TTranslation
		>
	>
	implements CoreSystemNodeResponseBuilderItf<
		TConfiguration,
		TVariables,
		TStatus,
		TNodeRequest,
		TSlide,
		TNodeResponse
	>
{

	private nodeBrowser:TNodeBrowser;
	private translationContainer:TTranslationContainer;

	private nodeRequest:TNodeRequest|undefined;

	constructor(
		nodeBrowser:TNodeBrowser,
		translationContainer:TTranslationContainer
	)
	{
		this.nodeBrowser = nodeBrowser;
		this.translationContainer = translationContainer;
	}

	public build(nodeRequest:TNodeRequest):TNodeResponse
	{
		this.nodeRequest = nodeRequest;

		return this.buildNodeResponse();
	}

		protected abstract buildNodeResponse():TNodeResponse;

	protected hasNodeRequestQuery():boolean
	{
		return this.getNodeRequest().hasQuery();
	}

	protected getNodeRequestQueryAfterHas():string
	{
		return this.getNodeRequest().getQueryAfterHas();
	}

	protected getNodeRequest():TNodeRequest
	{
		return this.nodeRequest as TNodeRequest;
	}

	protected getNodeBrowser():TNodeBrowser
	{
		return this.nodeBrowser;
	}

	protected getTranslation():TTranslation
	{
		return this.translationContainer.get(
			this
				.getNodeRequest()
				.getStatus()
				.getConfiguration()
				.getLanguageCode()
		);
	}

	protected queryMatches(list:string):boolean
	{
		let items:string[] = list.split(',');
		return items.indexOf(this.getNodeRequestQueryAfterHas()) != -1;
	}

}