import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreAppResponseItf } from "../appResponse/CoreAppResponseItf";
import { CoreSlideItf } from "../slide/CoreSlideItf";
import { CoreNodeResponseItf } from "../nodeResponse/CoreNodeResponseItf";
import { CoreAppResponseByNodeResponseBuilderItf } from "./CoreAppResponseByNodeResponseBuilderItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";

export
	abstract class CoreAppResponseByNodeResponseBuilderAbs<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>,
		TSlide extends CoreSlideItf,
		TAppResponse extends CoreAppResponseItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide
		>,
		TNodeResponse extends CoreNodeResponseItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide
		>
	>
	implements CoreAppResponseByNodeResponseBuilderItf<
		TConfiguration,
		TVariables,
		TStatus,
		TSlide,
		TAppResponse,
		TNodeResponse
	>
{

	public build(nodeResponse:TNodeResponse):TAppResponse
	{
		let appResponse:TAppResponse = this.instantiate(
			nodeResponse.getStatus()
		);

		appResponse.setSlides(
			nodeResponse.getSlides()
		);

		appResponse.setEnd(
			nodeResponse.getEnd()
		);

		return appResponse;
	}

		protected abstract instantiate(status:TStatus):TAppResponse;

}