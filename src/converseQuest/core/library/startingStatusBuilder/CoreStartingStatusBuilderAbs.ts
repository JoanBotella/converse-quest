import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreStartingStatusBuilderItf } from "./CoreStartingStatusBuilderItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";

export
	abstract class CoreStartingStatusBuilderAbs<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>
	>
	implements CoreStartingStatusBuilderItf<
		TConfiguration,
		TVariables,
		TStatus
	>
{

	public abstract build():TStatus;

}