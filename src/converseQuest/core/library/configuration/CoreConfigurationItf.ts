
export
	interface CoreConfigurationItf
{

	getLanguageCode: () => string;

	setLanguageCode: (languageCode:string) => void;

}