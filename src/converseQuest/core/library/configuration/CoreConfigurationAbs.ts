import { CoreConfigurationItf } from "./CoreConfigurationItf";

export
	abstract class CoreConfigurationAbs
	implements CoreConfigurationItf
{

	private languageCode:string;

	constructor(
		languageCode:string
	)
	{
		this.languageCode = languageCode;
	}

	public getLanguageCode():string
	{
		return this.languageCode;
	}

	public setLanguageCode(languageCode:string):void
	{
		this.languageCode = languageCode;
	}

}