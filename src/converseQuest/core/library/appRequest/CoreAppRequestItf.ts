import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";

export
	interface CoreAppRequestItf<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>
	>
{

	hasQuery: () => boolean;

	getQueryAfterHas: () => string;

	setQuery: (query:string) => void;

	unsetQuery: () => void;

	getStatus: () => TStatus;

	setStatus: (status:TStatus) => void;

}