import { CoreAppRequestItf } from "./CoreAppRequestItf";
import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";

export
	abstract class CoreAppRequestAbs<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>
	>
	implements CoreAppRequestItf<
		TConfiguration,
		TVariables,
		TStatus
	>
{

	private query:string|undefined;
	private status:TStatus;

	constructor(
		status:TStatus
	)
	{
		this.query = undefined;
		this.status = status;
	}

	public hasQuery():boolean
	{
		return this.query !== undefined;
	}

	public getQueryAfterHas():string
	{
		return this.query as string;
	}

	public setQuery(query:string):void
	{
		this.query = query;
	}

	public unsetQuery():void
	{
		this.query = undefined;
	}

	public getStatus():TStatus
	{
		return this.status;
	}

	public setStatus(status:TStatus):void
	{
		this.status = status;
	}

}