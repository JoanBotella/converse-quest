
export
	interface CoreTranslationItf
{

	cliQueryBuilder_prompt: () => string;

	cliAppResponseDispatcher_prompt: () => string;

}