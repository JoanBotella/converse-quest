import { CoreTranslationItf } from "./CoreTranslationItf";

export
	abstract class CoreTranslationAbs
	implements CoreTranslationItf
{

	public cliQueryBuilder_prompt():string { return '> '; }

	public cliAppResponseDispatcher_prompt():string { return '...'; }

}