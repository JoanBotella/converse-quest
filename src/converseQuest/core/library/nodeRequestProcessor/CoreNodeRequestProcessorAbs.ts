import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreNodeRequestItf } from "../nodeRequest/CoreNodeRequestItf";
import { CoreSlideItf } from "../slide/CoreSlideItf";
import { CoreNodeResponseItf } from "../nodeResponse/CoreNodeResponseItf";
import { CoreNodeRequestProcessorItf } from "./CoreNodeRequestProcessorItf";
import { CoreSystemNodeResponseBuilderItf } from "../systemNodeResponseBuilder/CoreSystemNodeResponseBuilderItf";
import { CoreNodeItf } from "../node/CoreNodeItf";
import { CoreRouterItf } from "../router/CoreRouterItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";

export
	abstract class CoreNodeRequestProcessorAbs<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>,
		TNodeRequest extends CoreNodeRequestItf<
			TConfiguration,
			TVariables,
			TStatus
		>,
		TSlide extends CoreSlideItf,
		TNodeResponse extends CoreNodeResponseItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide
		>,
		TSystemNodeResponseBuilder extends CoreSystemNodeResponseBuilderItf<
			TConfiguration,
			TVariables,
			TStatus,
			TNodeRequest,
			TSlide,
			TNodeResponse
		>,
		TNode extends CoreNodeItf<
			TConfiguration,
			TVariables,
			TStatus,
			TNodeRequest,
			TSlide,
			TNodeResponse
		>,
		TRouter extends CoreRouterItf<
			TConfiguration,
			TVariables,
			TStatus,
			TNodeRequest,
			TSlide,
			TNodeResponse,
			TNode
		>
	>
	implements CoreNodeRequestProcessorItf<
		TConfiguration,
		TVariables,
		TStatus,
		TNodeRequest,
		TSlide,
		TNodeResponse
	>
{

	private systemNodeResponseBuilder:TSystemNodeResponseBuilder;
	private router:TRouter;

	constructor(
		systemNodeResponseBuilder:TSystemNodeResponseBuilder,
		router:TRouter
	)
	{
		this.systemNodeResponseBuilder = systemNodeResponseBuilder;
		this.router = router;
	}

	public process(nodeRequest:TNodeRequest):TNodeResponse
	{
		let
			nodeId:string = this.getNodeIdFromBreadcrumbs(nodeRequest),
			node:TNode = this.router.route(nodeId)
		;
		return this.buildNodeResponse(node, nodeRequest);
	}

		private getNodeIdFromBreadcrumbs(nodeRequest:TNodeRequest):string
		{
			let breadcrumbs:string[] = nodeRequest.getStatus().getBreadcrumbs();

			if (breadcrumbs.length)
			{
				return breadcrumbs[breadcrumbs.length - 1];
			}

			throw new Error('Any node could be processed because breadcrumbs are empty.');
		}

		private buildNodeResponse(node:TNode, nodeRequest:TNodeRequest):TNodeResponse
		{
			node.run(nodeRequest);

			if (node.hasNodeResponse())
			{
				return node.getNodeResponseAfterHas();
			}

			return this.systemNodeResponseBuilder.build(nodeRequest);
		}

}