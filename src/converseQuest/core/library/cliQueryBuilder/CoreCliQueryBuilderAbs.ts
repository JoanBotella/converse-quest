import { CoreCliPrompterItf } from "../cliPrompter/CoreCliPrompterItf";
import { CoreCliQueryNormalizerItf } from "../cliQueryNormalizer/CoreCliQueryNormalizerItf";
import { CoreCliQueryNormalizerContainerItf } from "../cliQueryNormalizerContainer/CoreCliQueryNormalizerContainerItf";
import { CoreTranslationItf } from "../translation/CoreTranslationItf";
import { CoreTranslationContainerItf } from "../translationContainer/CoreTranslationContainerItf";

export
	class CoreCliQueryBuilderAbs<
		TCliPrompter extends CoreCliPrompterItf,
		TCliQueryNormalizer extends CoreCliQueryNormalizerItf,
		TCliQueryNormalizerContainer extends CoreCliQueryNormalizerContainerItf<
			TCliQueryNormalizer
		>,
		TTranslation extends CoreTranslationItf,
		TTranslationContainer extends CoreTranslationContainerItf<
			TTranslation
		>
	>

{

	private cliPrompter:TCliPrompter;
	private cliQueryNormalizerContainer:TCliQueryNormalizerContainer;
	private translationContainer:TTranslationContainer;

	private query:string|undefined;

	constructor(
		cliPrompter:TCliPrompter,
		cliQueryNormalizerContainer:TCliQueryNormalizerContainer,
		translationContainer:TTranslationContainer
	)
	{
		this.cliPrompter = cliPrompter;
		this.cliQueryNormalizerContainer = cliQueryNormalizerContainer;
		this.translationContainer = translationContainer;

		this.query = undefined;
	}

	public tryToBuild(languageCode:string):void
	{
		this.query = undefined;

		let answer:string = this.prompt(languageCode);
		if (answer != '')
		{
			let cliQueryNormalizer:TCliQueryNormalizer = this.cliQueryNormalizerContainer.get(languageCode);
			this.query = cliQueryNormalizer.normalize(answer);
		}
	}

		private prompt(languageCode:string):string
		{
			let translation:TTranslation = this.translationContainer.get(languageCode);
			return this.cliPrompter.prompt(
				translation.cliQueryBuilder_prompt()
			);
		}

	public hasQuery():boolean
	{
		return this.query !== undefined;
	}

	public getQueryAfterHas():string
	{
		return this.query as string;
	}

}