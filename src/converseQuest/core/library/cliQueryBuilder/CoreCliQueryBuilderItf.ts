
export
	interface CoreCliQueryBuilderItf
{

	tryToBuild: (languageCode:string) => void;

	hasQuery: () => boolean;

	getQueryAfterHas: () => string;

}