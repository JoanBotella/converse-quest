import { CoreNodeResponseItf } from "./CoreNodeResponseItf";
import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreSlideItf } from "../slide/CoreSlideItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";

export
	abstract class CoreNodeResponseAbs<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>,
		TSlide extends CoreSlideItf
	>
	implements CoreNodeResponseItf<
		TConfiguration,
		TVariables,
		TStatus,
		TSlide
	>
{

	private status:TStatus;
	private slides:TSlide[];
	private end:boolean;
	private processAgain:boolean;

	constructor(
		status:TStatus
	)
	{
		this.status = status;
		this.slides = [];
		this.end = false;
		this.processAgain = false;
	}

	public getStatus():TStatus
	{
		return this.status;
	}

	public setStatus(status:TStatus):void
	{
		this.status = status;
	}

	public appendSlide(slide:TSlide):void
	{
		this.slides.push(slide);
	}

	public getSlides():TSlide[]
	{
		return this.slides;
	}

	public setSlides(slides:TSlide[]):void
	{
		this.slides = slides;
	}

	public setEnd(end:boolean):void
	{
		this.end = end;
	}

	public getEnd():boolean
	{
		return this.end;
	}

	public setProcessAgain(processAgain:boolean):void
	{
		this.processAgain = processAgain;
	}

	public getProcessAgain():boolean
	{
		return this.processAgain;
	}

}