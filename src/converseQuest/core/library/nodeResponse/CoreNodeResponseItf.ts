import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreSlideItf } from "../slide/CoreSlideItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";

export
	interface CoreNodeResponseItf<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>,
		TSlide extends CoreSlideItf
	>
{

	getStatus: () => TStatus;

	setStatus: (status:TStatus) => void;

	appendSlide: (slide:TSlide) => void;

	getSlides: () => TSlide[];

	setSlides(slides:TSlide[]):void;

	setEnd: (end:boolean) => void;

	getEnd: () => boolean;

	setProcessAgain: (processAgain:boolean) => void;

	getProcessAgain: () => boolean;

}