import { CoreCliQueryNormalizerContainerItf } from "./CoreCliQueryNormalizerContainerItf";
import { CoreCliQueryNormalizerItf } from "../cliQueryNormalizer/CoreCliQueryNormalizerItf";

export
	abstract class CoreCliQueryNormalizerContainerAbs<
		TCliQueryNormalizer extends CoreCliQueryNormalizerItf
	>
	implements CoreCliQueryNormalizerContainerItf<
		TCliQueryNormalizer
	>
{

	private cliQueryNormalizers:Map<string, TCliQueryNormalizer>;

	constructor(
	)
	{
		this.cliQueryNormalizers = new Map<string, TCliQueryNormalizer>();
	}

	public get(languageCode:string):TCliQueryNormalizer
	{
		if (!this.cliQueryNormalizers.has(languageCode))
		{
			this.cliQueryNormalizers.set(
				languageCode,
				this.buildCliQueryNormalizer(languageCode)
			);
		}
		return this.cliQueryNormalizers.get(languageCode) as TCliQueryNormalizer;
	}

		protected abstract buildCliQueryNormalizer(languageCode:string):TCliQueryNormalizer;

}