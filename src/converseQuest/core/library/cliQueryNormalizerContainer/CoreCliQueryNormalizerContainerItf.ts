import { CoreCliQueryNormalizerItf } from "../cliQueryNormalizer/CoreCliQueryNormalizerItf";

export
	interface CoreCliQueryNormalizerContainerItf<
		TCliQueryNormalizer extends CoreCliQueryNormalizerItf
	>
{

	get: (languageCode:string) => TCliQueryNormalizer;

}