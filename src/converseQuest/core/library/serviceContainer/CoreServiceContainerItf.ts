import { CoreCliAppRunnerItf } from "../cliAppRunner/CoreCliAppRunnerItf";
import { CoreCliAppRequestBuilderItf } from "../cliAppRequestBuilder/CoreCliAppRequestBuilderItf";
import { CoreAppItf } from "../app/CoreAppItf";
import { CoreCliAppResponseDispatcherItf } from "../cliAppResponseDispatcher/CoreCliAppResponseDispatcherItf";
import { CoreRouterItf } from "../router/CoreRouterItf";
import { CoreCliStylizerItf } from "../../service/cliStylizer/CoreCliStylizerItf";
import { CoreCliPrompterItf } from "../cliPrompter/CoreCliPrompterItf";
import { CoreCliQueryBuilderItf } from "../cliQueryBuilder/CoreCliQueryBuilderItf";
import { CoreNodeRequestByAppRequestBuilderItf } from "../nodeRequestByAppRequestBuilder/CoreNodeRequestByAppRequestBuilderItf";
import { CoreSystemNodeResponseBuilderItf } from "../systemNodeResponseBuilder/CoreSystemNodeResponseBuilderItf";
import { CoreAppResponseByNodeResponseBuilderItf } from "../appResponseByNodeResponseBuilder/CoreAppResponseByNodeResponseBuilderItf";
import { CoreNextNodeRequestBuilderItf } from "../nextNodeRequestBuilder/CoreNextNodeRequestBuilderItf";
import { CoreNodeRequestProcessorItf } from "../nodeRequestProcessor/CoreNodeRequestProcessorItf";
import { CoreStartingStatusBuilderItf } from "../startingStatusBuilder/CoreStartingStatusBuilderItf";
import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreAppRequestItf } from "../appRequest/CoreAppRequestItf";
import { CoreSlideItf } from "../slide/CoreSlideItf";
import { CoreAppResponseItf } from "../appResponse/CoreAppResponseItf";
import { CoreNodeRequestItf } from "../nodeRequest/CoreNodeRequestItf";
import { CoreNodeResponseItf } from "../nodeResponse/CoreNodeResponseItf";
import { CoreNodeItf } from "../node/CoreNodeItf";
import { CoreNodeBuilderItf } from "../nodeBuilder/CoreNodeBuilderItf";
import { CoreNodeBrowserItf } from "../nodeBrowser/CoreNodeBrowserItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";
import { CoreTranslationItf } from "../translation/CoreTranslationItf";
import { CoreTranslationContainerItf } from "../translationContainer/CoreTranslationContainerItf";
import { CoreCliQueryNormalizerContainerItf } from "../cliQueryNormalizerContainer/CoreCliQueryNormalizerContainerItf";
import { CoreCliQueryNormalizerItf } from "../cliQueryNormalizer/CoreCliQueryNormalizerItf";

export
	interface CoreServiceContainerItf<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>,
		TNodeRequest extends CoreNodeRequestItf<
			TConfiguration,
			TVariables,
			TStatus
		>,
		TSlide extends CoreSlideItf,
		TNodeResponse extends CoreNodeResponseItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide
		>,
		TNode extends CoreNodeItf<
			TConfiguration,
			TVariables,
			TStatus,
			TNodeRequest,
			TSlide,
			TNodeResponse
		>,
		TAppRequest extends CoreAppRequestItf<
			TConfiguration,
			TVariables,
			TStatus
		>,
		TAppResponse extends CoreAppResponseItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide
		>,

		TCliAppRunner extends CoreCliAppRunnerItf,
		TCliAppRequestBuilder extends CoreCliAppRequestBuilderItf<
			TConfiguration,
			TVariables,
			TStatus,
			TAppRequest
		>,
		TApp extends CoreAppItf<
			TConfiguration,
			TVariables,
			TStatus,
			TAppRequest,
			TSlide,
			TAppResponse
		>,
		TCliAppResponseDispatcher extends CoreCliAppResponseDispatcherItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide,
			TAppResponse
		>,
		TRouter extends CoreRouterItf<
			TConfiguration,
			TVariables,
			TStatus,
			TNodeRequest,
			TSlide,
			TNodeResponse,
			TNode
		>,
		TCliStylizer extends CoreCliStylizerItf,
		TCliQueryNormalizer extends CoreCliQueryNormalizerItf,
		TCliQueryNormalizerContainer extends CoreCliQueryNormalizerContainerItf<
			TCliQueryNormalizer
		>,
		TCliPrompter extends CoreCliPrompterItf,
		TCliQueryBuilder extends CoreCliQueryBuilderItf,
		TNodeRequestByAppRequestBuilder extends CoreNodeRequestByAppRequestBuilderItf<
			TConfiguration,
			TVariables,
			TStatus,
			TNodeRequest,
			TAppRequest
		>,
		TSystemNodeResponseBuilder extends CoreSystemNodeResponseBuilderItf<
			TConfiguration,
			TVariables,
			TStatus,
			TNodeRequest,
			TSlide,
			TNodeResponse
		>,
		TAppResponseByNodeResponseBuilder extends CoreAppResponseByNodeResponseBuilderItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide,
			TAppResponse,
			TNodeResponse
		>,
		TNextNodeRequestBuilder extends CoreNextNodeRequestBuilderItf<
			TConfiguration,
			TVariables,
			TStatus,
			TNodeRequest,
			TSlide,
			TNodeResponse
		>,
		TNodeRequestProcessor extends CoreNodeRequestProcessorItf<
			TConfiguration,
			TVariables,
			TStatus,
			TNodeRequest,
			TSlide,
			TNodeResponse
		>,
		TStartingStatusBuilder extends CoreStartingStatusBuilderItf<
			TConfiguration,
			TVariables,
			TStatus
		>,
		TNodeBuilder extends CoreNodeBuilderItf,
		TNodeBrowser extends CoreNodeBrowserItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide,
			TNodeResponse
		>,
		TTranslation extends CoreTranslationItf,
		TTranslationContainer extends CoreTranslationContainerItf<
			TTranslation
		>
	>
{

	getCliAppRunner: () => TCliAppRunner;

	getCliAppRequestBuilder: () => TCliAppRequestBuilder;

	getApp: () => TApp;

	getCliAppResponseDispatcher: () => TCliAppResponseDispatcher;

	getRouter: () => TRouter;

	getCliStylizer: () => TCliStylizer;

	getCliQueryNormalizerContainer: () => TCliQueryNormalizerContainer;

	getCliPrompter: () => TCliPrompter;

	getCliQueryBuilder: () => TCliQueryBuilder;

	getNodeRequestByAppRequestBuilder: () => TNodeRequestByAppRequestBuilder;

	getSystemNodeResponseBuilder: () => TSystemNodeResponseBuilder;

	getAppResponseByNodeResponseBuilder: () => TAppResponseByNodeResponseBuilder;

	getNextNodeRequestBuilder: () => TNextNodeRequestBuilder;

	getNodeRequestProcessor: () => TNodeRequestProcessor;

	getStartingStatusBuilder: () => TStartingStatusBuilder;

	getNodeBuilder: () => TNodeBuilder;

	getNodeBrowser: () => TNodeBrowser;

	getTranslationContainer: () => TTranslationContainer;

}