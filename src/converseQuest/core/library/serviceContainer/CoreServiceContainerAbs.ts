import { CoreServiceContainerItf } from "./CoreServiceContainerItf";
import { CoreStatusItf } from "../status/CoreStatusItf";
import { CoreNodeRequestItf } from "../nodeRequest/CoreNodeRequestItf";
import { CoreSlideItf } from "../slide/CoreSlideItf";
import { CoreNodeResponseItf } from "../nodeResponse/CoreNodeResponseItf";
import { CoreNodeItf } from "../node/CoreNodeItf";
import { CoreAppRequestItf } from "../appRequest/CoreAppRequestItf";
import { CoreAppResponseItf } from "../appResponse/CoreAppResponseItf";
import { CoreCliAppRunnerItf } from "../cliAppRunner/CoreCliAppRunnerItf";
import { CoreCliAppRequestBuilderItf } from "../cliAppRequestBuilder/CoreCliAppRequestBuilderItf";
import { CoreAppItf } from "../app/CoreAppItf";
import { CoreCliAppResponseDispatcherItf } from "../cliAppResponseDispatcher/CoreCliAppResponseDispatcherItf";
import { CoreRouterItf } from "../router/CoreRouterItf";
import { CoreCliStylizerItf } from "../../service/cliStylizer/CoreCliStylizerItf";
import { CoreCliPrompterItf } from "../cliPrompter/CoreCliPrompterItf";
import { CoreCliQueryBuilderItf } from "../cliQueryBuilder/CoreCliQueryBuilderItf";
import { CoreNodeRequestByAppRequestBuilderItf } from "../nodeRequestByAppRequestBuilder/CoreNodeRequestByAppRequestBuilderItf";
import { CoreSystemNodeResponseBuilderItf } from "../systemNodeResponseBuilder/CoreSystemNodeResponseBuilderItf";
import { CoreAppResponseByNodeResponseBuilderItf } from "../appResponseByNodeResponseBuilder/CoreAppResponseByNodeResponseBuilderItf";
import { CoreNextNodeRequestBuilderItf } from "../nextNodeRequestBuilder/CoreNextNodeRequestBuilderItf";
import { CoreNodeRequestProcessorItf } from "../nodeRequestProcessor/CoreNodeRequestProcessorItf";
import { CoreStartingStatusBuilderItf } from "../startingStatusBuilder/CoreStartingStatusBuilderItf";
import { CoreNodeBuilderItf } from "../nodeBuilder/CoreNodeBuilderItf";
import { CoreNodeBrowserItf } from "../nodeBrowser/CoreNodeBrowserItf";
import { CoreConfigurationItf } from "../configuration/CoreConfigurationItf";
import { CoreVariablesItf } from "../variables/CoreVariablesItf";
import { CoreTranslationContainerItf } from "../translationContainer/CoreTranslationContainerItf";
import { CoreTranslationItf } from "../translation/CoreTranslationItf";
import { CoreCliQueryNormalizerItf } from "../cliQueryNormalizer/CoreCliQueryNormalizerItf";
import { CoreCliQueryNormalizerContainerItf } from "../cliQueryNormalizerContainer/CoreCliQueryNormalizerContainerItf";

export
	abstract class CoreServiceContainerAbs<
		TConfiguration extends CoreConfigurationItf,
		TVariables extends CoreVariablesItf,
		TStatus extends CoreStatusItf<
			TConfiguration,
			TVariables
		>,
		TNodeRequest extends CoreNodeRequestItf<
			TConfiguration,
			TVariables,
			TStatus
		>,
		TSlide extends CoreSlideItf,
		TNodeResponse extends CoreNodeResponseItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide
		>,
		TNode extends CoreNodeItf<
			TConfiguration,
			TVariables,
			TStatus,
			TNodeRequest,
			TSlide,
			TNodeResponse
		>,
		TAppRequest extends CoreAppRequestItf<
			TConfiguration,
			TVariables,
			TStatus
		>,
		TAppResponse extends CoreAppResponseItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide
		>,

		TCliAppRunner extends CoreCliAppRunnerItf,
		TCliAppRequestBuilder extends CoreCliAppRequestBuilderItf<
			TConfiguration,
			TVariables,
			TStatus,
			TAppRequest
		>,
		TApp extends CoreAppItf<
			TConfiguration,
			TVariables,
			TStatus,
			TAppRequest,
			TSlide,
			TAppResponse
		>,
		TCliAppResponseDispatcher extends CoreCliAppResponseDispatcherItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide,
			TAppResponse
		>,
		TRouter extends CoreRouterItf<
			TConfiguration,
			TVariables,
			TStatus,
			TNodeRequest,
			TSlide,
			TNodeResponse,
			TNode
		>,
		TCliStylizer extends CoreCliStylizerItf,
		TCliQueryNormalizer extends CoreCliQueryNormalizerItf,
		TCliQueryNormalizerContainer extends CoreCliQueryNormalizerContainerItf<
			TCliQueryNormalizer
		>,
		TCliPrompter extends CoreCliPrompterItf,
		TCliQueryBuilder extends CoreCliQueryBuilderItf,
		TNodeRequestByAppRequestBuilder extends CoreNodeRequestByAppRequestBuilderItf<
			TConfiguration,
			TVariables,
			TStatus,
			TNodeRequest,
			TAppRequest
		>,
		TSystemNodeResponseBuilder extends CoreSystemNodeResponseBuilderItf<
			TConfiguration,
			TVariables,
			TStatus,
			TNodeRequest,
			TSlide,
			TNodeResponse
		>,
		TAppResponseByNodeResponseBuilder extends CoreAppResponseByNodeResponseBuilderItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide,
			TAppResponse,
			TNodeResponse
		>,
		TNextNodeRequestBuilder extends CoreNextNodeRequestBuilderItf<
			TConfiguration,
			TVariables,
			TStatus,
			TNodeRequest,
			TSlide,
			TNodeResponse
		>,
		TNodeRequestProcessor extends CoreNodeRequestProcessorItf<
			TConfiguration,
			TVariables,
			TStatus,
			TNodeRequest,
			TSlide,
			TNodeResponse
		>,
		TStartingStatusBuilder extends CoreStartingStatusBuilderItf<
			TConfiguration,
			TVariables,
			TStatus
		>,
		TNodeBuilder extends CoreNodeBuilderItf,
		TNodeBrowser extends CoreNodeBrowserItf<
			TConfiguration,
			TVariables,
			TStatus,
			TSlide,
			TNodeResponse
		>,
		TTranslation extends CoreTranslationItf,
		TTranslationContainer extends CoreTranslationContainerItf<
			TTranslation
		>
	>
	implements CoreServiceContainerItf<
		TConfiguration,
		TVariables,
		TStatus,
		TNodeRequest,
		TSlide,
		TNodeResponse,
		TNode,
		TAppRequest,
		TAppResponse,

		TCliAppRunner,
		TCliAppRequestBuilder,
		TApp,
		TCliAppResponseDispatcher,
		TRouter,
		TCliStylizer,
		TCliQueryNormalizer,
		TCliQueryNormalizerContainer,
		TCliPrompter,
		TCliQueryBuilder,
		TNodeRequestByAppRequestBuilder,
		TSystemNodeResponseBuilder,
		TAppResponseByNodeResponseBuilder,
		TNextNodeRequestBuilder,
		TNodeRequestProcessor,
		TStartingStatusBuilder,
		TNodeBuilder,
		TNodeBrowser,
		TTranslation,
		TTranslationContainer
	>
{

	private cliAppRunner:TCliAppRunner|undefined;

	public getCliAppRunner():TCliAppRunner
	{
		if (this.cliAppRunner === undefined)
		{
			this.cliAppRunner = this.buildCliAppRunner();
		}
		return this.cliAppRunner;
	}

		protected abstract buildCliAppRunner():TCliAppRunner;

	private cliAppRequestBuilder:TCliAppRequestBuilder|undefined;

	public getCliAppRequestBuilder():TCliAppRequestBuilder
	{
		if (this.cliAppRequestBuilder === undefined)
		{
			this.cliAppRequestBuilder = this.buildCliAppRequestBuilder();
		}
		return this.cliAppRequestBuilder;
	}

		protected abstract buildCliAppRequestBuilder():TCliAppRequestBuilder;

	private app:TApp|undefined;

	public getApp():TApp
	{
		if (this.app === undefined)
		{
			this.app = this.buildApp();
		}
		return this.app;
	}

		protected abstract buildApp():TApp;

	private cliAppResponseDispatcher:TCliAppResponseDispatcher|undefined;

	public getCliAppResponseDispatcher():TCliAppResponseDispatcher
	{
		if (this.cliAppResponseDispatcher === undefined)
		{
			this.cliAppResponseDispatcher = this.buildCliAppResponseDispatcher();
		}
		return this.cliAppResponseDispatcher;
	}

		protected abstract buildCliAppResponseDispatcher():TCliAppResponseDispatcher;

	private router:TRouter|undefined;

	public getRouter():TRouter
	{
		if (this.router === undefined)
		{
			this.router = this.buildRouter();
		}
		return this.router;
	}

		protected abstract buildRouter():TRouter;

	private cliStylizer:TCliStylizer|undefined;

	public getCliStylizer():TCliStylizer
	{
		if (this.cliStylizer === undefined)
		{
			this.cliStylizer = this.buildCliStylizer()
		}
		return this.cliStylizer;
	}

		protected abstract buildCliStylizer():TCliStylizer;

	private cliQueryNormalizerContainer:TCliQueryNormalizerContainer|undefined;

	public getCliQueryNormalizerContainer():TCliQueryNormalizerContainer
	{
		if (this.cliQueryNormalizerContainer === undefined)
		{
			this.cliQueryNormalizerContainer = this.buildCliQueryNormalizerContainer();
		}
		return this.cliQueryNormalizerContainer;
	}

		protected abstract buildCliQueryNormalizerContainer():TCliQueryNormalizerContainer;

	private cliPrompter:TCliPrompter|undefined;

	public getCliPrompter():TCliPrompter
	{
		if (this.cliPrompter === undefined)
		{
			this.cliPrompter = this.buildCliPrompter();
		}
		return this.cliPrompter;
	}

		protected abstract buildCliPrompter():TCliPrompter;

	private cliQueryBuilder:TCliQueryBuilder|undefined;

	public getCliQueryBuilder():TCliQueryBuilder
	{
		if (this.cliQueryBuilder === undefined)
		{
			this.cliQueryBuilder = this.buildCliQueryBuilder();
		}
		return this.cliQueryBuilder;
	}

		protected abstract buildCliQueryBuilder():TCliQueryBuilder;

	private nodeRequestByAppRequestBuilder:TNodeRequestByAppRequestBuilder|undefined;

	public getNodeRequestByAppRequestBuilder():TNodeRequestByAppRequestBuilder
	{
		if (this.nodeRequestByAppRequestBuilder === undefined)
		{
			this.nodeRequestByAppRequestBuilder = this.buildNodeRequestByAppRequestBuilder()
		}
		return this.nodeRequestByAppRequestBuilder;
	}

		protected abstract buildNodeRequestByAppRequestBuilder():TNodeRequestByAppRequestBuilder;

	private systemNodeResponseBuilder:TSystemNodeResponseBuilder|undefined;

	public getSystemNodeResponseBuilder():TSystemNodeResponseBuilder
	{
		if (this.systemNodeResponseBuilder === undefined)
		{
			this.systemNodeResponseBuilder = this.buildSystemNodeResponseBuilder();
		}
		return this.systemNodeResponseBuilder;
	}

		protected abstract buildSystemNodeResponseBuilder():TSystemNodeResponseBuilder;

	private appResponseByNodeResponseBuilder:TAppResponseByNodeResponseBuilder|undefined;

	public getAppResponseByNodeResponseBuilder():TAppResponseByNodeResponseBuilder
	{
		if (this.appResponseByNodeResponseBuilder === undefined)
		{
			this.appResponseByNodeResponseBuilder = this.buildAppResponseByNodeResponseBuilder();
		}
		return this.appResponseByNodeResponseBuilder;
	}

		protected abstract buildAppResponseByNodeResponseBuilder():TAppResponseByNodeResponseBuilder;

	private nextNodeRequestBuilder:TNextNodeRequestBuilder|undefined;

	public getNextNodeRequestBuilder():TNextNodeRequestBuilder
	{
		if (this.nextNodeRequestBuilder === undefined)
		{
			this.nextNodeRequestBuilder = this.buildNextNodeRequestBuilder();
		}
		return this.nextNodeRequestBuilder;
	}

		protected abstract buildNextNodeRequestBuilder():TNextNodeRequestBuilder;

	private nodeRequestProcessor:TNodeRequestProcessor|undefined;

	public getNodeRequestProcessor():TNodeRequestProcessor
	{
		if (this.nodeRequestProcessor === undefined)
		{
			this.nodeRequestProcessor = this.buildNodeRequestProcessor();
		}
		return this.nodeRequestProcessor;
	}

		protected abstract buildNodeRequestProcessor():TNodeRequestProcessor;

	private startingStatusBuilder:TStartingStatusBuilder|undefined;

	public getStartingStatusBuilder():TStartingStatusBuilder
	{
		if (this.startingStatusBuilder === undefined)
		{
			this.startingStatusBuilder = this.buildStartingStatusBuilder();
		}
		return this.startingStatusBuilder;
	}

		protected abstract buildStartingStatusBuilder():TStartingStatusBuilder;

	private nodeBuilder:TNodeBuilder|undefined;

	public getNodeBuilder():TNodeBuilder
	{
		if (this.nodeBuilder === undefined)
		{
			this.nodeBuilder = this.buildNodeBuilder();
		}
		return this.nodeBuilder;
	}

		protected abstract buildNodeBuilder():TNodeBuilder;

	private nodeBrowser:TNodeBrowser|undefined;

	public getNodeBrowser():TNodeBrowser
	{
		if (this.nodeBrowser === undefined)
		{
			this.nodeBrowser = this.buildNodeBrowser();
		}
		return this.nodeBrowser;
	}

		protected abstract buildNodeBrowser():TNodeBrowser;

	private translationContainer:TTranslationContainer|undefined;

	public getTranslationContainer():TTranslationContainer
	{
		if (this.translationContainer === undefined)
		{
			this.translationContainer = this.buildTranslationContainer();
		}
		return this.translationContainer;
	}

		protected abstract buildTranslationContainer():TTranslationContainer;

}