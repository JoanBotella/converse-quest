import { CoreSlideItf } from "./CoreSlideItf";

export
	abstract class CoreSlideAbs
	implements CoreSlideItf
{

	private title:string|undefined;
	private text:string|undefined;

	constructor()
	{
		this.title = undefined;
		this.text = undefined;
	}

	public hasTitle():boolean
	{
		return this.title !== undefined;
	}

	public getTitleAfterHas():string
	{
		return this.title as string;
	}

	public setTitle(title:string):void
	{
		this.title = title;
	}

	public unsetTitle():void
	{
		this.title = undefined;
	}

	public hasText():boolean
	{
		return this.text !== undefined;
	}

	public getTextAfterHas():string
	{
		return this.text as string;
	}

	public setText(text:string):void
	{
		this.text = text;
	}

	public unsetText():void
	{
		this.text = undefined;
	}

}