
export
	interface CoreSlideItf
{

	hasTitle: () => boolean;

	getTitleAfterHas: () => string;

	setTitle: (title:string) => void;

	unsetTitle: () => void;

	hasText: () => boolean;

	getTextAfterHas: () => string;

	setText: (text:string) => void;

	unsetText: () => void;

}