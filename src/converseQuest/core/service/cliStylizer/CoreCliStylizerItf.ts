
export
	interface CoreCliStylizerItf
{

	stylizeText: (text:string) => string;

	stylizeTitle: (text:string) => string;

	stylizeSlidePrompt: (text:string) => string;

	stylizeQueryPrompt: (text:string) => string;

}