
export
	class CoreCliStylizer
{

	// https://gist.github.com/iamnewton/8754917

	// Regular colors

	static readonly FG_BLACK = '\x1b[0;30m'
	static readonly FG_RED = '\x1b[0;31m'
	static readonly FG_GREEN = '\x1b[0;32m'
	static readonly FG_YELLOW = '\x1b[0;33m'
	static readonly FG_BLUE = '\x1b[0;34m'
	static readonly FG_MAGENTA = '\x1b[0;35m'
	static readonly FG_CYAN = '\x1b[0;36m'
	static readonly FG_WHITE = '\x1b[0;37m'

	// Bold

	static readonly BOLD_BLACK = '\x1b[1;30m'
	static readonly BOLD_RED = '\x1b[1;31m'
	static readonly BOLD_GREEN = '\x1b[1;32m'
	static readonly BOLD_YELLOW = '\x1b[1;33m'
	static readonly BOLD_BLUE = '\x1b[1;34m'
	static readonly BOLD_MAGENTA = '\x1b[1;35m'
	static readonly BOLD_CYAN = '\x1b[1;36m'
	static readonly BOLD_WHITE = '\x1b[1;37m'

	// Underline

	static readonly UNDERLINE_BLACK = '\x1b[4;30m'
	static readonly UNDERLINE_RED = '\x1b[4;31m'
	static readonly UNDERLINE_GREEN = '\x1b[4;32m'
	static readonly UNDERLINE_YELLOW = '\x1b[4;33m'
	static readonly UNDERLINE_BLUE = '\x1b[4;34m'
	static readonly UNDERLINE_MAGENTA = '\x1b[4;35m'
	static readonly UNDERLINE_CYAN = '\x1b[4;36m'
	static readonly UNDERLINE_WHITE = '\x1b[4;37m'

	// Background

	static readonly BG_BLACK = '\x1b[40m'
	static readonly BG_RED = '\x1b[41m'
	static readonly BG_GREEN = '\x1b[42m'
	static readonly BG_YELLOW = '\x1b[43m'
	static readonly BG_BLUE = '\x1b[44m'
	static readonly BG_MAGENTA = '\x1b[45m'
	static readonly BG_CYAN = '\x1b[46m'
	static readonly BG_WHITE = '\x1b[47m'

	// High intensity

	static readonly HI_FG_BLACK = '\x1b[0;90m'
	static readonly HI_FG_RED = '\x1b[0;91m'
	static readonly HI_FG_GREEN = '\x1b[0;92m'
	static readonly HI_FG_YELLOW = '\x1b[0;93m'
	static readonly HI_FG_BLUE = '\x1b[0;94m'
	static readonly HI_FG_MAGENTA = '\x1b[0;95m'
	static readonly HI_FG_CYAN = '\x1b[0;96m'
	static readonly HI_FG_WHITE = '\x1b[0;97m'

	// Bold high intensity

	static readonly HI_BOLD_BLACK = '\x1b[1;90m'
	static readonly HI_BOLD_RED = '\x1b[1;91m'
	static readonly HI_BOLD_GREEN = '\x1b[1;92m'
	static readonly HI_BOLD_YELLOW = '\x1b[1;93m'
	static readonly HI_BOLD_BLUE = '\x1b[1;94m'
	static readonly HI_BOLD_MAGENTA = '\x1b[1;95m'
	static readonly HI_BOLD_CYAN = '\x1b[1;96m'
	static readonly HI_BOLD_WHITE = '\x1b[1;97m'

	// Bold high intensity background

	static readonly HI_BG_BLACK = '\x1b[0,100m'
	static readonly HI_BG_RED = '\x1b[0,101m'
	static readonly HI_BG_GREEN = '\x1b[0,102m'
	static readonly HI_BG_YELLOW = '\x1b[0,103m'
	static readonly HI_BG_BLUE = '\x1b[0,104m'
	static readonly HI_BG_MAGENTA = '\x1b[0,105m'
	static readonly HI_BG_CYAN = '\x1b[0,106m'
	static readonly HI_BG_WHITE = '\x1b[0,107m'

	// Styles

	static readonly STYLE_RESET = '\x1b[0m'
	static readonly STYLE_BRIGHT = '\x1b[1m'
	static readonly STYLE_DIM = '\x1b[2m'
	static readonly STYLE_ITALIC = '\x1b[3m'
	static readonly STYLE_UNDERSCORE = '\x1b[4m'
	static readonly STYLE_BLINK = '\x1b[5m'
	static readonly STYLE_REVERSE = '\x1b[7m'
	static readonly STYLE_HIDDEN = '\x1b[8m'
	static readonly STYLE_STRIKETHROUG = '\x1b[9m'

	public stylizeText(text:string):string
	{
		text = text.replace(/<br \/>/g, '\n');

		text = text.replace(/<p>/g, '\n');
		text = text.replace(/<p class="talking">/g, '\n' + CoreCliStylizer.STYLE_ITALIC);
		text = text.replace(/<\/p>/g, '\n' + CoreCliStylizer.STYLE_RESET);

		text = text.replace(/<span class="character">/g, CoreCliStylizer.FG_GREEN);
		text = text.replace(/<span class="verb">/g, CoreCliStylizer.FG_YELLOW);
		text = text.replace(/<span class="item">/g, CoreCliStylizer.FG_BLUE);
		text = text.replace(/<span class="exit">/g, CoreCliStylizer.FG_RED);

		text = text.replace(/<\/span>/g, CoreCliStylizer.STYLE_RESET);

		return text;
	}

	public stylizeTitle(text:string):string
	{
		return '\n' + CoreCliStylizer.STYLE_BRIGHT + text + CoreCliStylizer.STYLE_RESET;
	}

	public stylizeSlidePrompt(text:string):string
	{
		return CoreCliStylizer.HI_FG_BLACK + text + CoreCliStylizer.STYLE_RESET;
	}

	public stylizeQueryPrompt(text:string):string
	{
		return CoreCliStylizer.HI_FG_BLACK + text + CoreCliStylizer.STYLE_RESET;
	}

}