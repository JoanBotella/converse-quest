import { DemoServiceContainer } from "./demo/library/serviceContainer/DemoServiceContainer";
import { DemoCliAppRunnerItf } from "./demo/service/cliAppRunner/DemoCliAppRunnerItf";

let serviceContainer:DemoServiceContainer = new DemoServiceContainer();
let cliAppRunner:DemoCliAppRunnerItf = serviceContainer.getCliAppRunner();
cliAppRunner.run();