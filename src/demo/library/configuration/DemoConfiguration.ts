import { CoreConfigurationAbs } from "../../../converseQuest/core/library/configuration/CoreConfigurationAbs";
import { DemoConfigurationItf } from "./DemoConfigurationItf";

export
	class DemoConfiguration
	extends CoreConfigurationAbs
	implements DemoConfigurationItf
{

}