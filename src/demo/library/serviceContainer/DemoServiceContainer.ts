import { DemoCliAppRunnerItf } from "../../service/cliAppRunner/DemoCliAppRunnerItf";
import { DemoCliAppRunner } from "../../service/cliAppRunner/DemoCliAppRunner";
import { DemoApp } from "../../service/app/DemoApp";
import { DemoAppItf } from "../../service/app/DemoAppItf";
import { DemoCliAppRequestBuilderItf } from "../../service/cliAppRequestBuilder/DemoCliAppRequestBuilderItf";
import { DemoCliAppRequestBuilder } from "../../service/cliAppRequestBuilder/DemoCliAppRequestBuilder";
import { DemoCliAppResponseDispatcherItf } from "../../service/cliAppResponseDispatcher/DemoCliAppResponseDispatcherItf";
import { DemoCliAppResponseDispatcher } from "../../service/cliAppResponseDispatcher/DemoCliAppResponseDispatcher";
import { DemoRouterItf } from "../../service/router/DemoRouterItf";
import { DemoRouter } from "../../service/router/DemoRouter";
import { DemoCliPrompterItf } from "../../service/cliPrompter/DemoCliPrompterItf";
import { DemoCliPrompter } from "../../service/cliPrompter/DemoCliPrompter";
import { DemoCliQueryBuilderItf } from "../../service/cliQueryBuilder/DemoCliQueryBuilderItf";
import { DemoCliQueryBuilder } from "../../service/cliQueryBuilder/DemoCliQueryBuilder";
import { DemoNodeRequestByAppRequestBuilder } from "../../service/nodeRequestByAppRequestBuilder/DemoNodeRequestByAppRequestBuilder";
import { DemoNodeRequestByAppRequestBuilderItf } from "../../service/nodeRequestByAppRequestBuilder/DemoNodeRequestByAppRequestBuilderItf";
import { DemoSystemNodeResponseBuilderItf } from "../../service/systemNodeResponseBuilder/DemoSystemNodeResponseBuilderItf";
import { DemoSystemNodeResponseBuilder } from "../../service/systemNodeResponseBuilder/DemoSystemNodeResponseBuilder";
import { DemoAppResponseByNodeResponseBuilderItf } from "../../service/appResponseByNodeResponseBuilder/DemoAppResponseByNodeResponseItf";
import { DemoAppResponseByNodeResponseBuilder } from "../../service/appResponseByNodeResponseBuilder/DemoAppResponseByNodeResponseBuilder";
import { DemoNextNodeRequestBuilderItf } from "../../service/nextNodeRequestBuilder/DemoNextNodeRequestBuilderItf";
import { DemoNextNodeRequestBuilder } from "../../service/nextNodeRequestBuilder/DemoNextNodeRequestBuilder";
import { DemoNodeRequestProcessorItf } from "../../service/nodeRequestProcessor/DemoNodeRequestProcessorItf";
import { DemoNodeRequestProcessor } from "../../service/nodeRequestProcessor/DemoNodeRequestProcessor";
import { DemoStartingStatusBuilderItf } from "../../service/startingStatusBuilder/DemoStartingStatusBuilderItf";
import { DemoStartingStatusBuilder } from "../../service/startingStatusBuilder/DemoStartingStatusBuilder";
import { DemoServiceContainerItf } from "./DemoServiceContainerItf";
import { DemoNodeBuilderItf } from "../../service/nodeBuilder/DemoNodeBuilderItf";
import { DemoNodeBuilder } from "../../service/nodeBuilder/DemoNodeBuilder";
import { CoreCliStylizerItf } from "../../../converseQuest/core/service/cliStylizer/CoreCliStylizerItf";
import { CoreCliStylizer } from "../../../converseQuest/core/service/cliStylizer/CoreCliStylizer";
import { CoreServiceContainerAbs } from "../../../converseQuest/core/library/serviceContainer/CoreServiceContainerAbs";
import { DemoStatusItf } from "../status/DemoStatusItf";
import { DemoNodeRequestItf } from "../nodeRequest/DemoNodeRequestItf";
import { DemoSlideItf } from "../slide/DemoSlideItf";
import { DemoNodeResponseItf } from "../nodeResponse/DemoNodeResponseItf";
import { DemoNodeItf } from "../node/DemoNodeItf";
import { DemoAppRequestItf } from "../appRequest/DemoAppRequestItf";
import { DemoAppResponseItf } from "../appResponse/DemoAppResponseItf";
import { DemoNodeBrowserItf } from "../../service/nodeBrowser/DemoNodeBrowserItf";
import { DemoNodeBrowser } from "../../service/nodeBrowser/DemoNodeBrowser";
import { DemoConfigurationItf } from "../configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../variables/DemoVariablesItf";
import { DemoTranslationItf } from "../translation/DemoTranslationItf";
import { DemoTranslationContainerItf } from "../../service/translationContainer/DemoTranslationContainerItf";
import { DemoTranslationContainer } from "../../service/translationContainer/DemoTranslationContainer";
import { DemoCliQueryNormalizerItf } from "../cliQueryNormalizer/DemoCliQueryNormalizerItf";
import { DemoCliQueryNormalizerContainer } from "../../service/cliQueryNormalizerContainer/DemoCliQueryNormalizerContainer";
import { DemoCliQueryNormalizerContainerItf } from "../../service/cliQueryNormalizerContainer/DemoCliQueryNormalizerContainerItf";

export
	class DemoServiceContainer
	extends CoreServiceContainerAbs<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoNodeRequestItf,
		DemoSlideItf,
		DemoNodeResponseItf,
		DemoNodeItf,
		DemoAppRequestItf,
		DemoAppResponseItf,

		DemoCliAppRunnerItf,
		DemoCliAppRequestBuilderItf,
		DemoAppItf,
		DemoCliAppResponseDispatcherItf,
		DemoRouterItf,
		CoreCliStylizerItf,
		DemoCliQueryNormalizerItf,
		DemoCliQueryNormalizerContainerItf,
		DemoCliPrompterItf,
		DemoCliQueryBuilderItf,
		DemoNodeRequestByAppRequestBuilderItf,
		DemoSystemNodeResponseBuilderItf,
		DemoAppResponseByNodeResponseBuilderItf,
		DemoNextNodeRequestBuilderItf,
		DemoNodeRequestProcessorItf,
		DemoStartingStatusBuilderItf,
		DemoNodeBuilderItf,
		DemoNodeBrowserItf,
		DemoTranslationItf,
		DemoTranslationContainerItf
	>
	implements DemoServiceContainerItf
{

	protected buildCliAppRunner():DemoCliAppRunnerItf
	{
		return new DemoCliAppRunner(
			this.getStartingStatusBuilder(),
			this.getCliAppRequestBuilder(),
			this.getApp(),
			this.getCliAppResponseDispatcher()
		);
	}

	protected buildCliAppRequestBuilder():DemoCliAppRequestBuilderItf
	{
		return new DemoCliAppRequestBuilder(
			this.getCliQueryBuilder()
		);
	}

	protected buildApp():DemoAppItf
	{
		return new DemoApp(
			this.getNodeRequestByAppRequestBuilder(),
			this.getAppResponseByNodeResponseBuilder(),
			this.getNextNodeRequestBuilder(),
			this.getNodeRequestProcessor()
		);
	}

	protected buildCliAppResponseDispatcher():DemoCliAppResponseDispatcherItf
	{
		return new DemoCliAppResponseDispatcher(
			this.getCliStylizer(),
			this.getCliPrompter(),
			this.getTranslationContainer()
		);
	}

	protected buildRouter():DemoRouterItf
	{
		return new DemoRouter(
			this.getNodeBuilder()
		);
	}

	protected buildCliStylizer():CoreCliStylizerItf
	{
		return new CoreCliStylizer();
	}

	protected buildCliQueryNormalizerContainer():DemoCliQueryNormalizerContainerItf
	{
		return new DemoCliQueryNormalizerContainer();
	}

	protected buildCliPrompter():DemoCliPrompterItf
	{
		return new DemoCliPrompter(
			this.getCliStylizer()
		);
	}

	protected buildCliQueryBuilder():DemoCliQueryBuilderItf
	{
		return new DemoCliQueryBuilder(
			this.getCliPrompter(),
			this.getCliQueryNormalizerContainer(),
			this.getTranslationContainer()
		);
	}

	protected buildNodeRequestByAppRequestBuilder():DemoNodeRequestByAppRequestBuilderItf
	{
		return new DemoNodeRequestByAppRequestBuilder();
	}

	protected buildSystemNodeResponseBuilder():DemoSystemNodeResponseBuilderItf
	{
		return new DemoSystemNodeResponseBuilder(
			this.getNodeBrowser(),
			this.getTranslationContainer()
		);
	}

	protected buildAppResponseByNodeResponseBuilder():DemoAppResponseByNodeResponseBuilderItf
	{
		return new DemoAppResponseByNodeResponseBuilder();
	}

	protected buildNextNodeRequestBuilder():DemoNextNodeRequestBuilderItf
	{
		return new DemoNextNodeRequestBuilder();
	}

	protected buildNodeRequestProcessor():DemoNodeRequestProcessorItf
	{
		return new DemoNodeRequestProcessor(
			this.getSystemNodeResponseBuilder(),
			this.getRouter()
		);
	}

	protected buildStartingStatusBuilder():DemoStartingStatusBuilderItf
	{
		return new DemoStartingStatusBuilder();
	}

	protected buildNodeBuilder():DemoNodeBuilderItf
	{
		return new DemoNodeBuilder(
			this
		);
	}

	protected buildNodeBrowser():DemoNodeBrowserItf
	{
		return new DemoNodeBrowser();
	}

	protected buildTranslationContainer():DemoTranslationContainerItf
	{
		return new DemoTranslationContainer();
	}

}