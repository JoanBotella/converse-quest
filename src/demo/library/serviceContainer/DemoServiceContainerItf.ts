import { DemoCliAppRunnerItf } from "../../service/cliAppRunner/DemoCliAppRunnerItf";
import { DemoAppItf } from "../../service/app/DemoAppItf";
import { DemoCliAppRequestBuilderItf } from "../../service/cliAppRequestBuilder/DemoCliAppRequestBuilderItf";
import { DemoCliAppResponseDispatcherItf } from "../../service/cliAppResponseDispatcher/DemoCliAppResponseDispatcherItf";
import { DemoRouterItf } from "../../service/router/DemoRouterItf";
import { DemoCliPrompterItf } from "../../service/cliPrompter/DemoCliPrompterItf";
import { DemoCliQueryBuilderItf } from "../../service/cliQueryBuilder/DemoCliQueryBuilderItf";
import { DemoNodeRequestByAppRequestBuilderItf } from "../../service/nodeRequestByAppRequestBuilder/DemoNodeRequestByAppRequestBuilderItf";
import { DemoSystemNodeResponseBuilderItf } from "../../service/systemNodeResponseBuilder/DemoSystemNodeResponseBuilderItf";
import { DemoAppResponseByNodeResponseBuilderItf } from "../../service/appResponseByNodeResponseBuilder/DemoAppResponseByNodeResponseItf";
import { DemoNextNodeRequestBuilderItf } from "../../service/nextNodeRequestBuilder/DemoNextNodeRequestBuilderItf";
import { DemoNodeRequestProcessorItf } from "../../service/nodeRequestProcessor/DemoNodeRequestProcessorItf";
import { DemoStartingStatusBuilderItf } from "../../service/startingStatusBuilder/DemoStartingStatusBuilderItf";
import { DemoNodeBuilderItf } from "../../service/nodeBuilder/DemoNodeBuilderItf";
import { CoreCliStylizerItf } from "../../../converseQuest/core/service/cliStylizer/CoreCliStylizerItf";
import { CoreServiceContainerItf } from "../../../converseQuest/core/library/serviceContainer/CoreServiceContainerItf";
import { DemoStatusItf } from "../status/DemoStatusItf";
import { DemoNodeRequestItf } from "../nodeRequest/DemoNodeRequestItf";
import { DemoSlideItf } from "../slide/DemoSlideItf";
import { DemoNodeResponseItf } from "../nodeResponse/DemoNodeResponseItf";
import { DemoNodeItf } from "../node/DemoNodeItf";
import { DemoAppRequestItf } from "../appRequest/DemoAppRequestItf";
import { DemoAppResponseItf } from "../appResponse/DemoAppResponseItf";
import { DemoNodeBrowserItf } from "../../service/nodeBrowser/DemoNodeBrowserItf";
import { DemoConfigurationItf } from "../configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../variables/DemoVariablesItf";
import { DemoTranslationItf } from "../translation/DemoTranslationItf";
import { DemoTranslationContainerItf } from "../../service/translationContainer/DemoTranslationContainerItf";
import { DemoCliQueryNormalizerItf } from "../cliQueryNormalizer/DemoCliQueryNormalizerItf";
import { DemoCliQueryNormalizerContainerItf } from "../../service/cliQueryNormalizerContainer/DemoCliQueryNormalizerContainerItf";

export
	interface DemoServiceContainerItf
	extends CoreServiceContainerItf<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoNodeRequestItf,
		DemoSlideItf,
		DemoNodeResponseItf,
		DemoNodeItf,
		DemoAppRequestItf,
		DemoAppResponseItf,

		DemoCliAppRunnerItf,
		DemoCliAppRequestBuilderItf,
		DemoAppItf,
		DemoCliAppResponseDispatcherItf,
		DemoRouterItf,
		CoreCliStylizerItf,
		DemoCliQueryNormalizerItf,
		DemoCliQueryNormalizerContainerItf,
		DemoCliPrompterItf,
		DemoCliQueryBuilderItf,
		DemoNodeRequestByAppRequestBuilderItf,
		DemoSystemNodeResponseBuilderItf,
		DemoAppResponseByNodeResponseBuilderItf,
		DemoNextNodeRequestBuilderItf,
		DemoNodeRequestProcessorItf,
		DemoStartingStatusBuilderItf,
		DemoNodeBuilderItf,
		DemoNodeBrowserItf,
		DemoTranslationItf,
		DemoTranslationContainerItf
	>
{

}