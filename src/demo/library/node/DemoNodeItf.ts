import { CoreNodeItf } from "../../../converseQuest/core/library/node/CoreNodeItf";
import { DemoStatusItf } from "../status/DemoStatusItf";
import { DemoNodeRequestItf } from "../nodeRequest/DemoNodeRequestItf";
import { DemoSlideItf } from "../slide/DemoSlideItf";
import { DemoNodeResponseItf } from "../nodeResponse/DemoNodeResponseItf";
import { DemoVariablesItf } from "../variables/DemoVariablesItf";
import { DemoConfigurationItf } from "../configuration/DemoConfigurationItf";

export
	interface DemoNodeItf
	extends CoreNodeItf<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoNodeRequestItf,
		DemoSlideItf,
		DemoNodeResponseItf
	>
{

}