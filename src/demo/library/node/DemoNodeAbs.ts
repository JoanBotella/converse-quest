import { CoreNodeAbs } from "../../../converseQuest/core/library/node/CoreNodeAbs";
import { DemoStatusItf } from "../status/DemoStatusItf";
import { DemoNodeRequestItf } from "../nodeRequest/DemoNodeRequestItf";
import { DemoSlideItf } from "../slide/DemoSlideItf";
import { DemoNodeResponseItf } from "../nodeResponse/DemoNodeResponseItf";
import { DemoNodeItf } from "./DemoNodeItf";
import { DemoVariablesItf } from "../variables/DemoVariablesItf";
import { DemoNodeResponse } from "../nodeResponse/DemoNodeResponse";
import { DemoNodeBrowserItf } from "../../service/nodeBrowser/DemoNodeBrowserItf";
import { DemoConfigurationItf } from "../configuration/DemoConfigurationItf";
import { DemoTranslationItf } from "../translation/DemoTranslationItf";
import { DemoTranslationContainerItf } from "../../service/translationContainer/DemoTranslationContainerItf";

export
	abstract class DemoNodeAbs
	extends CoreNodeAbs<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoNodeRequestItf,
		DemoSlideItf,
		DemoNodeResponseItf,
		DemoNodeBrowserItf,
		DemoTranslationItf,
		DemoTranslationContainerItf
	>
	implements DemoNodeItf
{

	protected buildNodeResponse(
		configuration:DemoConfigurationItf,
		variables:DemoVariablesItf,
		status:DemoStatusItf,
		slides:DemoSlideItf[]
	):DemoNodeResponseItf
	{
		status.setConfiguration(configuration);
		status.setVariables(variables);

		let nodeResponse:DemoNodeResponseItf = new DemoNodeResponse(status);

		nodeResponse.setSlides(slides);

		return nodeResponse;
	}

}