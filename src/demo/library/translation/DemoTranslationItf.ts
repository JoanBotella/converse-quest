import { CoreTranslationItf } from "../../../converseQuest/core/library/translation/CoreTranslationItf";

export
	interface DemoTranslationItf
	extends CoreTranslationItf
{

	node_bedroom_query_goCorridor: () => string;
	node_bedroom_query_look: () => string;
	node_bedroom_query_lookKeys: () => string;
	node_bedroom_query_takeKeys: () => string;
	node_bedroom_text_lookKeys: () => string;
	node_bedroom_text_look_1: () => string;
	node_bedroom_text_look_2: () => string;
	node_bedroom_text_look_3: () => string;
	node_bedroom_text_takeKeys: () => string;
	node_bedroom_title: () => string;
	node_configurationMenu_query_1: () => string;
	node_configurationMenu_query_2: () => string;
	node_configurationMenu_text_look: () => string;
	node_configurationMenu_title: () => string;
	node_corridor_query_goBedroom: () => string;
	node_corridor_query_goLivingRoom: () => string;
	node_corridor_query_look: () => string;
	node_corridor_text_look: () => string;
	node_corridor_title: () => string;
	node_languageMenu_query_1: () => string;
	node_languageMenu_query_2: () => string;
	node_languageMenu_text_look: () => string;
	node_languageMenu_title: () => string;
	node_livingRoom_query_goCorridor: () => string;
	node_livingRoom_query_look: () => string;
	node_livingRoom_query_lookJoan: () => string;
	node_livingRoom_query_talkJoan: () => string;
	node_livingRoom_text_look: () => string;
	node_livingRoom_text_lookJoan: () => string;
	node_livingRoom_title: () => string;
	node_talkJoan_query_1: () => string;
	node_talkJoan_text_1: () => string;
	node_talkJoan_text_look: () => string;
	node_titleMenu_query_1: () => string;
	node_titleMenu_query_2: () => string;
	node_titleMenu_query_3: () => string;
	node_titleMenu_text_1: () => string;
	node_titleMenu_text_3: () => string;
	node_titleMenu_text_look: () => string;
	node_titleMenu_title: () => string;
	systemNodeResponseBuilder_query_configuration: () => string;
	systemNodeResponseBuilder_query_exit: () => string;
	systemNodeResponseBuilder_query_inventory: () => string;
	systemNodeResponseBuilder_query_lookKeys: () => string;
	systemNodeResponseBuilder_text_exit: () => string;
	systemNodeResponseBuilder_text_inventory_empty: () => string;
	systemNodeResponseBuilder_text_inventory_keys: () => string;
	systemNodeResponseBuilder_text_lookKeys: () => string;
	systemNodeResponseBuilder_text_notUnderstood: () => string;

}