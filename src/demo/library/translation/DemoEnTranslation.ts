import { CoreTranslationAbs } from "../../../converseQuest/core/library/translation/CoreTranslationAbs";
import { DemoTranslationItf } from "./DemoTranslationItf";

export
	class DemoEnTranslation
	extends CoreTranslationAbs
	implements DemoTranslationItf
{

	static readonly CODE:string = 'en';

	public node_bedroom_query_goCorridor():string { return 'go corridor'; }
	public node_bedroom_query_look():string { return 'look'; }
	public node_bedroom_query_lookKeys():string { return 'look keys'; }
	public node_bedroom_query_takeKeys():string { return 'take keys'; }
	public node_bedroom_text_lookKeys():string { return '<p>A lot of <span class="item">keys</span> joined by a keychain. I have there the key of the house and the one for the motorbike.</p>'; }
	public node_bedroom_text_look_1():string { return '<p>This is my bedroom.'; }
	public node_bedroom_text_look_2():string { return ' My <span class="item">keys</span> are on the desktop.'; }
	public node_bedroom_text_look_3():string { return '</p><p>I can exit to the <span class="exit">corridor</span>.</p>'; }
	public node_bedroom_text_takeKeys():string { return '<p>Taken.</p>'; }
	public node_bedroom_title():string { return 'Bedroom'; }
	public node_configurationMenu_query_1():string { return '1'; }
	public node_configurationMenu_query_2():string { return '2'; }
	public node_configurationMenu_text_look():string { return '<p>Choose an option:</p><p><span class="verb">1</span>) Language</p><p><span class="verb">2</span>) Back</p>'; }
	public node_configurationMenu_title():string { return 'Configuration Menu'; }
	public node_corridor_query_goBedroom():string { return 'go bedroom' }
	public node_corridor_query_goLivingRoom():string { return 'go living room' }
	public node_corridor_query_look():string { return 'look' }
	public node_corridor_text_look():string { return '<p>This is the corridor.</p><p>I can go back to my <span class="exit">bedroom</span> or enter the <span class="exit">living room</span>.</p>'; }
	public node_corridor_title():string { return 'Corridor'; }
	public node_languageMenu_query_1():string { return '1'; }
	public node_languageMenu_query_2():string { return '2'; }
	public node_languageMenu_text_look():string { return '<p>Choose an option:</p><p><span class="verb">1</span>) English</p><p><span class="verb">2</span>) Español</p>'; }
	public node_languageMenu_title():string { return 'Language Menu'; }
	public node_livingRoom_query_goCorridor():string { return 'go corridor'; }
	public node_livingRoom_query_look():string { return 'look'; }
	public node_livingRoom_query_lookJoan():string { return 'look joan'; }
	public node_livingRoom_query_talkJoan():string { return 'talk joan'; }
	public node_livingRoom_text_look():string { return '<p>This is the living room. My friend <span class="character">Joan</span> is on the sofa.</p><p>I can go back to the <span class="exit">corridor</span>.</p>'; }
	public node_livingRoom_text_lookJoan():string { return '<p>He\'s reading something.</p>'; }
	public node_livingRoom_title():string { return 'Living Room'; }
	public node_talkJoan_query_1():string { return '1'; }
	public node_talkJoan_text_1():string { return '<p class="talking">Ok</p>'; }
	public node_talkJoan_text_look():string { return '<p class="talking">Hello buddy. Need something?</p><p><span class="verb">1</span>) No, thanks.</p>'; }
	public node_titleMenu_query_1():string { return '1'; }
	public node_titleMenu_query_2():string { return '2'; }
	public node_titleMenu_query_3():string { return '3'; }
	public node_titleMenu_text_1():string { return '<p>Get ready!</p>'; }
	public node_titleMenu_text_3():string { return '<p>Leaving already?</p>'; }
	public node_titleMenu_text_look():string { return '<p>Choose an option:</p><p><span class="verb">1</span>) Start game</p><p><span class="verb">2</span>) Configuration</p><p><span class="verb">3</span>) Quit</p>'; }
	public node_titleMenu_title():string { return 'Title Menu'; }
	public systemNodeResponseBuilder_query_configuration():string { return 'c,configuration'; }
	public systemNodeResponseBuilder_query_exit():string { return 'quit,exit'; }
	public systemNodeResponseBuilder_query_inventory():string { return 'i,inventory'; }
	public systemNodeResponseBuilder_query_lookKeys():string { return 'look keys'; }
	public systemNodeResponseBuilder_text_exit():string { return '<p>Bye!</p>'; }
	public systemNodeResponseBuilder_text_inventory_empty():string { return '<p>I\'m empty-handed.</p>'; }
	public systemNodeResponseBuilder_text_inventory_keys():string { return '<p>I have my <span class="item">keys</span> with me.</p>'; }
	public systemNodeResponseBuilder_text_lookKeys():string { return '<p>A lot of keys joined by a keychain. I have there the key of the house and the one for the motorbike.</p>'; }
	public systemNodeResponseBuilder_text_notUnderstood():string { return '<p>What?</p>'; }

}