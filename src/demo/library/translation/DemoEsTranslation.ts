import { CoreTranslationAbs } from "../../../converseQuest/core/library/translation/CoreTranslationAbs";
import { DemoTranslationItf } from "./DemoTranslationItf";

export
	class DemoEsTranslation
	extends CoreTranslationAbs
	implements DemoTranslationItf
{

	static readonly CODE:string = 'es';

	public node_bedroom_query_goCorridor():string { return 'ir pasillo'; }
	public node_bedroom_query_look():string { return 'mirar'; }
	public node_bedroom_query_lookKeys():string { return 'mirar llaves'; }
	public node_bedroom_query_takeKeys():string { return 'coger llaves'; }
	public node_bedroom_text_lookKeys():string { return '<p>Muchas <span class="item">llaves</span> en un llavero. Tengo ahí la llave de la casa y la de la moto.</p>'; }
	public node_bedroom_text_look_1():string { return '<p>Ésta es mi habitación.'; }
	public node_bedroom_text_look_2():string { return ' Mis <span class="item">llaves</span> están sobre el escritorio.'; }
	public node_bedroom_text_look_3():string { return '</p><p>Puedo salir al <span class="exit">pasillo</span>.</p>'; }
	public node_bedroom_text_takeKeys():string { return '<p>Cogidas.</p>'; }
	public node_bedroom_title():string { return 'Dormitorio'; }
	public node_configurationMenu_query_1():string { return '1'; }
	public node_configurationMenu_query_2():string { return '2'; }
	public node_configurationMenu_text_look():string { return '<p>Elige una opción:</p><p><span class="verb">1</span>) Idioma</p><p><span class="verb">2</span>) Atrás</p>'; }
	public node_configurationMenu_title():string { return 'Menú de Configuración'; }
	public node_corridor_query_goBedroom():string { return 'ir dormitorio' }
	public node_corridor_query_goLivingRoom():string { return 'ir salon' }
	public node_corridor_query_look():string { return 'mirar' }
	public node_corridor_text_look():string { return '<p>Éste es el pasillo.</p><p>Puedo volver a mi <span class="exit">dormitorio</span> o entrar en el <span class="exit">salón</span>.</p>'; }
	public node_corridor_title():string { return 'Pasillo'; }
	public node_languageMenu_query_1():string { return '1'; }
	public node_languageMenu_query_2():string { return '2'; }
	public node_languageMenu_text_look():string { return '<p>Elige una opción:</p><p><span class="verb">1</span>) English</p><p><span class="verb">2</span>) Español</p>'; }
	public node_languageMenu_title():string { return 'Menú de Idioma'; }
	public node_livingRoom_query_goCorridor():string { return 'ir pasillo'; }
	public node_livingRoom_query_look():string { return 'mirar'; }
	public node_livingRoom_query_lookJoan():string { return 'mirar joan'; }
	public node_livingRoom_query_talkJoan():string { return 'hablar joan'; }
	public node_livingRoom_text_look():string { return '<p>Éste es el salón. Mi amigo <span class="character">Joan</span> está en el sofá.</p><p>Puedo volver al <span class="exit">pasillo</span>.</p>'; }
	public node_livingRoom_text_lookJoan():string { return '<p>Está leyendo algo.</p>'; }
	public node_livingRoom_title():string { return 'Salón'; }
	public node_talkJoan_query_1():string { return '1'; }
	public node_talkJoan_text_1():string { return '<p class="talking">Ok</p>'; }
	public node_talkJoan_text_look():string { return '<p class="talking">Hola compi. ¿Necesitas algo?</p><p><span class="verb">1</span>) No, gracias.</p>'; }
	public node_titleMenu_query_1():string { return '1'; }
	public node_titleMenu_query_2():string { return '2'; }
	public node_titleMenu_query_3():string { return '3'; }
	public node_titleMenu_text_1():string { return '<p>¡Prepárate!</p>'; }
	public node_titleMenu_text_3():string { return '<p>¿Ya te vas?</p>'; }
	public node_titleMenu_text_look():string { return '<p>Elige una opción:</p><p><span class="verb">1</span>) Empezar el juego</p><p><span class="verb">2</span>) Configuración</p><p><span class="verb">3</span>) Salir</p>'; }
	public node_titleMenu_title():string { return 'Menú de Título'; }
	public systemNodeResponseBuilder_query_configuration():string { return 'c,configuracion'; }
	public systemNodeResponseBuilder_query_exit():string { return 'salir'; }
	public systemNodeResponseBuilder_query_inventory():string { return 'i,inventario'; }
	public systemNodeResponseBuilder_query_lookKeys():string { return 'mirar llaves'; }
	public systemNodeResponseBuilder_text_exit():string { return '<p>¡Adiós!</p>'; }
	public systemNodeResponseBuilder_text_inventory_empty():string { return '<p>Tengo las manos vacías.</p>'; }
	public systemNodeResponseBuilder_text_inventory_keys():string { return '<p>Tengo mis <span class="item">llaves</span> conmigo.</p>'; }
	public systemNodeResponseBuilder_text_lookKeys():string { return '<p>Muchas llaves unidas por un llavero. Ahí tengo la llave de la casa y de la moto.</p>'; }
	public systemNodeResponseBuilder_text_notUnderstood():string { return '<p>¿Qué?</p>'; }

}