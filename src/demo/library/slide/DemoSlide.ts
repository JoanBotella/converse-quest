import { CoreSlideAbs } from "../../../converseQuest/core/library/slide/CoreSlideAbs";
import { DemoSlideItf } from "./DemoSlideItf";

export
	class DemoSlide
	extends CoreSlideAbs
	implements DemoSlideItf
{
}