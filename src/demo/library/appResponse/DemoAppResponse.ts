import { CoreAppResponseAbs } from "../../../converseQuest/core/library/appResponse/CoreAppResponseAbs";
import { DemoAppResponseItf } from "./DemoAppResponseItf";
import { DemoStatusItf } from "../status/DemoStatusItf";
import { DemoSlideItf } from "../slide/DemoSlideItf";
import { DemoConfigurationItf } from "../configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../variables/DemoVariablesItf";

export
	class DemoAppResponse
	extends CoreAppResponseAbs<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoSlideItf
	>
	implements DemoAppResponseItf
{

}