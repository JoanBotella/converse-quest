import { CoreAppResponseItf } from "../../../converseQuest/core/library/appResponse/CoreAppResponseItf";
import { DemoStatusItf } from "../status/DemoStatusItf";
import { DemoSlideItf } from "../slide/DemoSlideItf";
import { DemoVariablesItf } from "../variables/DemoVariablesItf";
import { DemoConfigurationItf } from "../configuration/DemoConfigurationItf";

export
	interface DemoAppResponseItf
	extends CoreAppResponseItf<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoSlideItf
	>
{

}