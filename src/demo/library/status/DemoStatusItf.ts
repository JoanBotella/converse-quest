import { CoreStatusItf } from "../../../converseQuest/core/library/status/CoreStatusItf";
import { DemoVariablesItf } from "../variables/DemoVariablesItf";
import { DemoConfigurationItf } from "../configuration/DemoConfigurationItf";

export
	interface DemoStatusItf
	extends CoreStatusItf<
		DemoConfigurationItf,
		DemoVariablesItf
	>
{

}