import { CoreStatusAbs } from "../../../converseQuest/core/library/status/CoreStatusAbs";
import { DemoStatusItf } from "./DemoStatusItf";
import { DemoVariablesItf } from "../variables/DemoVariablesItf";
import { DemoConfigurationItf } from "../configuration/DemoConfigurationItf";

export
	class DemoStatus
	extends CoreStatusAbs<
		DemoConfigurationItf,
		DemoVariablesItf
	>
	implements DemoStatusItf
{

}