import { DemoStatusItf } from "../status/DemoStatusItf";
import { DemoSlideItf } from "../slide/DemoSlideItf";
import { CoreNodeResponseItf } from "../../../converseQuest/core/library/nodeResponse/CoreNodeResponseItf";
import { DemoConfigurationItf } from "../configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../variables/DemoVariablesItf";

export
	interface DemoNodeResponseItf
	extends CoreNodeResponseItf<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoSlideItf
	>
{

}