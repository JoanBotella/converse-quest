import { CoreNodeResponseAbs } from "../../../converseQuest/core/library/nodeResponse/CoreNodeResponseAbs";
import { DemoStatusItf } from "../status/DemoStatusItf";
import { DemoSlideItf } from "../slide/DemoSlideItf";
import { DemoNodeResponseItf } from "../nodeResponse/DemoNodeResponseItf";
import { DemoConfigurationItf } from "../configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../variables/DemoVariablesItf";

export
	class DemoNodeResponse
	extends CoreNodeResponseAbs<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoSlideItf
	>
	implements DemoNodeResponseItf
{

}