import { CoreVariablesItf } from "../../../converseQuest/core/library/variables/CoreVariablesItf";

export
	interface DemoVariablesItf
	extends CoreVariablesItf
{

	getHasKeys: () => boolean;

	setHasKeys: (hasKeys:boolean) => void;

}