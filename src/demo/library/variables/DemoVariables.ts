import { DemoVariablesItf } from "./DemoVariablesItf";
import { CoreVariablesAbs } from "../../../converseQuest/core/library/variables/CoreVariablesAbs";

export
	class DemoVariables
	extends CoreVariablesAbs
	implements DemoVariablesItf
{

	private hasKeys:boolean;

	constructor()
	{
		super();
		this.hasKeys = false;
	}

	public getHasKeys():boolean
	{
		return this.hasKeys;
	}

	public setHasKeys(hasKeys:boolean):void
	{
		this.hasKeys = hasKeys;
	}

}