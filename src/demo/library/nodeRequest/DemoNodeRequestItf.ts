import { DemoStatusItf } from "../status/DemoStatusItf";
import { CoreNodeRequestItf } from "../../../converseQuest/core/library/nodeRequest/CoreNodeRequestItf";
import { DemoConfigurationItf } from "../configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../variables/DemoVariablesItf";

export
	interface DemoNodeRequestItf
	extends CoreNodeRequestItf<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf
	>
{

}