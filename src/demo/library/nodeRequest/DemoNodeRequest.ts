import { DemoNodeRequestItf } from "./DemoNodeRequestItf";
import { DemoStatusItf } from "../status/DemoStatusItf";
import { CoreNodeRequestAbs } from "../../../converseQuest/core/library/nodeRequest/CoreNodeRequestAbs";
import { DemoConfigurationItf } from "../configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../variables/DemoVariablesItf";

export
	class DemoNodeRequest
	extends CoreNodeRequestAbs<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf
	>
	implements DemoNodeRequestItf
{

}