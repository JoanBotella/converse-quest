import { CoreCliQueryNormalizerAbs } from "../../../converseQuest/core/library/cliQueryNormalizer/CoreCliQueryNormalizerAbs";
import { DemoCliQueryNormalizerItf } from "./DemoCliQueryNormalizerItf";

export
	class DemoCliEnQueryNormalizer
	extends CoreCliQueryNormalizerAbs
	implements DemoCliQueryNormalizerItf
{

	public normalize(query:string):string
	{
		query = query.toLowerCase();

		query = query.replace(/ (a|the|at|to|through|up) /g, ' ');

		return query;
	}

}