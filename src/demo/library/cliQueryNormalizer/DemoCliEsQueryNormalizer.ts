import { CoreCliQueryNormalizerAbs } from "../../../converseQuest/core/library/cliQueryNormalizer/CoreCliQueryNormalizerAbs";
import { DemoCliQueryNormalizerItf } from "./DemoCliQueryNormalizerItf";

export
	class DemoCliEsQueryNormalizer
	extends CoreCliQueryNormalizerAbs
	implements DemoCliQueryNormalizerItf
{

	public normalize(query:string):string
	{
		query = query.toLowerCase();

		query = query.replace(/á/g, 'a');
		query = query.replace(/é/g, 'e');
		query = query.replace(/í/g, 'i');
		query = query.replace(/ó/g, 'o');
		query = query.replace(/ú/g, 'u');

		query = query.replace(/ (el|la|los|las|hacia|a|al|en|con) /g, ' ');

		return query;
	}

}