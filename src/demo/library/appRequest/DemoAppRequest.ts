import { CoreAppRequestAbs } from "../../../converseQuest/core/library/appRequest/CoreAppRequestAbs";
import { DemoAppRequestItf } from "./DemoAppRequestItf";
import { DemoStatusItf } from "../status/DemoStatusItf";
import { DemoConfigurationItf } from "../configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../variables/DemoVariablesItf";

export
	class DemoAppRequest
	extends CoreAppRequestAbs<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf
	>
	implements DemoAppRequestItf
{

}