import { CoreAppRequestItf } from "../../../converseQuest/core/library/appRequest/CoreAppRequestItf";
import { DemoStatusItf } from "../status/DemoStatusItf";
import { DemoConfigurationItf } from "../configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../variables/DemoVariablesItf";

export
	interface DemoAppRequestItf
	extends CoreAppRequestItf<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf
	>
{

}