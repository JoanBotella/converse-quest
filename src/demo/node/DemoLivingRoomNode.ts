import { DemoNodeAbs } from "../library/node/DemoNodeAbs";
import { DemoSlide } from "../library/slide/DemoSlide";
import { DemoSlideItf } from "../library/slide/DemoSlideItf";
import { DemoNodeResponseItf } from "../library/nodeResponse/DemoNodeResponseItf";
import { DemoCorridorNode } from "./DemoCorridorNode";
import { DemoTalkJoanNode } from "./DemoTalkJoanNode";
import { DemoStatusItf } from "../library/status/DemoStatusItf";
import { DemoVariablesItf } from "../library/variables/DemoVariablesItf";
import { DemoNodeRequestItf } from "../library/nodeRequest/DemoNodeRequestItf";
import { DemoConfigurationItf } from "../library/configuration/DemoConfigurationItf";
import { DemoTranslationItf } from "../library/translation/DemoTranslationItf";

export
	class DemoLivingRoomNode
	extends DemoNodeAbs
{

	static readonly ID:string = 'livingRoom';

	protected tryToSetupNodeResponse():void
	{
		let
			nodeRequest:DemoNodeRequestItf = this.getNodeRequest(),
			status:DemoStatusItf = nodeRequest.getStatus(),
			configuration:DemoConfigurationItf = status.getConfiguration(),
			variables:DemoVariablesItf = status.getVariables(),
			translation:DemoTranslationItf = this.getTranslation()
		;

		if (!this.hasNodeRequestQuery())
		{
			let slide:DemoSlideItf = new DemoSlide();

			slide.setTitle(
				translation.node_livingRoom_title()
			);

			slide.setText(
				translation.node_livingRoom_text_look()
			);

			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[
					slide,
				]
			);

			this.setNodeResponse(nodeResponse);
			return;
		}

		if (this.queryMatches(translation.node_livingRoom_query_look()))
		{
			let slide:DemoSlide = new DemoSlide();

			slide.setText(
				translation.node_livingRoom_text_look()
			);

			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[
					slide,
				]
			);

			this.setNodeResponse(nodeResponse);
		}
		else if (this.queryMatches(translation.node_livingRoom_query_lookJoan()))
		{
			let slide:DemoSlide = new DemoSlide();

			slide.setText(
				translation.node_livingRoom_text_lookJoan()
			);

			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[
					slide,
				]
			);

			this.setNodeResponse(nodeResponse);
		}
		else if (this.queryMatches(translation.node_livingRoom_query_talkJoan()))
		{
			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[]
			);

			nodeResponse = this.getNodeBrowser().push(
				nodeResponse,
				DemoTalkJoanNode.ID
			);

			this.setNodeResponse(nodeResponse);
		}
		else if (this.queryMatches(translation.node_livingRoom_query_goCorridor()))
		{
			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[]
			);

			nodeResponse = this.getNodeBrowser().change(
				nodeResponse,
				DemoCorridorNode.ID
			);

			this.setNodeResponse(nodeResponse);
		}
	}

}
