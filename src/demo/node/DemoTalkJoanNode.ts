import { DemoNodeAbs } from "../library/node/DemoNodeAbs";
import { DemoSlide } from "../library/slide/DemoSlide";
import { DemoVariablesItf } from "../library/variables/DemoVariablesItf";
import { DemoNodeResponseItf } from "../library/nodeResponse/DemoNodeResponseItf";
import { DemoSlideItf } from "../library/slide/DemoSlideItf";
import { DemoStatusItf } from "../library/status/DemoStatusItf";
import { DemoNodeRequestItf } from "../library/nodeRequest/DemoNodeRequestItf";
import { DemoConfigurationItf } from "../library/configuration/DemoConfigurationItf";
import { DemoTranslationItf } from "../library/translation/DemoTranslationItf";

export
	class DemoTalkJoanNode
	extends DemoNodeAbs
{

	static readonly ID:string = 'talkJoan';

	protected tryToSetupNodeResponse():void
	{
		let
			nodeRequest:DemoNodeRequestItf = this.getNodeRequest(),
			status:DemoStatusItf = nodeRequest.getStatus(),
			configuration:DemoConfigurationItf = status.getConfiguration(),
			variables:DemoVariablesItf = status.getVariables(),
			translation:DemoTranslationItf = this.getTranslation()
		;

		if (!this.hasNodeRequestQuery())
		{
			let slide:DemoSlideItf = new DemoSlide();

			slide.setText(
				translation.node_talkJoan_text_look()
			);

			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[
					slide,
				]
			);

			this.setNodeResponse(nodeResponse);
			return;
		}

		if (this.queryMatches(translation.node_talkJoan_query_1()))
		{
			let slide:DemoSlideItf = new DemoSlide();

			slide.setText(
				translation.node_talkJoan_text_1()
			);

			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[
					slide,
				]
			);

			nodeResponse = this.getNodeBrowser().pop(nodeResponse);

			this.setNodeResponse(nodeResponse);
		}
	}

}
