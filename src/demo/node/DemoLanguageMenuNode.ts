import { DemoNodeAbs } from "../library/node/DemoNodeAbs";
import { DemoSlideItf } from "../library/slide/DemoSlideItf";
import { DemoSlide } from "../library/slide/DemoSlide";
import { DemoNodeResponseItf } from "../library/nodeResponse/DemoNodeResponseItf";
import { DemoStatusItf } from "../library/status/DemoStatusItf";
import { DemoVariablesItf } from "../library/variables/DemoVariablesItf";
import { DemoNodeRequestItf } from "../library/nodeRequest/DemoNodeRequestItf";
import { DemoConfigurationItf } from "../library/configuration/DemoConfigurationItf";
import { DemoEnTranslation } from "../library/translation/DemoEnTranslation";
import { DemoEsTranslation } from "../library/translation/DemoEsTranslation";
import { DemoTranslationItf } from "../library/translation/DemoTranslationItf";

export
	class DemoLanguageMenuNode
	extends DemoNodeAbs
{

	static readonly ID:string = 'languageMenu';

	protected tryToSetupNodeResponse():void
	{
		let
			nodeRequest:DemoNodeRequestItf = this.getNodeRequest(),
			status:DemoStatusItf = nodeRequest.getStatus(),
			configuration:DemoConfigurationItf = status.getConfiguration(),
			variables:DemoVariablesItf = status.getVariables(),
			translation:DemoTranslationItf = this.getTranslation()
		;

		if (!this.hasNodeRequestQuery())
		{
			let slide:DemoSlideItf = new DemoSlide();

			slide.setTitle(
				translation.node_languageMenu_title()
			);

			slide.setText(
				translation.node_languageMenu_text_look()
			);

			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[
					slide,
				]
			);

			this.setNodeResponse(nodeResponse);
			return;
		}

		if (this.queryMatches(translation.node_languageMenu_query_1()))
		{
			configuration.setLanguageCode(
				DemoEnTranslation.CODE
			);

			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[]
			);

			nodeResponse = this.getNodeBrowser().pop(nodeResponse);

			this.setNodeResponse(nodeResponse);
		}
		else if (this.queryMatches(translation.node_languageMenu_query_2()))
		{
			configuration.setLanguageCode(
				DemoEsTranslation.CODE
			);

			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[]
			);

			nodeResponse = this.getNodeBrowser().pop(nodeResponse);

			this.setNodeResponse(nodeResponse);
		}

	}

}
