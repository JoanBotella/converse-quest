import { DemoNodeAbs } from "../library/node/DemoNodeAbs";
import { DemoSlideItf } from "../library/slide/DemoSlideItf";
import { DemoNodeResponseItf } from "../library/nodeResponse/DemoNodeResponseItf";
import { DemoBedroomNode } from "./DemoBedroomNode";
import { DemoSlide } from "../library/slide/DemoSlide";
import { DemoLivingRoomNode } from "./DemoLivingRoomNode";
import { DemoStatusItf } from "../library/status/DemoStatusItf";
import { DemoVariablesItf } from "../library/variables/DemoVariablesItf";
import { DemoNodeRequestItf } from "../library/nodeRequest/DemoNodeRequestItf";
import { DemoConfigurationItf } from "../library/configuration/DemoConfigurationItf";
import { DemoTranslationItf } from "../library/translation/DemoTranslationItf";

export
	class DemoCorridorNode
	extends DemoNodeAbs
{

	static readonly ID:string = 'corridor';

	protected tryToSetupNodeResponse():void
	{
		let
			nodeRequest:DemoNodeRequestItf = this.getNodeRequest(),
			status:DemoStatusItf = nodeRequest.getStatus(),
			configuration:DemoConfigurationItf = status.getConfiguration(),
			variables:DemoVariablesItf = status.getVariables(),
			translation:DemoTranslationItf = this.getTranslation()
		;

		if (!this.hasNodeRequestQuery())
		{
			let slide:DemoSlideItf = new DemoSlide();

			slide.setTitle(
				translation.node_corridor_title()
			);

			slide.setText(
				translation.node_corridor_text_look()
			);

			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[
					slide,
				]
			);

			this.setNodeResponse(nodeResponse);
			return;
		}

		if (this.queryMatches(translation.node_corridor_query_look()))
		{
			let slide:DemoSlideItf = new DemoSlide();

			slide.setText(
				translation.node_corridor_text_look()
			);

			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[
					slide,
				]
			);

			this.setNodeResponse(nodeResponse);
		}
		else if (this.queryMatches(translation.node_corridor_query_goBedroom()))
		{
			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[]
			);

			nodeResponse = this.getNodeBrowser().change(
				nodeResponse,
				DemoBedroomNode.ID
			);

			this.setNodeResponse(nodeResponse);
		}
		else if (this.queryMatches(translation.node_corridor_query_goLivingRoom()))
		{
			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[]
			);

			nodeResponse = this.getNodeBrowser().change(
				nodeResponse,
				DemoLivingRoomNode.ID
			);

			this.setNodeResponse(nodeResponse);
		}
	}

}
