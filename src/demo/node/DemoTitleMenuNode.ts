import { DemoNodeAbs } from "../library/node/DemoNodeAbs";
import { DemoSlideItf } from "../library/slide/DemoSlideItf";
import { DemoSlide } from "../library/slide/DemoSlide";
import { DemoBedroomNode } from "./DemoBedroomNode";
import { DemoNodeResponseItf } from "../library/nodeResponse/DemoNodeResponseItf";
import { DemoStatusItf } from "../library/status/DemoStatusItf";
import { DemoVariablesItf } from "../library/variables/DemoVariablesItf";
import { DemoConfigurationMenuNode } from "./DemoConfigurationMenuNode";
import { DemoNodeRequestItf } from "../library/nodeRequest/DemoNodeRequestItf";
import { DemoConfigurationItf } from "../library/configuration/DemoConfigurationItf";
import { DemoTranslationItf } from "../library/translation/DemoTranslationItf";

export
	class DemoTitleMenuNode
	extends DemoNodeAbs
{

	static readonly ID:string = 'titleMenu';

	protected tryToSetupNodeResponse():void
	{
		let
			nodeRequest:DemoNodeRequestItf = this.getNodeRequest(),
			status:DemoStatusItf = nodeRequest.getStatus(),
			configuration:DemoConfigurationItf = status.getConfiguration(),
			variables:DemoVariablesItf = status.getVariables(),
			translation:DemoTranslationItf = this.getTranslation()
		;

		if (!this.hasNodeRequestQuery())
		{
			let slide:DemoSlideItf = new DemoSlide();

			slide.setTitle(
				translation.node_titleMenu_title()
			);

			slide.setText(
				translation.node_titleMenu_text_look()
			);

			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[
					slide,
				]
			);

			this.setNodeResponse(nodeResponse);
			return;
		}

		if (this.queryMatches(translation.node_titleMenu_query_1()))
		{
			let slide:DemoSlideItf = new DemoSlide();

			slide.setText(
				translation.node_titleMenu_text_1()
			);

			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[
					slide,
				]
			);

			nodeResponse = this.getNodeBrowser().change(
				nodeResponse,
				DemoBedroomNode.ID
			);

			this.setNodeResponse(nodeResponse);
		}
		else if (this.queryMatches(translation.node_titleMenu_query_2()))
		{
			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[]
			);

			nodeResponse = this.getNodeBrowser().push(
				nodeResponse,
				DemoConfigurationMenuNode.ID
			);

			this.setNodeResponse(nodeResponse);
		}
		else if (this.queryMatches(translation.node_titleMenu_query_3()))
		{
			let slide:DemoSlideItf = new DemoSlide();

			slide.setText(
				translation.node_titleMenu_text_3()
			);

			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[
					slide,
				]
			);

			nodeResponse.setEnd(true);

			this.setNodeResponse(nodeResponse);
		}

	}

}
