import { DemoNodeAbs } from "../library/node/DemoNodeAbs";
import { DemoSlideItf } from "../library/slide/DemoSlideItf";
import { DemoSlide } from "../library/slide/DemoSlide";
import { DemoNodeResponseItf } from "../library/nodeResponse/DemoNodeResponseItf";
import { DemoStatusItf } from "../library/status/DemoStatusItf";
import { DemoVariablesItf } from "../library/variables/DemoVariablesItf";
import { DemoNodeRequestItf } from "../library/nodeRequest/DemoNodeRequestItf";
import { DemoConfigurationItf } from "../library/configuration/DemoConfigurationItf";
import { DemoLanguageMenuNode } from "./DemoLanguageMenuNode";
import { DemoTranslationItf } from "../library/translation/DemoTranslationItf";

export
	class DemoConfigurationMenuNode
	extends DemoNodeAbs
{

	static readonly ID:string = 'configurationMenu';

	protected tryToSetupNodeResponse():void
	{
		let
			nodeRequest:DemoNodeRequestItf = this.getNodeRequest(),
			status:DemoStatusItf = nodeRequest.getStatus(),
			configuration:DemoConfigurationItf = status.getConfiguration(),
			variables:DemoVariablesItf = status.getVariables(),
			translation:DemoTranslationItf = this.getTranslation()
		;

		if (!this.hasNodeRequestQuery())
		{
			let slide:DemoSlideItf = new DemoSlide();

			slide.setTitle(
				translation.node_configurationMenu_title()
			);

			slide.setText(
				translation.node_configurationMenu_text_look()
			);

			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[
					slide,
				]
			);

			this.setNodeResponse(nodeResponse);
			return;
		}

		if (this.queryMatches(translation.node_configurationMenu_query_1()))
		{
			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[]
			);

			nodeResponse = this.getNodeBrowser().push(
				nodeResponse,
				DemoLanguageMenuNode.ID
			);

			this.setNodeResponse(nodeResponse);
		}
		else if (this.queryMatches(translation.node_configurationMenu_query_2()))
		{
			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[]
			);

			nodeResponse = this.getNodeBrowser().pop(nodeResponse);

			this.setNodeResponse(nodeResponse);
		}

	}

}
