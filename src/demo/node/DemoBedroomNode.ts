import { DemoNodeAbs } from "../library/node/DemoNodeAbs";
import { DemoSlideItf } from "../library/slide/DemoSlideItf";
import { DemoNodeResponseItf } from "../library/nodeResponse/DemoNodeResponseItf";
import { DemoSlide } from "../library/slide/DemoSlide";
import { DemoCorridorNode } from "./DemoCorridorNode";
import { DemoStatusItf } from "../library/status/DemoStatusItf";
import { DemoVariablesItf } from "../library/variables/DemoVariablesItf";
import { DemoNodeRequestItf } from "../library/nodeRequest/DemoNodeRequestItf";
import { DemoConfigurationItf } from "../library/configuration/DemoConfigurationItf";
import { DemoTranslationItf } from "../library/translation/DemoTranslationItf";

export
	class DemoBedroomNode
	extends DemoNodeAbs
{
	static readonly ID:string = 'bedroom';

	protected tryToSetupNodeResponse():void
	{
		let
			nodeRequest:DemoNodeRequestItf = this.getNodeRequest(),
			status:DemoStatusItf = nodeRequest.getStatus(),
			configuration:DemoConfigurationItf = status.getConfiguration(),
			variables:DemoVariablesItf = status.getVariables(),
			translation:DemoTranslationItf = this.getTranslation()
		;

		if (!this.hasNodeRequestQuery())
		{
			let slide:DemoSlideItf = new DemoSlide();

			slide.setTitle(
				translation.node_bedroom_title()
			);

			slide.setText(
				this.buildLook(variables)
			);

			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[
					slide,
				]
			);

			this.setNodeResponse(nodeResponse);
			return;
		}

		if (this.queryMatches(translation.node_bedroom_query_look()))
		{
			let slide:DemoSlideItf = new DemoSlide();

			slide.setText(
				this.buildLook(variables)
			);

			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[
					slide,
				]
			);

			this.setNodeResponse(nodeResponse);
		}
		else if (this.queryMatches(translation.node_bedroom_query_lookKeys()))
		{
			let slide:DemoSlideItf = new DemoSlide();

			slide.setText(
				translation.node_bedroom_text_lookKeys()
			);

			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[
					slide,
				]
			);

			this.setNodeResponse(nodeResponse);
		}
		else if (this.queryMatches(translation.node_bedroom_query_takeKeys()))
		{
			variables.setHasKeys(true);

			let slide:DemoSlideItf = new DemoSlide();

			slide.setText(
				translation.node_bedroom_text_takeKeys()
			);

			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[
					slide,
				]
			);

			this.setNodeResponse(nodeResponse);
		}
		else if (this.queryMatches(translation.node_bedroom_query_goCorridor()))
		{
			let nodeResponse:DemoNodeResponseItf = this.buildNodeResponse(
				configuration,
				variables,
				status,
				[]
			);

			nodeResponse = this.getNodeBrowser().change(
				nodeResponse,
				DemoCorridorNode.ID
			);

			this.setNodeResponse(nodeResponse);
		}
	}

		protected buildLook(variables:DemoVariablesItf):string
		{
			let
				translation:DemoTranslationItf = this.getTranslation(),
				text:string = translation.node_bedroom_text_look_1()
			;
			if (!variables.getHasKeys())
			{
				text += translation.node_bedroom_text_look_2();
			}
			text += translation.node_bedroom_text_look_3();
			return text;
		}

}
