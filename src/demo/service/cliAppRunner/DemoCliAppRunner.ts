import { DemoCliAppRunnerItf } from "./DemoCliAppRunnerItf";
import { CoreCliAppRunnerAbs } from "../../../converseQuest/core/library/cliAppRunner/CoreCliAppRunnerAbs";
import { DemoStatusItf } from "../../library/status/DemoStatusItf";
import { DemoAppRequestItf } from "../../library/appRequest/DemoAppRequestItf";
import { DemoAppItf } from "../app/DemoAppItf";
import { DemoAppResponseItf } from "../../library/appResponse/DemoAppResponseItf";
import { DemoSlideItf } from "../../library/slide/DemoSlideItf";
import { DemoCliAppResponseDispatcherItf } from "../cliAppResponseDispatcher/DemoCliAppResponseDispatcherItf";
import { DemoCliAppRequestBuilderItf } from "../cliAppRequestBuilder/DemoCliAppRequestBuilderItf";
import { DemoStartingStatusBuilderItf } from "../startingStatusBuilder/DemoStartingStatusBuilderItf";
import { DemoConfigurationItf } from "../../library/configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../../library/variables/DemoVariablesItf";

export
	class DemoCliAppRunner
	extends CoreCliAppRunnerAbs<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoAppRequestItf,
		DemoSlideItf,
		DemoAppResponseItf,
		DemoStartingStatusBuilderItf,
		DemoCliAppRequestBuilderItf,
		DemoAppItf,
		DemoCliAppResponseDispatcherItf
	>
	implements DemoCliAppRunnerItf
{

}