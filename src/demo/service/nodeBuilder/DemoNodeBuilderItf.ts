import { DemoNodeItf } from "../../library/node/DemoNodeItf";
import { CoreNodeBuilderItf } from "../../../converseQuest/core/library/nodeBuilder/CoreNodeBuilderItf";

export
	interface DemoNodeBuilderItf
	extends CoreNodeBuilderItf
{

	buildBedroomNode: () => DemoNodeItf;

	buildCorridorNode: () => DemoNodeItf;

	buildLivingRoomNode: () => DemoNodeItf;

	buildTalkJoanNode: () => DemoNodeItf;

	buildTitleMenuNode: () => DemoNodeItf;

	buildSettingsMenuNode: () => DemoNodeItf;

	buildLanguageMenuNode: () => DemoNodeItf;

}