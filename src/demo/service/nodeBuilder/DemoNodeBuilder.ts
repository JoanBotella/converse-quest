import { DemoNodeBuilderItf } from "./DemoNodeBuilderItf";
import { DemoBedroomNode } from "../../node/DemoBedroomNode";
import { DemoNodeItf } from "../../library/node/DemoNodeItf";
import { DemoCorridorNode } from "../../node/DemoCorridorNode";
import { DemoLivingRoomNode } from "../../node/DemoLivingRoomNode";
import { DemoTalkJoanNode } from "../../node/DemoTalkJoanNode";
import { DemoTitleMenuNode } from "../../node/DemoTitleMenuNode";
import { CoreNodeBuilderAbs } from "../../../converseQuest/core/library/nodeBuilder/CoreNodeBuilderAbs";
import { DemoConfigurationMenuNode } from "../../node/DemoConfigurationMenuNode";
import { DemoServiceContainerItf } from "../../library/serviceContainer/DemoServiceContainerItf";
import { DemoLanguageMenuNode } from "../../node/DemoLanguageMenuNode";

export
	class DemoNodeBuilder
	extends CoreNodeBuilderAbs
	implements DemoNodeBuilderItf
{

	private serviceContainer:DemoServiceContainerItf;

	constructor(
		serviceContainer:DemoServiceContainerItf
	)
	{
		super();
		this.serviceContainer = serviceContainer;
	}

	public buildBedroomNode():DemoNodeItf
	{
		return new DemoBedroomNode(
			this.serviceContainer.getNodeBrowser(),
			this.serviceContainer.getTranslationContainer()
		);
	}

	public buildCorridorNode():DemoNodeItf
	{
		return new DemoCorridorNode(
			this.serviceContainer.getNodeBrowser(),
			this.serviceContainer.getTranslationContainer()
		);
	}

	public buildLivingRoomNode():DemoNodeItf
	{
		return new DemoLivingRoomNode(
			this.serviceContainer.getNodeBrowser(),
			this.serviceContainer.getTranslationContainer()
		);
	}

	public buildTalkJoanNode():DemoNodeItf
	{
		return new DemoTalkJoanNode(
			this.serviceContainer.getNodeBrowser(),
			this.serviceContainer.getTranslationContainer()
		);
	}

	public buildTitleMenuNode():DemoNodeItf
	{
		return new DemoTitleMenuNode(
			this.serviceContainer.getNodeBrowser(),
			this.serviceContainer.getTranslationContainer()
		);
	}

	public buildSettingsMenuNode():DemoNodeItf
	{
		return new DemoConfigurationMenuNode(
			this.serviceContainer.getNodeBrowser(),
			this.serviceContainer.getTranslationContainer()
		);
	}

	public buildLanguageMenuNode():DemoNodeItf
	{
		return new DemoLanguageMenuNode(
			this.serviceContainer.getNodeBrowser(),
			this.serviceContainer.getTranslationContainer()
		);
	}

}