import { CoreCliQueryBuilderAbs } from "../../../converseQuest/core/library/cliQueryBuilder/CoreCliQueryBuilderAbs";
import { DemoCliPrompterItf } from "../cliPrompter/DemoCliPrompterItf";
import { DemoCliQueryBuilderItf } from "./DemoCliQueryBuilderItf";
import { DemoCliQueryNormalizerItf } from "../../library/cliQueryNormalizer/DemoCliQueryNormalizerItf";
import { DemoCliQueryNormalizerContainerItf } from "../cliQueryNormalizerContainer/DemoCliQueryNormalizerContainerItf";
import { DemoTranslationItf } from "../../library/translation/DemoTranslationItf";
import { DemoTranslationContainerItf } from "../translationContainer/DemoTranslationContainerItf";

export
	class DemoCliQueryBuilder
	extends CoreCliQueryBuilderAbs<
		DemoCliPrompterItf,
		DemoCliQueryNormalizerItf,
		DemoCliQueryNormalizerContainerItf,
		DemoTranslationItf,
		DemoTranslationContainerItf
	>
	implements DemoCliQueryBuilderItf
{
}