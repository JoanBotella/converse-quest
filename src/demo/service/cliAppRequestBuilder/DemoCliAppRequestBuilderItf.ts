import { CoreCliAppRequestBuilderItf } from "../../../converseQuest/core/library/cliAppRequestBuilder/CoreCliAppRequestBuilderItf"
import { DemoStatusItf } from "../../library/status/DemoStatusItf";
import { DemoAppRequestItf } from "../../library/appRequest/DemoAppRequestItf";
import { DemoConfigurationItf } from "../../library/configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../../library/variables/DemoVariablesItf";

export
	interface DemoCliAppRequestBuilderItf
	extends CoreCliAppRequestBuilderItf<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoAppRequestItf
	>
{

}