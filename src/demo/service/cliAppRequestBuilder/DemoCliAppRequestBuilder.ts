import { CoreCliAppRequestBuilderAbs } from "../../../converseQuest/core/library/cliAppRequestBuilder/CoreCliAppRequestBuilderAbs";
import { DemoCliAppRequestBuilderItf } from "./DemoCliAppRequestBuilderItf";
import { DemoAppRequestItf } from "../../library/appRequest/DemoAppRequestItf";
import { DemoAppRequest } from "../../library/appRequest/DemoAppRequest";
import { DemoStatusItf } from "../../library/status/DemoStatusItf";
import { DemoCliQueryBuilderItf } from "../cliQueryBuilder/DemoCliQueryBuilderItf";
import { DemoConfigurationItf } from "../../library/configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../../library/variables/DemoVariablesItf";

export
	class DemoCliAppRequestBuilder
	extends CoreCliAppRequestBuilderAbs<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoAppRequestItf,
		DemoCliQueryBuilderItf
	>
	implements DemoCliAppRequestBuilderItf
{

	protected instantiate(status:DemoStatusItf):DemoAppRequestItf
	{
		return new DemoAppRequest(status);
	}

}