import { DemoStatusItf } from "../../library/status/DemoStatusItf";
import { CoreStartingStatusBuilderAbs } from "../../../converseQuest/core/library/startingStatusBuilder/CoreStartingStatusBuilderAbs";
import { DemoStartingStatusBuilderItf } from "./DemoStartingStatusBuilderItf";
import { DemoStatus } from "../../library/status/DemoStatus";
import { DemoTitleMenuNode } from "../../node/DemoTitleMenuNode";
import { DemoConfiguration } from "../../library/configuration/DemoConfiguration";
import { DemoVariables } from "../../library/variables/DemoVariables";
import { DemoConfigurationItf } from "../../library/configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../../library/variables/DemoVariablesItf";
import { DemoEnTranslation } from "../../library/translation/DemoEnTranslation";

export
	class DemoStartingStatusBuilder
	extends CoreStartingStatusBuilderAbs<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf
	>
	implements DemoStartingStatusBuilderItf
{

	public build():DemoStatusItf
	{
		return new DemoStatus(
			DemoTitleMenuNode.ID,
			new DemoConfiguration(
				DemoEnTranslation.CODE
			),
			new DemoVariables()
		);
	}

}