import { CoreStartingStatusBuilderItf } from "../../../converseQuest/core/library/startingStatusBuilder/CoreStartingStatusBuilderItf";
import { DemoStatusItf } from "../../library/status/DemoStatusItf";
import { DemoVariablesItf } from "../../library/variables/DemoVariablesItf";
import { DemoConfigurationItf } from "../../library/configuration/DemoConfigurationItf";

export
	interface DemoStartingStatusBuilderItf
	extends CoreStartingStatusBuilderItf<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf
	>
{

}