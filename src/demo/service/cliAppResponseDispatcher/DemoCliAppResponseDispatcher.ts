import { CoreCliAppResponseDispatcherAbs } from "../../../converseQuest/core/library/cliAppResponseDispatcher/CoreCliAppResponseDispatcherAbs";
import { DemoCliAppResponseDispatcherItf } from "./DemoCliAppResponseDispatcherItf";
import { DemoSlideItf } from "../../library/slide/DemoSlideItf";
import { DemoStatusItf } from "../../library/status/DemoStatusItf";
import { DemoAppResponseItf } from "../../library/appResponse/DemoAppResponseItf";
import { DemoCliPrompterItf } from "../cliPrompter/DemoCliPrompterItf";
import { CoreCliStylizerItf } from "../../../converseQuest/core/service/cliStylizer/CoreCliStylizerItf";
import { DemoVariablesItf } from "../../library/variables/DemoVariablesItf";
import { DemoConfigurationItf } from "../../library/configuration/DemoConfigurationItf";
import { DemoTranslationItf } from "../../library/translation/DemoTranslationItf";
import { DemoTranslationContainerItf } from "../translationContainer/DemoTranslationContainerItf";

export
	class DemoCliAppResponseDispatcher
	extends CoreCliAppResponseDispatcherAbs<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoSlideItf,
		DemoAppResponseItf,
		CoreCliStylizerItf,
		DemoCliPrompterItf,
		DemoTranslationItf,
		DemoTranslationContainerItf
	>
	implements DemoCliAppResponseDispatcherItf
{

}