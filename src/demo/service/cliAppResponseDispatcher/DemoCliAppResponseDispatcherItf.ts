import { CoreCliAppResponseDispatcherItf } from "../../../converseQuest/core/library/cliAppResponseDispatcher/CoreCliAppResponseDispatcherItf";
import { DemoStatusItf } from "../../library/status/DemoStatusItf";
import { DemoSlideItf } from "../../library/slide/DemoSlideItf";
import { DemoAppResponseItf } from "../../library/appResponse/DemoAppResponseItf";
import { DemoConfigurationItf } from "../../library/configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../../library/variables/DemoVariablesItf";

export
	interface DemoCliAppResponseDispatcherItf
	extends CoreCliAppResponseDispatcherItf<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoSlideItf,
		DemoAppResponseItf
	>
{

}