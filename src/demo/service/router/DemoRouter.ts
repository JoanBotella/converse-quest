import { DemoStatusItf } from "../../library/status/DemoStatusItf";
import { DemoNodeRequestItf } from "../../library/nodeRequest/DemoNodeRequestItf";
import { DemoSlideItf } from "../../library/slide/DemoSlideItf";
import { DemoNodeResponseItf } from "../../library/nodeResponse/DemoNodeResponseItf";
import { DemoNodeItf } from "../../library/node/DemoNodeItf";
import { CoreRouterAbs } from "../../../converseQuest/core/library/router/CoreRouterAbs";
import { DemoRouterItf } from "./DemoRouterItf";
import { DemoTitleMenuNode } from "../../node/DemoTitleMenuNode";
import { DemoBedroomNode } from "../../node/DemoBedroomNode";
import { DemoCorridorNode } from "../../node/DemoCorridorNode";
import { DemoLivingRoomNode } from "../../node/DemoLivingRoomNode";
import { DemoTalkJoanNode } from "../../node/DemoTalkJoanNode";
import { DemoNodeBuilderItf } from "../nodeBuilder/DemoNodeBuilderItf";
import { DemoConfigurationMenuNode } from "../../node/DemoConfigurationMenuNode";
import { DemoConfigurationItf } from "../../library/configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../../library/variables/DemoVariablesItf";
import { DemoLanguageMenuNode } from "../../node/DemoLanguageMenuNode";

export
	class DemoRouter
	extends CoreRouterAbs<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoNodeRequestItf,
		DemoSlideItf,
		DemoNodeResponseItf,
		DemoNodeItf,
		DemoNodeBuilderItf
	>
	implements DemoRouterItf
{

	public route(nodeId:string):DemoNodeItf
	{
		let nodeBuilder:DemoNodeBuilderItf = this.getNodeBuilder();

		switch (nodeId)
		{
			case DemoBedroomNode.ID: return nodeBuilder.buildBedroomNode();
			case DemoConfigurationMenuNode.ID: return nodeBuilder.buildSettingsMenuNode();
			case DemoCorridorNode.ID: return nodeBuilder.buildCorridorNode();
			case DemoLanguageMenuNode.ID: return nodeBuilder.buildLanguageMenuNode();
			case DemoLivingRoomNode.ID: return nodeBuilder.buildLivingRoomNode();
			case DemoTalkJoanNode.ID: return nodeBuilder.buildTalkJoanNode();
			case DemoTitleMenuNode.ID: return nodeBuilder.buildTitleMenuNode();

			default:
				throw new Error('The node id "' + nodeId + '" is unroutable.');
		}
	}

}