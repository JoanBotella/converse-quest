import { DemoTranslationItf } from "../../library/translation/DemoTranslationItf";
import { DemoTranslationContainerItf } from "./DemoTranslationContainerItf";
import { CoreTranslationContainerAbs } from "../../../converseQuest/core/library/translationContainer/CoreTranslationContainerAbs";
import { DemoEnTranslation } from "../../library/translation/DemoEnTranslation";
import { DemoEsTranslation } from "../../library/translation/DemoEsTranslation";

export
	class DemoTranslationContainer
	extends CoreTranslationContainerAbs<
		DemoTranslationItf
	>
	implements DemoTranslationContainerItf
{

	protected buildTranslation(languageCode:string):DemoTranslationItf
	{
		switch(languageCode)
		{
			case DemoEnTranslation.CODE: return new DemoEnTranslation();
			case DemoEsTranslation.CODE: return new DemoEsTranslation();
		}
		throw new Error('There is no translation for the language code "' + languageCode + '"');
	}

}