import { CoreTranslationContainerItf } from "../../../converseQuest/core/library/translationContainer/CoreTranslationContainerItf";
import { DemoTranslationItf } from "../../library/translation/DemoTranslationItf";

export
	interface DemoTranslationContainerItf
	extends CoreTranslationContainerItf<
		DemoTranslationItf
	>
{

}