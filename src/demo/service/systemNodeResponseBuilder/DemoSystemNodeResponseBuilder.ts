import { DemoStatusItf } from "../../library/status/DemoStatusItf";
import { DemoNodeRequestItf } from "../../library/nodeRequest/DemoNodeRequestItf";
import { DemoSlideItf } from "../../library/slide/DemoSlideItf";
import { DemoNodeResponseItf } from "../../library/nodeResponse/DemoNodeResponseItf";
import { DemoSystemNodeResponseBuilderItf } from "./DemoSystemNodeResponseBuilderItf";
import { CoreSystemNodeResponseBuilderAbs } from "../../../converseQuest/core/library/systemNodeResponseBuilder/CoreSystemNodeResponseBuilderAbs";
import { DemoNodeResponse } from "../../library/nodeResponse/DemoNodeResponse";
import { DemoVariablesItf } from "../../library/variables/DemoVariablesItf";
import { DemoSlide } from "../../library/slide/DemoSlide";
import { DemoNodeBrowserItf } from "../nodeBrowser/DemoNodeBrowserItf";
import { DemoConfigurationItf } from "../../library/configuration/DemoConfigurationItf";
import { DemoConfigurationMenuNode } from "../../node/DemoConfigurationMenuNode";
import { DemoTranslationItf } from "../../library/translation/DemoTranslationItf";
import { DemoTranslationContainerItf } from "../translationContainer/DemoTranslationContainerItf";

export
	class DemoSystemNodeResponseBuilder
	extends CoreSystemNodeResponseBuilderAbs<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoNodeRequestItf,
		DemoSlideItf,
		DemoNodeResponseItf,
		DemoNodeBrowserItf,
		DemoTranslationItf,
		DemoTranslationContainerItf
	>
	implements DemoSystemNodeResponseBuilderItf
{

	public buildNodeResponse():DemoNodeResponseItf
	{
		let
			nodeRequest:DemoNodeRequestItf = this.getNodeRequest(),
			status:DemoStatusItf = nodeRequest.getStatus(),
			configuration:DemoConfigurationItf = status.getConfiguration(),
			variables:DemoVariablesItf = status.getVariables(),
			translation:DemoTranslationItf = this.getTranslation()
		;

		if (!this.hasNodeRequestQuery())
		{
			return new DemoNodeResponse(status);
		}

		if (this.queryMatches(translation.systemNodeResponseBuilder_query_exit()))
		{
			let nodeResponse:DemoNodeResponseItf = new DemoNodeResponse(status);

			let slide:DemoSlideItf = new DemoSlide();

			slide.setText(
				translation.systemNodeResponseBuilder_text_exit()
			);

			nodeResponse.appendSlide(slide);
			nodeResponse.setEnd(true);

			return nodeResponse;
		}

		if (this.queryMatches(translation.systemNodeResponseBuilder_query_inventory()))
		{
			let nodeResponse:DemoNodeResponseItf = new DemoNodeResponse(status);

			let slide:DemoSlideItf = new DemoSlide();

			slide.setText(
				this.buildInventoryText()
			);

			nodeResponse.appendSlide(slide);

			return nodeResponse;
		}

		if (this.queryMatches(translation.systemNodeResponseBuilder_query_configuration()))
		{
			let nodeResponse:DemoNodeResponseItf = new DemoNodeResponse(status);

			nodeResponse = this.getNodeBrowser().push(
				nodeResponse,
				DemoConfigurationMenuNode.ID
			);

			return nodeResponse;
		}

		if (
			variables.getHasKeys()
			&& this.queryMatches(translation.systemNodeResponseBuilder_query_lookKeys())
		)
		{
			let nodeResponse:DemoNodeResponseItf = new DemoNodeResponse(status);

			let slide:DemoSlideItf = new DemoSlide();

			slide.setText(
				translation.systemNodeResponseBuilder_text_lookKeys()
			);

			nodeResponse.appendSlide(slide);

			return nodeResponse;
		}

		return this.buildNotUnderstoodNodeResponse();
	}

		protected buildInventoryText():string
		{
			let
				nodeRequest:DemoNodeRequestItf = this.getNodeRequest(),
				status:DemoStatusItf = nodeRequest.getStatus(),
				configuration:DemoConfigurationItf = status.getConfiguration(),
				variables:DemoVariablesItf = status.getVariables(),
				translation:DemoTranslationItf = this.getTranslation(),
				text:string = ''
			;

			if (variables.getHasKeys())
			{
				text += translation.systemNodeResponseBuilder_text_inventory_keys();
			}

			if (text == '')
			{
				text += translation.systemNodeResponseBuilder_text_inventory_empty();
			}

			return text;
		}

		protected buildNotUnderstoodNodeResponse():DemoNodeResponseItf
		{
			let nodeResponse:DemoNodeResponseItf = new DemoNodeResponse(
				this.getNodeRequest().getStatus()
			);

			let slide:DemoSlideItf = new DemoSlide();

			slide.setText(
				this.getTranslation().systemNodeResponseBuilder_text_notUnderstood()
			);
	
			nodeResponse.appendSlide(slide);
	
			return nodeResponse;
		}

}