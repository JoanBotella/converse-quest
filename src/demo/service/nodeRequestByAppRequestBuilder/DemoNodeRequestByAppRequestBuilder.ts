import { DemoStatusItf } from "../../library/status/DemoStatusItf";
import { DemoNodeRequestItf } from "../../library/nodeRequest/DemoNodeRequestItf";
import { DemoAppRequestItf } from "../../library/appRequest/DemoAppRequestItf";
import { CoreNodeRequestByAppRequestBuilderAbs } from "../../../converseQuest/core/library/nodeRequestByAppRequestBuilder/CoreNodeRequestByAppRequestBuilderAbs";
import { DemoNodeRequestByAppRequestBuilderItf } from "./DemoNodeRequestByAppRequestBuilderItf";
import { DemoStatus } from "../../library/status/DemoStatus";
import { DemoNodeRequest } from "../../library/nodeRequest/DemoNodeRequest";
import { DemoConfigurationItf } from "../../library/configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../../library/variables/DemoVariablesItf";

export
	class DemoNodeRequestByAppRequestBuilder
	extends CoreNodeRequestByAppRequestBuilderAbs<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoNodeRequestItf,
		DemoAppRequestItf
	>
	implements DemoNodeRequestByAppRequestBuilderItf
{

	protected instantiateNodeRequest(status:DemoStatus):DemoNodeRequest
	{
		return new DemoNodeRequest(status);
	}

}