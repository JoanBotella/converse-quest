import { CoreNodeRequestByAppRequestBuilderItf } from "../../../converseQuest/core/library/nodeRequestByAppRequestBuilder/CoreNodeRequestByAppRequestBuilderItf";
import { DemoStatusItf } from "../../library/status/DemoStatusItf";
import { DemoNodeRequestItf } from "../../library/nodeRequest/DemoNodeRequestItf";
import { DemoAppRequestItf } from "../../library/appRequest/DemoAppRequestItf";
import { DemoConfigurationItf } from "../../library/configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../../library/variables/DemoVariablesItf";

export
	interface DemoNodeRequestByAppRequestBuilderItf
	extends CoreNodeRequestByAppRequestBuilderItf<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoNodeRequestItf,
		DemoAppRequestItf
	>
{

}