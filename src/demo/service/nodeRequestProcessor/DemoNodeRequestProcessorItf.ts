import { CoreNodeRequestProcessorItf } from "../../../converseQuest/core/library/nodeRequestProcessor/CoreNodeRequestProcessorItf";
import { DemoStatusItf } from "../../library/status/DemoStatusItf";
import { DemoNodeRequestItf } from "../../library/nodeRequest/DemoNodeRequestItf";
import { DemoSlideItf } from "../../library/slide/DemoSlideItf";
import { DemoNodeResponseItf } from "../../library/nodeResponse/DemoNodeResponseItf";
import { DemoConfigurationItf } from "../../library/configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../../library/variables/DemoVariablesItf";

export
	interface DemoNodeRequestProcessorItf
	extends CoreNodeRequestProcessorItf<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoNodeRequestItf,
		DemoSlideItf,
		DemoNodeResponseItf
	>
{

}