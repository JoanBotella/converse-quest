import { DemoStatusItf } from "../../library/status/DemoStatusItf";
import { DemoNodeRequestItf } from "../../library/nodeRequest/DemoNodeRequestItf";
import { DemoSlideItf } from "../../library/slide/DemoSlideItf";
import { DemoNodeResponseItf } from "../../library/nodeResponse/DemoNodeResponseItf";
import { CoreNodeRequestProcessorAbs } from "../../../converseQuest/core/library/nodeRequestProcessor/CoreNodeRequestProcessorAbs";
import { DemoNodeRequestProcessorItf } from "./DemoNodeRequestProcessorItf";
import { DemoSystemNodeResponseBuilderItf } from "../systemNodeResponseBuilder/DemoSystemNodeResponseBuilderItf";
import { DemoNodeItf } from "../../library/node/DemoNodeItf";
import { DemoRouterItf } from "../router/DemoRouterItf";
import { DemoConfigurationItf } from "../../library/configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../../library/variables/DemoVariablesItf";

export
	class DemoNodeRequestProcessor
	extends CoreNodeRequestProcessorAbs<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoNodeRequestItf,
		DemoSlideItf,
		DemoNodeResponseItf,
		DemoSystemNodeResponseBuilderItf,
		DemoNodeItf,
		DemoRouterItf
	>
	implements DemoNodeRequestProcessorItf
{

}