import { CoreNodeBrowserItf } from "../../../converseQuest/core/library/nodeBrowser/CoreNodeBrowserItf";
import { DemoStatusItf } from "../../library/status/DemoStatusItf";
import { DemoSlideItf } from "../../library/slide/DemoSlideItf";
import { DemoNodeResponseItf } from "../../library/nodeResponse/DemoNodeResponseItf";
import { DemoConfigurationItf } from "../../library/configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../../library/variables/DemoVariablesItf";

export
	interface DemoNodeBrowserItf
	extends CoreNodeBrowserItf<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoSlideItf,
		DemoNodeResponseItf
	>
{

}