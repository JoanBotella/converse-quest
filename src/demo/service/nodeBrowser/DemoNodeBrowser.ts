import { CoreNodeBrowserAbs } from "../../../converseQuest/core/library/nodeBrowser/CoreNodeBrowserAbs"
import { DemoNodeBrowserItf } from "./DemoNodeBrowserItf";
import { DemoStatusItf } from "../../library/status/DemoStatusItf";
import { DemoSlideItf } from "../../library/slide/DemoSlideItf";
import { DemoNodeResponseItf } from "../../library/nodeResponse/DemoNodeResponseItf";
import { DemoConfigurationItf } from "../../library/configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../../library/variables/DemoVariablesItf";

export
	class DemoNodeBrowser
	extends CoreNodeBrowserAbs<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoSlideItf,
		DemoNodeResponseItf
	>
	implements DemoNodeBrowserItf
{

}