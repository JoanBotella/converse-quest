import { CoreAppItf } from "../../../converseQuest/core/library/app/CoreAppItf";
import { DemoAppRequestItf } from "../../library/appRequest/DemoAppRequestItf";
import { DemoStatusItf } from "../../library/status/DemoStatusItf";
import { DemoAppResponseItf } from "../../library/appResponse/DemoAppResponseItf";
import { DemoSlideItf } from "../../library/slide/DemoSlideItf";
import { DemoConfigurationItf } from "../../library/configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../../library/variables/DemoVariablesItf";

export
	interface DemoAppItf
	extends CoreAppItf<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoAppRequestItf,
		DemoSlideItf,
		DemoAppResponseItf
	>
{

}