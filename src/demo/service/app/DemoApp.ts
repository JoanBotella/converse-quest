import { CoreAppAbs } from "../../../converseQuest/core/library/app/CoreAppAbs";
import { DemoAppRequestItf } from "../../library/appRequest/DemoAppRequestItf";
import { DemoAppItf } from "./DemoAppItf";
import { DemoStatusItf } from "../../library/status/DemoStatusItf";
import { DemoAppResponseItf } from "../../library/appResponse/DemoAppResponseItf";
import { DemoSlideItf } from "../../library/slide/DemoSlideItf";
import { DemoNodeRequestItf } from "../../library/nodeRequest/DemoNodeRequestItf";
import { DemoNodeResponseItf } from "../../library/nodeResponse/DemoNodeResponseItf";
import { DemoNodeRequestByAppRequestBuilderItf } from "../nodeRequestByAppRequestBuilder/DemoNodeRequestByAppRequestBuilderItf";
import { DemoAppResponseByNodeResponseBuilderItf } from "../appResponseByNodeResponseBuilder/DemoAppResponseByNodeResponseItf";
import { DemoNextNodeRequestBuilderItf } from "../nextNodeRequestBuilder/DemoNextNodeRequestBuilderItf";
import { DemoNodeRequestProcessorItf } from "../nodeRequestProcessor/DemoNodeRequestProcessorItf";
import { DemoConfigurationItf } from "../../library/configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../../library/variables/DemoVariablesItf";

export
	class DemoApp
	extends CoreAppAbs<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoAppRequestItf,
		DemoSlideItf,
		DemoAppResponseItf,
		DemoNodeRequestItf,
		DemoNodeResponseItf,
		DemoNodeRequestByAppRequestBuilderItf,
		DemoAppResponseByNodeResponseBuilderItf,
		DemoNextNodeRequestBuilderItf,
		DemoNodeRequestProcessorItf
	>
	implements DemoAppItf
{

}