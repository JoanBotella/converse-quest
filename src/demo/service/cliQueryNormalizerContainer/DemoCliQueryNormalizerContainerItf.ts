import { CoreCliQueryNormalizerContainerItf } from "../../../converseQuest/core/library/cliQueryNormalizerContainer/CoreCliQueryNormalizerContainerItf";
import { DemoCliQueryNormalizerItf } from "../../library/cliQueryNormalizer/DemoCliQueryNormalizerItf";

export
	interface DemoCliQueryNormalizerContainerItf
	extends CoreCliQueryNormalizerContainerItf<
		DemoCliQueryNormalizerItf
	>
{

}