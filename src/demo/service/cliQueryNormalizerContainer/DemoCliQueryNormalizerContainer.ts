import { CoreCliQueryNormalizerContainerAbs } from "../../../converseQuest/core/library/cliQueryNormalizerContainer/CoreCliQueryNormalizerContainerAbs";
import { DemoCliQueryNormalizerContainerItf } from "./DemoCliQueryNormalizerContainerItf";
import { DemoCliQueryNormalizerItf } from "../../library/cliQueryNormalizer/DemoCliQueryNormalizerItf";
import { DemoEnTranslation } from "../../library/translation/DemoEnTranslation";
import { DemoEsTranslation } from "../../library/translation/DemoEsTranslation";
import { DemoCliEnQueryNormalizer } from "../../library/cliQueryNormalizer/DemoCliEnQueryNormalizer";
import { DemoCliEsQueryNormalizer } from "../../library/cliQueryNormalizer/DemoCliEsQueryNormalizer";

export
	class DemoCliQueryNormalizerContainer
	extends CoreCliQueryNormalizerContainerAbs<
		DemoCliQueryNormalizerItf
	>
	implements DemoCliQueryNormalizerContainerItf
{

	protected buildCliQueryNormalizer(languageCode:string):DemoCliQueryNormalizerItf
	{
		switch(languageCode)
		{
			case DemoEnTranslation.CODE: return new DemoCliEnQueryNormalizer();
			case DemoEsTranslation.CODE: return new DemoCliEsQueryNormalizer();
		}
		throw new Error('There is no cli query normalizer for the language code "' + languageCode + '"');
	}

}