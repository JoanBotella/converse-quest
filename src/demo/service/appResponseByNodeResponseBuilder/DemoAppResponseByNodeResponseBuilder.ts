import { DemoStatusItf } from "../../library/status/DemoStatusItf";
import { DemoSlideItf } from "../../library/slide/DemoSlideItf";
import { DemoAppResponseItf } from "../../library/appResponse/DemoAppResponseItf";
import { DemoNodeResponseItf } from "../../library/nodeResponse/DemoNodeResponseItf";
import { CoreAppResponseByNodeResponseBuilderAbs } from "../../../converseQuest/core/library/appResponseByNodeResponseBuilder/CoreAppResponseByNodeResponseBuilderAbs";
import { DemoAppResponseByNodeResponseBuilderItf } from "./DemoAppResponseByNodeResponseItf";
import { DemoAppResponse } from "../../library/appResponse/DemoAppResponse";
import { DemoConfigurationItf } from "../../library/configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../../library/variables/DemoVariablesItf";

export
	class DemoAppResponseByNodeResponseBuilder
	extends CoreAppResponseByNodeResponseBuilderAbs<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoSlideItf,
		DemoAppResponseItf,
		DemoNodeResponseItf
	>
	implements DemoAppResponseByNodeResponseBuilderItf
{

	protected instantiate(status:DemoStatusItf):DemoAppResponseItf
	{
		return new DemoAppResponse(status);
	}

}