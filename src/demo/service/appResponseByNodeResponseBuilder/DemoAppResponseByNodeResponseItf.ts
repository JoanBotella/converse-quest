import { CoreAppResponseByNodeResponseBuilderItf } from "../../../converseQuest/core/library/appResponseByNodeResponseBuilder/CoreAppResponseByNodeResponseBuilderItf";
import { DemoStatusItf } from "../../library/status/DemoStatusItf";
import { DemoSlideItf } from "../../library/slide/DemoSlideItf";
import { DemoAppResponseItf } from "../../library/appResponse/DemoAppResponseItf";
import { DemoNodeResponseItf } from "../../library/nodeResponse/DemoNodeResponseItf";
import { DemoVariablesItf } from "../../library/variables/DemoVariablesItf";
import { DemoConfigurationItf } from "../../library/configuration/DemoConfigurationItf";

export
	interface DemoAppResponseByNodeResponseBuilderItf
	extends CoreAppResponseByNodeResponseBuilderItf<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoSlideItf,
		DemoAppResponseItf,
		DemoNodeResponseItf
	>
{

}