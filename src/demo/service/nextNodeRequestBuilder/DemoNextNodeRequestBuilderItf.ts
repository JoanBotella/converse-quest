import { CoreNextNodeRequestBuilderItf } from "../../../converseQuest/core/library/nextNodeRequestBuilder/CoreNextNodeRequestBuilderItf";
import { DemoStatusItf } from "../../library/status/DemoStatusItf";
import { DemoNodeRequestItf } from "../../library/nodeRequest/DemoNodeRequestItf";
import { DemoSlideItf } from "../../library/slide/DemoSlideItf";
import { DemoNodeResponseItf } from "../../library/nodeResponse/DemoNodeResponseItf";
import { DemoVariablesItf } from "../../library/variables/DemoVariablesItf";
import { DemoConfigurationItf } from "../../library/configuration/DemoConfigurationItf";

export
	interface DemoNextNodeRequestBuilderItf
	extends CoreNextNodeRequestBuilderItf<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoNodeRequestItf,
		DemoSlideItf,
		DemoNodeResponseItf
	>
{

}