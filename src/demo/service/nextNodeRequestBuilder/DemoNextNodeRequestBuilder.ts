import { DemoStatusItf } from "../../library/status/DemoStatusItf";
import { DemoNodeRequestItf } from "../../library/nodeRequest/DemoNodeRequestItf";
import { CoreNextNodeRequestBuilderAbs } from "../../../converseQuest/core/library/nextNodeRequestBuilder/CoreNextNodeRequestBuilder";
import { DemoNextNodeRequestBuilderItf } from "./DemoNextNodeRequestBuilderItf";
import { DemoNodeRequest } from "../../library/nodeRequest/DemoNodeRequest";
import { DemoSlideItf } from "../../library/slide/DemoSlideItf";
import { DemoNodeResponseItf } from "../../library/nodeResponse/DemoNodeResponseItf";
import { DemoConfigurationItf } from "../../library/configuration/DemoConfigurationItf";
import { DemoVariablesItf } from "../../library/variables/DemoVariablesItf";

export
	class DemoNextNodeRequestBuilder
	extends CoreNextNodeRequestBuilderAbs<
		DemoConfigurationItf,
		DemoVariablesItf,
		DemoStatusItf,
		DemoNodeRequestItf,
		DemoSlideItf,
		DemoNodeResponseItf
	>
	implements DemoNextNodeRequestBuilderItf
{

	public build(nodeResponse:DemoNodeResponseItf):DemoNodeRequestItf
	{
		return new DemoNodeRequest(
			nodeResponse.getStatus()
		);
	}

}