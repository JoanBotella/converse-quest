import { CoreCliPrompterAbs } from "../../../converseQuest/core/library/cliPrompter/CoreCliPrompterAbs";
import { DemoCliPrompterItf } from "./DemoCliPrompterItf";
import { CoreCliStylizerItf } from "../../../converseQuest/core/service/cliStylizer/CoreCliStylizerItf";

export
	class DemoCliPrompter
	extends CoreCliPrompterAbs<
		CoreCliStylizerItf
	>
	implements DemoCliPrompterItf
{
}